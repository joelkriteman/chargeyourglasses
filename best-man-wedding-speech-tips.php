<?php include_once("includes/header.php");
$pageId=3;
$sql="SELECT * FROM ".TABLE_PAGES." WHERE status='1' AND pages_id='".$pageId."'";
$result=mysql_query($sql);
$rowsResult1=mysql_fetch_array($result);

$sqlFeedback="SELECT * FROM ".TABLE_FEEDBACKS." WHERE status='1' AND home_page_status='1' ORDER BY rand()";
$resultFeedback=mysql_query($sqlFeedback);
?>
<!--header end here-->

<section id="breadcrumbs" class="alpha-ver15">
  <div class="col_1280">
    <ul>
     <li><a href="<?php echo SITE_URL;?>">Home</a></li>
      <li>&gt;</li>
      <li><?php echo $rowsResult1['page_subtitle'];?></li>
    </ul>
  </div>
</section>
<section class="content-outer alpha-ver15">
  <div class="col_1280 narrow-side">
    <aside id="main-content" class="alignleft">
      <div class="sub-heading">
        <h1> <?php echo $rowsResult1['page_subtitle'];?></h1>
      </div>
      <div class="dashed-border"></div>
      <h3 class="omega-ver20">so you're the <strong><?php echo $rowsResult1['pages_title'];?></strong> <?php echo $rowsResult1['pages_content'];?></h3>
      <p> <?php echo $rowsResult1['page_tips_intro'];?></p>
      <div class="dashed-border"></div>
      <article class="omega20">
        <div class="blue sub-heading">You should also:</div>
        <ul class="bullet-link-list">
          <?php echo $rowsResult1['page_you_should_also'];?>
        </ul>
        <div class="blue sub-heading">Do:</div>
        <ul class="bullet-link-list">
          <?php echo $rowsResult1['page_do'];?>
        </ul>
        <div class="blue sub-heading">Don't:</div>
        <ul class="bullet-link-list">
          <?php echo $rowsResult1['page_dont'];?>
        </ul>
      </article>
    </aside>
    <?php include_once("includes/sidebar.php");?>
  </div>
</section>
<?php include_once("includes/footer.php");?>
