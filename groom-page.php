<?php include_once("includes/header.php");?>
<script>
$(document).ready(function(){
  $("ul").click(function(){
    $("#show").show();
	$("#show-tab").show();

	 $('#show-tab').addClass('active');

	    });
    $("#show3").click(function(){
	$("#show2").show();
		 $('#show-tab2').addClass('active2');

	    });
    $("#content-show").click(function(){
	$("#content-show1").show();
	    });
});
</script>

<section id="breadcrumbs" class="alpha-ver15">
  <div class="col_1280">
    <ul>
      <li><a href="#">Home &gt;</a></li>
      <li>Father of the bride</li>
    </ul>
  </div>
</section>
<!--header end here-->
<section class="content-outer">
  <div class="col_1280">
    <aside class="alignleft col_24">
      <div class="bride-outer">
        <div class="bride-circle-outer">
          <div class="ban-img col_80"><img src="<?php echo SITE_URL;?>/assets/images/father-of-bride.jpg" alt="father of the bride"></div>
          <div class="ban-ico"><img src="<?php echo SITE_URL;?>/assets/images/father-bride-ico.jpg" alt="bride"></div>
        </div>
        <strong>Your Wedding Party Members</strong>
        <section class="dashed-border">
          <p><strong>Bride's Side</strong></p>
          <ul class="brideside-left">
            <li>Bride: </li>
            <li>Father: </li>
            <li>Mother: </li>
            <li>Bridemaids: </li>
            <li>Grandma: </li>
          </ul>
          <ul class="brideside-right">
            <li>Anjali</li>
            <li>Prem</li>
            <li>blank</li>
            <li>blank</li>
            <li>blank</li>
            <li>blank</li>
          </ul>
        </section>
        <section class="dashed-border">
          <p><strong>Groom's Side</strong></p>
          <ul class="brideside-left">
            <li>Bride: </li>
            <li>Father: </li>
            <li>Mother: </li>
            <li>Bridemaids: </li>
            <li>Grandma: </li>
          </ul>
          <ul class="brideside-right">
            <li>Anjali</li>
            <li>Prem</li>
            <li>blank</li>
            <li>blank</li>
            <li>blank</li>
            <li>blank</li>
          </ul>
        </section>
        <section class="dashed-border aligncenter"> <a href="#" class="medium-btn">Edit Wedding Party</a> </section>
        <div class="clear"></div>
      </div>
    </aside>
    <aside class="alignright col_74 omega20">
      <h3 class="blue"> so you're the...</h3>
      <p class="big-heading omega10">Father of the Bride</p>
      <p class="blue omega10"><strong>... and you're looking to deliver a great wedding speech? Well you've come to the right place.</strong></p>
      <article class="work-outer omega30">
        <div class="work-circle">
          <p class="omega20">How it
            works</p>
        </div>
        <div class="work-para">Click on the sections below to build your speech. Each paragraph has been professionally written by talented speech writers. Put together a few appropriate paragraphs and you'll have your own fantastic Groom speech in no time! You can then download your speech and personalize it further.</div>
      </article>
      <article id="js-steps" class="step-outer omega-ver20">
        <aside class="step1 alignleft">
          <div class="step1bg">
            <div class="stepaligncenter"> <span class="numberic">1</span>
              <p class="alignright">speech <br>
                paragraph...</p>
            </div>
          </div>
          <ul class="step-links">
            <li><a href="javascript:(void);">Opening</a></li>
            <li><a href="#">Thanks for coming</a></li>
            <li><a href="#">They married somewhere else first</a></li>
            <li><a href="#">Absentees</a></li>
            <li><a href="#">About my wife</a></li>
            <li><a href="#">I'm not the perfect parent</a></li>
            <li><a href="#">Introducing my daughter</a></li>
            <li><a href="#">Daughter as an infant</a></li>
            <li><a href="#">Daughter at school</a></li>
            <li><a href="#">Daughter at University</a></li>
            <li><a href="#">My daughter's wonderful</a></li>
            <li><a href="#">I'll tease my daughter a little</a></li>
            <li><a href="#">Daughter's hobbies</a></li>
            <li><a href="#">About the new son-in-law</a></li>
            <li><a href="#">Welcoming his parents</a></li>
            <li><a href="#">Toast</a></li>
          </ul>
        </aside>
        <aside class="step2 alignleft">
          <div class="step2bg-default" id="show-tab">
            <div class="stepaligncenter"> <span class="numberic">2</span>
              <p class="alignright">paragraph <br>
                options...</p>
              <span></span></div>
          </div>
          <ul class="step-links" id="show" style="display:none;">
            <li><a href="javascript:(void);" id="show3"> sober...</a></li>
            <li><a href="#">Nervous...</a></li>
            <li><a href="#">Pride...</a></li>
            <li><a href="#">Obscure venue...</a></li>
            <li><a href="#">Late afternoon start...</a></li>
            <li><a href="#">My wife...</a></li>
            <li><a href="#">Historic Venue...</a></li>
            <li><a href="#">Second Daughter’s wedding...</a></li>
          </ul>
        </aside>
        <aside class="step3 alignleft">
          <p id="show-tab2" class="step3bg-default"><img src="assets/images/step3-ico.png"><span></span></p>
          <div id="show2" style="display:none;">
            <div class="step-para omega30">
              <p> <span class="test-ico-left"><img src="<?php echo SITE_URL;?>/assets/images/test-ico.png"></span>Ladies and Gentlemen I am under
                strict instructions not to have a drink until after the first toast. So you'll have to excuse me if I seem in a hurry to finish.</p>
              <span class="test-ico-right"><img src="<?php echo SITE_URL;?>/assets/images/test-ico2.png"></span> </div>
            <div class="aligncenter omega20"><a href="#" id="content-show" class="small-btn">+ Add to Speech</a></div>
            <div class="step-para omega30">
              <p> <span class="test-ico-left"><img src="<?php echo SITE_URL;?>/assets/images/test-ico.png"></span>Ladies and Gentlemen I am under
                strict instructions not to have a drink until after the first toast. So you'll have to excuse me if I seem in a hurry to finish.</p>
              <span class="test-ico-right"><img src="<?php echo SITE_URL;?>/assets/images/test-ico2.png"></span> </div>
            <div class="aligncenter omega20"><a href="#"  class="small-btn">+ Add to Speech</a></div>
            <div class="step-para omega30">
              <p> <span class="test-ico-left"><img src="<?php echo SITE_URL;?>/assets/images/test-ico.png"></span>Ladies and Gentlemen I am under
                strict instructions not to have a drink until after the first toast. So you'll have to excuse me if I seem in a hurry to finish.</p>
              <span class="test-ico-right"><img src="<?php echo SITE_URL;?>/assets/images/test-ico2.png"></span> </div>
            <div class="aligncenter omega20"><a href="#" class="small-btn">+ Add to Speech</a></div>
          </div>
        </aside>
      </article>
      <div id="content-show1" style="display:none;">
        <p class="main-heading">Your Father of the Bride Speech</p>
        <div class="caption-blue"> This is the second time I have given a speech in public. The last time was three years ago. Some of you might
          remember it. I don't. blamed me. And I blamed the wine... <strong>(60 lines)</strong></div>
        <p class="aligncenter omega-ver20"> <a href="#" class="medium-btn">DOWNLOAD SPEECH<span class="sprite-img"><img src="<?php echo SITE_URL;?>/assets/images/download-ico.png" alt="download speech"></span></a> </p>
      </div>
    </aside>
  </div>
</section>
<!--footer outer start here-->
<?php include_once("includes/footer.php");?>
