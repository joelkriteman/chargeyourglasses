<?php
include_once("includes/header.php");
//PDT
// read the post from PayPal system and add 'cmd'
$req = 'cmd=_notify-synch';
$tx_token = $_GET['tx'];
$auth_token = "dJnDmYwhHqJAx9iuW13GGwFPTArHmCv-HgCfqYwTTod0TMztPtk2aiPOgzm";
$req .= "&tx=$tx_token&at=$auth_token";

// post back to PayPal system to validate
$header .= "POST /cgi-bin/webscr HTTP/1.0\r\n";
$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
$header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
$fp = fsockopen ('www.paypal.com', 80, $errno, $errstr, 30);
// $fp = fsockopen ('ssl://www.paypal.com', 443, $errno, $errstr, 30);

?>

<section id="breadcrumbs" class="alpha-ver15">
  <div class="col_1280">
    <ul>
      <li><a href="<?php echo SITE_URL;?>">Home</a></li>
      <li>&gt;</li>
      <li>Thank you for your payment</li>
    </ul>
  </div>
</section>
<section class="content-outer alpha-ver15">
  <div class="col_1280">
    <div class="order-outer alignleft col_40 omega-ver20">
      <?php if (!$fp) {
// HTTP ERROR
} else {
fputs ($fp, $header . $req);
// read the body data
$res = '';
$headerdone = false;
while (!feof($fp)) {
$line = fgets ($fp, 1024);
if (strcmp($line, "\r\n") == 0) {
// read the header
$headerdone = true;
}
else if ($headerdone)
{
// header has been read. now read the contents
$res .= $line;
 }
}

// parse the data
$lines = explode("\n", $res);
$keyarray = array();
if (strcmp ($lines[0], "SUCCESS") == 0) {
for ($i=1; $i<count($lines);$i++){
list($key,$val) = explode("=", $lines[$i]);
$keyarray[urldecode($key)] = urldecode($val);
}

// check the payment_status is Completed
// check that txn_id has not been previously processed
// check that receiver_email is your Primary PayPal email
// check that payment_amount/payment_currency are correct
// process payment

$firstname = $keyarray['first_name'];
$lastname = $keyarray['last_name'];
$payer_email = $keyarray['payer_email'];
$amount = $keyarray['mc_gross'];
//$tax = $keyarray['tax'];
//$discount = $keyarray['discount'];
$payment_date = $keyarray['payment_date'];
$payment_status = $keyarray['payment_status'];
$payment_type = $keyarray['payment_type'];
$pending_reason = $keyarray['pending_reason'];
$mc_currency = $keyarray['mc_currency'];
$transactionid = $keyarray['txn_id'];
$orderId = base64_decode($keyarray['custom']);
$backUrl = $keyarray['back_url'];

$orderDetails = $OrderObj->funGetOrderInfo($orderId);
$userDetails = $customerobj->funGetUserInfo($orderDetails['user_id']);
$day = $generalInfoDetail['time_days'];
if($payment_status=="Completed")
{
$orderUpdate = $OrderObj->OrderPaymentModify($orderId);
$userDetailsUpdate = $customerobj->ModifyUserPayment($orderDetails['user_id'],$day);
}
echo "<h1 class=\"omega-ver20\">Thank you for your payment</h1><p class=\"small-heading yellow-bg\"><strong>Your payment details:</strong></p><div class=\"order-inner\">";

$orderNumber = $orderDetails['order_number'];
if($orderNumber!="")
{
echo ("<ul><li class=\"order-left\">Transaction ID:</li> <li class=\"order-right\"> $transactionid</li></ul>");
echo ("<ul><li class=\"order-left\">Order Number:</li> <li class=\"order-right\"> $orderNumber</li></ul>");
echo ("<ul><li class=\"order-left\">Name:</li> <li class=\"order-right\">$firstname $lastname ($payer_email)</li></ul>");
echo ("<ul><li class=\"order-left\">Amount:</li> <li class=\"order-right\"> $amount GBP</li></ul>");
echo ("<ul><li class=\"order-left\">Date:</li> <li class=\"order-right\"> $payment_date</li></ul>");
echo ("<ul><li class=\"order-left\">Payment status:</li> <li class=\"order-right\"> $payment_status</li></ul>");
echo "</div>";
		 $_SESSION['session_username'] = $userDetails['user_email'];
		 $_SESSION['user_id'] = $orderDetails['user_id'];
if($orderDetails['payment_status_id']!=1)
{

		 $emailRegisterFile = SITE_EMAIL_TAMPLATE_WS . "payment-confirmation.html";
   	 	 $pwdContent = fun_getFileContent($emailRegisterFile);
 	     $pwdContent = str_replace("[%SITE_NAME%]", SITE_NAME, $pwdContent);
		 $pwdContent = str_replace("[%NAME%]", $firstname." ".$lastname, $pwdContent);
		 $pwdContent = str_replace("[%EMAIL%]",  $payer_email, $pwdContent);
		 $pwdContent = str_replace("[%DATE%]",  date("d/m/Y"), $pwdContent);
         $pwdContent = str_replace("[%AMOUNT%]", $amount." GBP", $pwdContent);
 		 $pwdContent = str_replace("[%ORDERID%]", $orderNumber, $pwdContent);
         $pwdContent = str_replace("[%PAYMENTSTATUS%]", $payment_status, $pwdContent);
		 $pwdContent = str_replace("[%SITE_LOGO%]", EMAIL_LOGO, $pwdContent);
		 
		 $emailPaymentAdminFile = SITE_EMAIL_TAMPLATE_WS . "payment-confirmation-admin.html";
   	 	 $payAdminContent = fun_getFileContent($emailPaymentAdminFile);
 	     $payAdminContent = str_replace("[%SITE_NAME%]", SITE_NAME, $payAdminContent);
		 $payAdminContent = str_replace("[%NAME%]", $firstname." ".$lastname, $payAdminContent);
		 $payAdminContent = str_replace("[%EMAIL%]",  $payer_email, $payAdminContent);
		 $payAdminContent = str_replace("[%AMOUNT%]",  $amount." GBP", $payAdminContent);
		 $payAdminContent = str_replace("[%ORDERID%]",$orderNumber, $payAdminContent);
         $payAdminContent = str_replace("[%PAYMENTSTATUS%]", $payment_status, $payAdminContent);
 		 $payAdminContent = str_replace("[%DATE%]",  date("d/m/Y"), $payAdminContent);
		 $payAdminContent = str_replace("[%SITE_LOGO%]", EMAIL_LOGO, $payAdminContent);
		 
		 $to=$payer_email;
		 $to1=SITE_PAYMENT_EMAIL_ID;
		 $subject = 'Your payment details for '.SITE_NAME.'';
		 $subject1 = 'Chargeyourglasses Payment details - '.$payer_email;
	 	 $from=SITE_SUPPORT_EMAIL_ID;
		 $fromContent="Charge Your Glasses";	
		 $mailSentStatus = fun_get_email($to , $subject, $pwdContent, $from,$fromContent);
		 $mailSentStatus1 = fun_get_email($to1 , $subject1, $payAdminContent, $from,$fromContent);
}
if($backUrl!="")
{
//redirectURL($backUrl);
}
}
}
else if (strcmp ($lines[0], "FAIL") == 0) {
// log for manual investigation
 }
}

fclose ($fp);
?>
    </div>
    <aside class="alignright col_58">
      <h2 class="omega-ver20" align="center">Now Choose Your Role</h2>
      <!--banner widget outer start here-->
      <section class="banner-widget-outer"> <a href="<?php echo SITE_URL;?>father-of-the-bride-wedding-speeches.php">
        <div class="banner-widget">
          <div class="ban-img">
            <div class="ban-bg"><img src="<?php echo SITE_URL;?>/assets/images/father-of-bride.jpg" alt="father of the bride"></div>
          </div>
          <div class="ban-ico"><img src="<?php echo SITE_URL;?>/assets/images/father-bride-ico.jpg" alt="bride"></div>
          <p>Are you <br>
            FATHER of the BRIDE?</p>
        </div>
        <p><span class="blue-btn">Click here</span></p>
        </a> </section>
      <!--banner widget outer end here--> 
      <!--banner widget outer start here-->
      <section class="banner-widget-outer"> <a href="<?php echo SITE_URL;?>groom-wedding-speeches.php">
        <div class="banner-widget">
          <div class="ban-img">
            <div class="ban-bg"><img src="<?php echo SITE_URL;?>/assets/images/groom-img.jpg" alt="Are You a Groom?"></div>
          </div>
          <div class="ban-ico"><img src="<?php echo SITE_URL;?>/assets/images/groom_ico.jpg" alt="groom"></div>
          <p>Are You <br>
            a Groom?</p>
        </div>
        <p><span class="blue-btn">Click here</span></p>
        </a> </section>
      <!--banner widget outer end here--> 
      <!--banner widget outer start here-->
      <section class="banner-widget-outer"> <a href="<?php echo SITE_URL;?>best-man-wedding-speeches.php">
        <div class="banner-widget">
          <div class="ban-img">
            <div class="ban-bg"><img src="<?php echo SITE_URL;?>/assets/images/best-man.jpg" alt="Are you the Best Man?"></div>
          </div>
          <div class="ban-ico"><img src="<?php echo SITE_URL;?>/assets/images/best-man-ico.jpg" alt="best man"></div>
          <p>Are you<br>
            the Best Man?</p>
        </div>
        <p><span class="blue-btn">Click here</span></p>
        </a> </section>
      <!--banner widget outer end here--> 
    </aside>
  </div>
</section>
<?php 
include_once("includes/footer.php");?>
