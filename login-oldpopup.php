<?php 
require_once("includes/application-top.php");
?>
<!DOCTYPE HTML>
<html>
<head>
<meta name="robots" content="noindex, nofollow" />
<script type="text/javascript" src="<?php echo SITE_URL;?>assets/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript">
function validateLoginFrm(){
var frm = document.LoginForm;
var referrer = window.parent.location.href;
if(frm.user_email.value==""){
	frm.user_email.style.borderColor='#FF0000';
			frm.user_email.focus();
			return false;
		}
		if(frm.user_email.value!=""){
		if(isEmail(frm.user_email.value)==false){
			frm.user_email.style.borderColor='#FF0000';
			frm.user_email.focus();
			return false;
		}
		}
		if(frm.user_pwd.value==""){
	frm.user_pwd.style.borderColor='#FF0000';
			frm.user_pwd.focus();
			return false;
		}
		if((frm.user_pwd.value!='')&&(frm.user_email.value!='')){
			var email=frm.user_email.value;
			var pwd=frm.user_pwd.value;
			var ajaxaction='SignUpCheck';
	 		$.post('<?php echo SITE_URL;?>ajaxcon', {ajaxaction:ajaxaction,email:email,password:pwd}, function(data) {
				//alert(data);
				if(data=="")
				{
					if(referrer== "http://localhost/chargeyourglasses/signup.php")
					{
						window.parent.location.href ="http://www.chargeyourglasses.com";
					}
					else
					{
					window.parent.location.href =referrer;
					}

				}
	 		$('#validation1').html(data);
			return false;
			});
			//alert("dasd");
			return false;
		}

}

function isEmail(str){
	var at="@";
	var dot=".";
	var lat=str.indexOf(at);
	var ldot=str.indexOf(dot);
	var lstr=str.length;

	if(str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
		return false;
	}
	if(str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
		return false;
	}
	if(str.indexOf(" ")!=-1){
		return false;
	}
	if(str.indexOf(at,(lat+1))!=-1){
		return false;
	}
	if(str.indexOf(dot,(lat+2))==-1){
		return false;
	}
	if(str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
		return false;
	}
return true;
}

</script>
<script type="text/javascript">
function signUpwithFb()
{
var fbWindow = parent.open("fb/request_permissions.php",'FacebookLogin','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no');
fbWindow.focus();
}
</script>
<link href="<?php echo SITE_URL;?>assets/css/popup.css" rel="stylesheet" type="text/css">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-571089-4', 'auto');
  ga('send', 'pageview');

</script>

</head>
<body>
<article class="register-outer">
  <div class="register-bg aligncenter">
    <h3>Please enter your login details below</h3>
  </div>
  <section class="alignleft col_49 omega-ver20">
    <div class="aligncenter alpha-all10">
      <p> You can connect with your Facebook
        account to prepopulate some of the fields. </p>
      <p class="omega20"> <a id="go_fb1" href="javascript:signUpwithFb();"><img src="assets/images/login-facebook-btn.jpg" /></a></p>
    </div>
  </section>
  <section class="alignright register-form col_49 omega-ver20">
    <div class="alpha-all10">
      <div id="validation1"></div>
      <form method="post" name="LoginForm" onSubmit="return validateLoginFrm();" action="<?php echo SITE_URL;?>login">
        <div class="omega5">
          <label>Email Address:<span class="red">*</span></label>
          <input type="text" name="user_email"/>
        </div>
        <div class="omega5">
          <label>Password:<span class="red">*</span></label>
          <input type="password" name="user_pwd"/>
        </div>
        <div class="omega5">
          <label>&nbsp;</label>
          <div class="alignleft">
            <input type="submit" value="Login" />
          </div>
          <div class="alignright omega15"><a href="<?php echo SITE_URL;?>forgot-password.php" class="examplepage blue" style="cursor:pointer;">Forgot Password?</a></div>
        </div>
      </form>
    </div>
  </section>
</article>
</body>
</html>