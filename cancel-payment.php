<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Payment Cancelled</title>
<link href="assets/css/payment-checkout.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div class="col-700">
  <div id="logo">
    <h1 class="center-align"><a href="http://www.chargeyourglasses.com" title="CHARGEYOURGLASSES"><img src="http://www.chargeyourglasses.com/assets/images/cyg_logo.jpg" alt="CHARGEYOURGLASSES | Create the Perfect Wedding Speeches"></a></h1>
  </div>
  <h1 class="center-align blue">PAYMENT FAILED</h1>
  <p class="center-align">OOPS - YOUR TRANSACTION DIDN'T GO THROUGH!<br/>
    <br/>
    In case you continue to face problems, please call our customer support at +44 (0)20 8245 8999 .</p>
  <div style="clear:both;"></div>
  <div class="alignmodule">
    <div class="alignleft"><a href="http://www.chargeyourglasses.com" class="bluebtn">Back to Home</a></div>
    <!--<div class="alignright"><a href="#" class="bluebtn">Try Again</a></div>
-->
    <div style="clear:both;"></div>
  </div>
</div>
</body>
</html>
