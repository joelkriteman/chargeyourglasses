<aside id="sidebar" class="alignright">
      <form method="post" name="ContactFrm" onsubmit="return validateContactFrm();" action="">
        <input type="hidden" name="security_key" value="<?php echo md5("CONTACTUS");?>"  />
        <input class="ZipCodeStyle" type="text" value="" name="ZipCode">
        <input type="hidden" name="contactForm" value="1"  />
        <p class="sub-heading">Need further help?</p>
        <p class="omega10">We can refer you to the UK's top wedding speech writers:</p>
        <section class="enquiry-form dashed-border">
     
          <aside class="omega10">
            <label>Name:<span class="red">*</span></label>
            <div class="input-form">
              <input type="text" name="name">
            </div>
          </aside>
          <aside class="omega10">
            <label>Email:<span class="red">*</span></label>
            <div class="input-form">
              <input type="text" name="email">
            </div>
          </aside>
          <aside class="omega10">
            <label>Phone:</label>
            <div class="input-form">
              <input type="text" name="phone_number">
            </div>
          </aside>
          <aside class="omega10">
            <label>Speech for:</label>
            <div class="input-form">
              <select name="speech_for">
                <option value="1">Best Man</option>
                <option value="2">Father of the Bride</option>
                <option value="3">Groom</option>
                
              </select>
            </div>
            
            
          </aside>
           <aside class="omega10">
          <label>Wedding Date:</label>
            <div class="input-form">
          <!-- <input type="text" name="user_wed_date" id="date" />-->
          <select class="date" name="wedding_day">
            <?php fun_create_number_options(1,31,date('d'));?>
          </select>
          <select class="month" name="wedding_month">
            <?php fun_created_month_option(date('m'));?>
          </select>
          <select class="year" name="wedding_year">
            <?php fun_created_year_option();?>
          </select></div>
        </aside>
          <aside class="omega10">
            <label>Please include as much detail below as possible so we can better respond to your enquiry and provide a cost estimate.  Do let us know if there&acute;s a convenient time to call.<span class="red">*</span></label>
            <div class="input-textarea">
              <textarea cols="" rows="5" name="message"></textarea>
            </div>
          </aside>
          <aside class="omega10">
            <input type="submit" value="Make Enquiry">
          </aside>
        </section>
      </form>
    </aside>