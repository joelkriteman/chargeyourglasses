<!--testimonials outer start here-->
<article id="testimonials-outer">
  <div class="col_1280">


    <!--Customers Feedback outer start here-->
    <section class="foot-widget-1 alignleft">
    	<img src="assets/images/phone.jpg" alt=""/ class="footerimage">
      <h3>Customer Feedback </h3>
      <article class="feedback-outer">
        <div class="alignleft col_100">
          <div class="flexslider">
            <ul class="slides">
            <?php while($rowsResultFeedback=mysql_fetch_array($resultFeedback)) {?>
              <li>
                <p class="sub-heading"><?php echo $rowsResultFeedback['client_name'];?></p>
                <p><?php echo $rowsResultFeedback['feedback_text'];?></p>
                <p class="omega20 small-heading"><span><?php echo $rowsResultFeedback['location'];?> | <?php echo fun_site_date_format($rowsResultFeedback['date']);?></span></p>
              </li>
              <?php } ?>
            </ul>
           </div>
          <p class="download-btn"><a href="<?php echo SITE_URL;?>testimonials.php" class="small-heading black">more feedback</a></p>
        </div>

      </article>
    </section>
    <!--Customers Feedback outer end here-->
    
    <!--foot widget2 start here-->
    <section class="foot-widget-2 alignleft">
    <img src="assets/images/suit.jpg" alt=""/ class="footerimage">
      <h3>Take Our Quiz</h3>
      <!--bullet points start here-->
      <div>
        <ul>
          <li><a href="<?php echo SITE_URL;?>how-well-do-you-know-the-groom-quiz.php">How well do you know the Groom? </a></li>
          <li><a href="<?php echo SITE_URL;?>how-well-do-you-know-your-bride-quiz.php">How well do you know your fianc&eacute;e?</a></li>
          <li><a href="<?php echo SITE_URL;?>is-he-good-enough-for-your-daughter-quiz.php">Is he good enough for your daughter?</a> </li>
        </ul>
      </div>
      <!--bullet point end here-->
    </section>
    <!--foot widget2 end here-->
    
    <!--foot widget3 start here-->
    <section class="foot-widget-3 alignleft">
    <img src="assets/images/pen.jpg" alt=""/ class="footerimage">
      <h3>A Sample of our speech material</h3>
      <div class="omega20">
        <div class="footer-para"><?php echo $generalInfoDetail['home_line_of_month_text'];?></div>
                  <p class="download-btn"> <a href="<?php echo SITE_URL;?>pdf/<?php echo $generalInfoDetail['sample_speech_file'];?>" onclick="ga('send', 'event', 'DownloadPDF', 'SamplePDF', 'HOMEPAGE');" target="_blank" >DOWNLOAD SAMPLE SPEECH</a></p>
      </div>
    </section>
    <!--foot widget3 end here-->
    
        <!--foot widget4 start here-->
    <section class="foot-widget-4 alignleft">
      <div> <a class="twitter-timeline" height="510" href="https://twitter.com/ChargeUrGlasses" data-widget-id="466520818982191104"></a>
        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
      </div>
    </section>
    <!--foot widget4 end here-->


    
    
    
  </div>
</article>
<!--testimonials outer end here-->

<!--bottom outer start here-->
<section id="bottom-outer" class="alpha-ver15">
  <div class="col_1280">
    <div class="alignleft"><img src="<?php echo SITE_URL;?>assets/images/footer-logo.png" alt="charge your glasses"></div>
    <div class="alignright footer-links"> <a href="<?php echo SITE_URL;?>terms-condition.php">T&amp;Cs</a> - <a href="<?php echo SITE_URL;?>privacy.php">Privacy</a> - <a href="<?php echo SITE_URL;?>sitemap.php">Sitemap</a> -  © Copyright 2007 - <?php echo date("Y"); ?></div>
  </div>
</section>
<!--bottom outer end here-->

</div>
<!--Page end here-->


<div id="content-area" style="display:none;">
<div class="introjs-overlay" style="top: 0;bottom: 0; left: 0;right: 0;position: fixed;background-color: rgb(0,0,0);opacity: 0.8;"></div>
<!--  <div class="margincenter"><img src="<?php echo SITE_URL;?>assets/images/sprite.png"></div>-->
<div class="introjs-helperLayer layer-0 subscribe" style="width: 119px; height:31px; top:47px;left: 879px;"><div class="introjs-tooltip" style="background-image: url(http://assets-1.housingcdn.com/website/feature_intro/sprite-df96b74c18dc441f994fcee8f247c87f.png); bottom: -120px; background-position: -390px -235px; background-repeat: initial initial;"></div></div>
<div class="introjs-helperLayer layer-1 icon-resize-horizontal" style="width: 16px; height:16px; top:364.25px;right:250px;"><div class="introjs-tooltip" style="background-image: url(http://assets-1.housingcdn.com/website/feature_intro/sprite-df96b74c18dc441f994fcee8f247c87f.png); left: 0px; top: -120px; background-position: -600px -240px; background-repeat: initial initial;"></div></div>
<div class="introjs-helperLayer layer-2" style="width: 145px; height:31px; top:47px;left: 722px;"><div class="introjs-tooltip" style="background-image: url(http://assets-1.housingcdn.com/website/feature_intro/sprite-df96b74c18dc441f994fcee8f247c87f.png); top: 0px; right: 130px; background-position: -600px 0px; background-repeat: initial initial;"></div></div>
<div class="introjs-helperLayer layer-3 icon-move" style="width: 17px; height:11px; top:367.203125px;left: 821.25px;"><div class="introjs-tooltip" style="background-image: url(http://assets-1.housingcdn.com/website/feature_intro/sprite-df96b74c18dc441f994fcee8f247c87f.png); left: 0px; top: -120px; background-position: -610px -113px; background-repeat: initial initial;"></div></div>
</div>

<!-- Payment Instruction Popup -->
<div style="display:none;">
  <div id="paymentpopup" class="popupwindow">
    <div class="register-bg aligncenter">
      <h3>Alert</h3>
    </div>
    <div class="alpha-all10 aligncenter">
      <p>After payment, click on <img src="<?php echo SITE_URL;?>assets/images/subscribe-btn.png" /></p>
      <p class="omega10">For reference find the screenshot below:</p>
      <p><img src="<?php echo SITE_URL;?>assets/images/thank-you-message.png" class="img-align" /></p>
      <p class="omega20"> <a class="proceed-btn" href="make-payment.php?back_url=<?php echo urlencode($_SERVER['HTTP_REFERER']);?>"><strong>OK! Proceed</strong></a> </p>
    </div>
  </div>
</div>
<!-- End Payment Instruction Popup -->

<!-- Payment Instruction Popup -->
<div style="display:none;">
  <div id="backurlpaymentpopup" class="popupwindow">
    <div class="register-bg aligncenter">
      <h3>Alert</h3>
    </div>
    <div class="alpha-all10 aligncenter">
      <p>After payment, click on <img src="<?php echo SITE_URL;?>assets/images/subscribe-btn.png" /></p>
      <p class="omega10">For reference find the screenshot below:</p>
      <p><img src="<?php echo SITE_URL;?>assets/images/thank-you-message.png" class="img-align" /></p>
      <p class="omega20"> <a class="proceed-btn" href="make-payment.php?back_url=<?php echo $_REQUEST['back_url'];?>"><strong>OK! Proceed</strong></a> </p>
    </div>
  </div>
</div>
<!-- End Payment Instruction Popup -->

<script type="text/javascript">
$(function () {
  $("#fixed-bar")
    .css({position:'fixed',bottom:'0px'})
    .hide();
  $(window).scroll(function () {
    if ($(this).scrollTop() > 200) {
      $('#fixed-bar').fadeIn(200);
    } else {
      $('#fixed-bar').fadeOut(200);
    }
  });
  $('.go-top').click(function () {
    $('html,body').animate({
      scrollTop: 0
    }, 1000);
    return false;
  });
});
</script>
<script type="text/javascript">
$("#show-background").click(function () {
  if ($("#content-area").hasClass("bg_hidden")){	
     $("#content-area")
      .removeClass("bg_hidden")
      .stop()
      .fadeOut("fast");
	 $('html').removeClass("overlay");
     $("#show-background").addClass("hide");
     $("#show-background").removeClass("show");
  }
  else{
     $("#content-area")
      .addClass("bg_hidden")
      .stop()
      .fadeIn("fast");
     $('html').addClass("overlay");
     $("#show-background").removeClass("hide");
	 $("#show-background").addClass("show");
  }
});  
</script>
<!-- mobile nav script-->
<script>
  var nav = responsiveNav(".nav-collapse");
</script>
<!-- end mobile nav script-->

<!-- curve text-->


        <script src="<?php echo SITE_URL;?>assets/js/plugins.js"></script>
        <script src="<?php echo SITE_URL;?>assets/js/mobile-steps.js"></script>
        <script src="<?php echo SITE_URL;?>assets/js/circletype.js"></script>

        <script>
            $('#demo1').circleType({radius: 80});
            $('#demo2').circleType({radius: 82, dir:-1});
            $('#demo3').circleType({radius: 1000000}); 
            $('#demo4').circleType({radius: 1000000});         
        </script>
<!-- end curve text-->
</body>
</html>