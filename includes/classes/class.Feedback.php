<?php
class Feedback{
	var $dbObj;
	
	function Feedback(){ // class constructor
		$this->dbObj = new DB();
		$this->dbObj->fun_db_connect();
	}
	
	function funGetFeedbackInfo($cateID){
		$locArray = array();
		$sql = "SELECT * FROM " . TABLE_FEEDBACKS . " WHERE feedback_id='".(int)$cateID."'";
		
		$result = $this->dbObj->fun_db_query($sql) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
		if(!$result || $this->dbObj->fun_db_get_num_rows($result) < 1){
			return; // user does not exists
		}
		$rowsCategory =  $this->dbObj->fun_db_fetch_rs_object($result);
		$locArray = array(
							"feedback_id" => fun_db_output($rowsCategory->feedback_id),
							"client_name" => fun_db_output($rowsCategory->client_name),
							"location" => fun_db_output($rowsCategory->location),
							"feedback_text" => fun_db_output($rowsCategory->feedback_text),
							"date" => fun_db_output($rowsCategory->date),
						    "list_order" => fun_db_output($rowsCategory->list_order),
							"status" => fun_db_output($rowsCategory->status),
							"home_page_status"=> fun_db_output($rowsCategory->home_page_status),
							"last_modified" => fun_db_output($rowsCategory->last_modified),
							"added_date" => fun_db_output($rowsCategory->added_date)
						 );
		return $locArray;
	}
	
	function fun_get_num_rows($sql){
		$totalRows = 0;
		$selected = "";
		$sql = trim($sql);
		if($sql==""){
			die("<font color='#ff0000' face='verdana' face='2'>Error: Query is Empty!</font>");
			exit;
		}
		$result = $this->dbObj->fun_db_query($sql);
		$totalRows = $this->dbObj->fun_db_get_num_rows($result);
		$this->dbObj->fun_db_free_resultset($result);
		return $totalRows;
	}
	
	
	
}
?>