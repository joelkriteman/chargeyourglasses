<?php
class Person{
	var $dbObj;
	
	function Person(){ // class constructor
		$this->dbObj = new DB();
		$this->dbObj->fun_db_connect();
	} 
	
	function fun_getPersonListNameOptions($conId=''){
		$selected = "";
		$sql = trim($sql);
		$sql = "SELECT * FROM " . TABLE_PERSON;
		$result = $this->dbObj->fun_db_query($sql);
		while($rowsCon = $this->dbObj->fun_db_fetch_rs_object($result)){
			if($rowsCon->person_id == $conId  && $conId!=''){
				$selected = "selected";
			}else{
				$selected = "";
			}
			echo "<option value=\"".fun_db_output($rowsCon->person_id)."\" " .$selected. ">";
			echo fun_db_output($rowsCon->person_name);
			echo "</option>\n";
		}
		$this->dbObj->fun_db_free_resultset($result);
	}
	
	function funGetPersonInfo($cateID){
		$locArray = array();
		$sql = "SELECT * FROM " . TABLE_PERSON . " WHERE person_id='".(int)$cateID."'";
		
		$result = $this->dbObj->fun_db_query($sql) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
		if(!$result || $this->dbObj->fun_db_get_num_rows($result) < 1){
			return; // user does not exists
		}
		$rowsCategory =  $this->dbObj->fun_db_fetch_rs_object($result);
		$locArray = array(
							"person_id" => fun_db_output($rowsCategory->person_id),
							"person_name" => fun_db_output($rowsCategory->person_name),
							"status" => fun_db_output($rowsCategory->status),
							"added_date" => fun_db_output($rowsCategory->added_date)
						 );
		return $locArray;
	}
	
}
?>