<?php
class Users{
	var $dbObj;
	
	function Users(){ // class constructor
		$this->dbObj = new DB();
		$this->dbObj->fun_db_connect();
	} 
function processUser(){
	
		$locArray = array(
						"user_fname" => $_POST['user_fname'],
						"user_lname" => $_POST['user_lname'],
						"user_wed_date" => $_POST['wedding_year']."-".$_POST['wedding_month']."-".$_POST['wedding_day'],
						"user_role" => $_POST['user_role'],
						"user_email" => $_POST['user_email'],
						"user_pwd" => $_POST['user_pwd'],
						"website_id" => ($_POST['website_id']),
						"user_encrypt_pwd" => md5($_POST['user_pwd']),
						"status" => 0,
						"total_orders" => 0,
						"last_login" => date("Y-m-d H:i:s")
					);
			$fields = "";
			$fieldsVal = "";
			foreach($locArray as $keys => $vals){
				$fields .= $keys . ", ";
				$fieldsVal .= "'" . fun_db_input($vals). "', ";
			}
			
			$sqlInsertFeature = "INSERT INTO " . TABLE_USERS . "(user_id, ".$fields." added_date) " ;
			 $sqlInsertFeature .= " VALUES(null, ".$fieldsVal." '".date("Y-m-d H:i:s")."')";
			$result=$this->dbObj->fun_db_query($sqlInsertFeature) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Users table.</font>");
			
			return $this->dbObj->fun_db_last_inserted_id($result);
		}
		
	function processFbUser(){
	
		$locArray = array(
						"user_fname" => $_SESSION['user_fname'],
						"user_lname" => $_SESSION['user_lname'],
						"user_email" => $_SESSION['user_email'],
						"user_pwd" => $_SESSION['user_pwd'],
						"website_id" => ($_SESSION['website_id']),
						"user_encrypt_pwd" => md5($_SESSION['user_pwd']),
						"status" => 0,
						"total_orders" => 0,
						"last_login" => date("Y-m-d H:i:s")
					);
			$fields = "";
			$fieldsVal = "";
			foreach($locArray as $keys => $vals){
				$fields .= $keys . ", ";
				$fieldsVal .= "'" . fun_db_input($vals). "', ";
			}
			
			$sqlInsertFeature = "INSERT INTO " . TABLE_USERS . "(user_id, ".$fields." added_date) " ;
			 $sqlInsertFeature .= " VALUES(null, ".$fieldsVal." '".date("Y-m-d H:i:s")."')";
			$result=$this->dbObj->fun_db_query($sqlInsertFeature) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Users table.</font>");
			
			return $this->dbObj->fun_db_last_inserted_id($result);
		}



	function funGetUserInfo($cateID){
		$locArray = array();
		$sql = "SELECT * FROM " . TABLE_USERS . " WHERE user_id='".(int)$cateID."'";
		
		$result = $this->dbObj->fun_db_query($sql) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
		if(!$result || $this->dbObj->fun_db_get_num_rows($result) < 1){
			return; // user does not exists
		}
		$rowsCategory =  $this->dbObj->fun_db_fetch_rs_object($result);
		$locArray = array(
							"user_id" => fun_db_output($rowsCategory->user_id),
							"user_title" => fun_db_output($rowsCategory->user_title),
							"user_fname" => fun_db_output($rowsCategory->user_fname),
							"user_lname" => fun_db_output($rowsCategory->user_lname),
							"user_wed_date" => fun_db_output($rowsCategory->user_wed_date),
							"website_id" => fun_db_output($rowsCategory->website_id),
							"expired_on" => fun_db_output($rowsCategory->expired_on),
							"user_role" => fun_db_output($rowsCategory->user_role),
							"user_email" => fun_db_output($rowsCategory->user_email),
							"total_orders" => fun_db_output($rowsCategory->total_orders),
							"last_login" => fun_db_output($rowsCategory->last_login),
							"status" => fun_db_output($rowsCategory->status),
							"added_date" => fun_db_output($rowsCategory->added_date)
						 );
		return $locArray;
	}
function funLoginCheckEmail($username){
		$scExists = 0;
		 $sqlCheck = "SELECT user_email  FROM " . TABLE_USERS . " WHERE user_email ='".fun_db_input($username)."' ";
		if($this->fun_get_num_rows($sqlCheck)){
				$scExists = 1;
			}
			return $scExists;
	}
	function funLoginCheck($username,$pwd){
		$scExists = 0;
		 $sqlCheck = "SELECT *  FROM " . TABLE_USERS . " WHERE user_email ='".fun_db_input($username)."' AND user_pwd  ='".fun_db_input($pwd)."' ";
		if($this->fun_get_num_rows($sqlCheck)){
				$_SESSION['session_username'] = $username;
				$scExists = 1;
			}
			return $scExists;
	}
	
	function funSignUpCheck($username,$pwd){
		$scExists = 0;
		  $sqlCheck = "SELECT *  FROM " . TABLE_USERS . " WHERE user_email ='".fun_db_input($username)."' AND user_encrypt_pwd  ='".fun_db_input($pwd)."' ";
		  $result = $this->dbObj->fun_db_query($sqlCheck) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
		  $rowsCategory =  $this->dbObj->fun_db_fetch_rs_object($result);
		if($this->fun_get_num_rows($sqlCheck)){
				$_SESSION['session_username'] = $username;
				$_SESSION['user_id'] = $rowsCategory->user_id;
				$scExists = 1;
			}
			return $scExists;
	}
	function processUserWedMembers(){

		$locArray = array(
						"best_man" => $_POST['best_man'],
						"father_of_bride" => $_POST['father_of_bride'],
						"groom" => $_POST['groom'],
						"bride" => $_POST['bride'],
						"mother_groom" => $_POST['mother_groom'],
						"mother_bride" => $_POST['mother_bride'],
						"father_groom" => $_POST['father_groom'],
						"brides_maid_1" => $_POST['brides_maid_1'],
						"brides_maid_2" => $_POST['brides_maid_2'],
						"groom_brother_1"  => $_POST['groom_brother_1'],
						"groom_brother_2"  => $_POST['groom_brother_2'],
						"bride_grandma"  => $_POST['bride_grandma'],
						"last_modified" => date("Y-m-d H:i:s")
					);
		
		 $sqlWedParty = "SELECT * FROM " . TABLE_USER_WED_PARTY . " WHERE user_id='".(int)$_SESSION['user_id']."'";
		if($this->fun_get_num_rows($sqlWedParty)>0){
			
			$fields = "";
			$fieldsVal = "";
			foreach($locArray as $keys => $vals){
				$fields .= $keys . "='" . fun_db_input($vals). "', ";
			}
			$fields = trim($fields);
			if($fields!=""){
				$fields = substr($fields,0,strlen($fields)-1);
				 $sqlUpdate = "UPDATE " . TABLE_USER_WED_PARTY . " SET " . $fields . " WHERE user_id='".(int)$_SESSION['user_id']."'";
				$this->dbObj->fun_db_query($sqlUpdate) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
 	
				return $this->dbObj->fun_db_get_affected_rows();
			}
		}
		
		else{
			$fields = "";
			$fieldsVal = "";
			foreach($locArray as $keys => $vals){
				$fields .= $keys . ", ";
				$fieldsVal .= "'" . fun_db_input($vals). "', ";
			}
			
			$sqlInsertFeature = "INSERT INTO " . TABLE_USER_WED_PARTY . "(wedding_party_id,user_id, ".$fields." added_date) " ;
			 $sqlInsertFeature .= " VALUES(null,'".$_SESSION['user_id']."', ".$fieldsVal." '".date("Y-m-d H:i:s")."')";
			$result=$this->dbObj->fun_db_query($sqlInsertFeature) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
			
			return $this->dbObj->fun_db_get_affected_rows();
		}
	}
function ModifyUserPayment($userID,$day)
	{
		$rightnow = date('Y-m-d');
		$adddays = date('Y-m-d', strtotime('+'.$day.' days'));
		$cateArray = array(
						"expired_on" => $adddays,
						"status" => 1,
					);
	
			
			$fields = "";
			$fieldsVal = "";
			foreach($cateArray as $keys => $vals){
				$fields .= $keys . "='" . fun_db_input($vals). "', ";
			}
			$fields = trim($fields);
			if($fields!=""){
				$fields = substr($fields,0,strlen($fields)-1);
				  $sqlUpdate = "UPDATE " . TABLE_USERS . " SET total_orders = total_orders+1, " . $fields . " WHERE user_id ='".(int)$userID."'";
				$this->dbObj->fun_db_query($sqlUpdate) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
				return $this->dbObj->fun_db_get_affected_rows();
		}
		
	}
	function funIsCustomerEmailExists($email){
		$scExists = false;
		if($email){
			 $sql = "SELECT * FROM ".TABLE_USERS."  WHERE user_email='" . $email."'";
			if($this->fun_get_num_rows($sql)){
				$scExists = true;
			}
		}
		return $scExists;
	}
	function funGetCustomerLoginInfoByEmail($email){
		$cateArray = array();
		 $sql = "SELECT * FROM " . TABLE_USERS . " WHERE user_email ='".$email."'";
		
		$result = $this->dbObj->fun_db_query($sql) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
		if(!$result || $this->dbObj->fun_db_get_num_rows($result) < 1){
			return; // user does not exists
		}
		$rowsCategory =  $this->dbObj->fun_db_fetch_rs_object($result);
		$cateArray = array(
							"user_id" => fun_db_output($rowsCategory->user_id),
							"user_title" => fun_db_output($rowsCategory->user_title),
							"user_fname" => fun_db_output($rowsCategory->user_fname),
							"user_lname" => fun_db_output($rowsCategory->user_lname),
							"user_wed_date" => fun_db_output($rowsCategory->user_wed_date),
							"website_id" => fun_db_output($rowsCategory->website_id),
							"expired_on" => fun_db_output($rowsCategory->expired_on),
							"user_role" => fun_db_output($rowsCategory->user_role),
							"user_email" => fun_db_output($rowsCategory->user_email),
							"total_orders" => fun_db_output($rowsCategory->total_orders),
							"last_login" => fun_db_output($rowsCategory->last_login),
							"status" => fun_db_output($rowsCategory->status),
							"added_date" => fun_db_output($rowsCategory->added_date)
						 );
		return $cateArray;
	}
	function editChangePwd($username,$pwd)
	{
	 $sqlUpdate = "UPDATE " . TABLE_USERS . " SET user_encrypt_pwd='".md5($pwd)."', user_pwd ='".($pwd)."' WHERE user_email='".$username."'";
	 $this->dbObj->fun_db_query($sqlUpdate) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
	return $this->dbObj->fun_db_get_affected_rows();
				
}

	function fun_get_num_rows($sql){
		$totalRows = 0;
		$selected = "";
		$sql = trim($sql);
		if($sql==""){
			die("<font color='#ff0000' face='verdana' face='2'>Error: Query is Empty!</font>");
			exit;
		}
		$result = $this->dbObj->fun_db_query($sql);
		$totalRows = $this->dbObj->fun_db_get_num_rows($result);
		$this->dbObj->fun_db_free_resultset($result);
		return $totalRows;
	}

}
?>