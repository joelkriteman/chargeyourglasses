<?php
class Orders{
	var $dbObj;
	
	function Orders(){ // class constructor
		$this->dbObj = new DB();
		$this->dbObj->fun_db_connect();
	} 
	function processOrder($userId){  //for fb users and renew order
	
		$booking_request_date=$_POST['user_wed_date'];
		$booking_date=explode('/',$booking_request_date);
		$orderNumber="CYG-".$booking_date[2].$booking_date[1]."-0".rand(100,10000);
		$locArray = array(
						"order_number" => $orderNumber,
						"payment_status_id" => 0,
						"total_amount" => $_POST['total_amount'],
						"website_id" => ($_POST['website_id']),
						"order_status" => 0
					);
			$fields = "";
			$fieldsVal = "";
			foreach($locArray as $keys => $vals){
				$fields .= $keys . ", ";
				$fieldsVal .= "'" . fun_db_input($vals). "', ";
			}
			
			$sqlInsertFeature = "INSERT INTO " . TABLE_ORDERS . "(order_id,user_id, ".$fields." added_date) " ;
			 $sqlInsertFeature .= " VALUES(null,".$userId.", ".$fieldsVal." '".date("Y-m-d H:i:s")."')";
			$result=$this->dbObj->fun_db_query($sqlInsertFeature) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Users table.</font>");
			
			return $this->dbObj->fun_db_last_inserted_id($result);
		}
	function processRenewOrder($userId,$websiteId,$amount){
	
		$orderNumber="CYG-".date("Y-m-d")."-0".rand(100,10000);
		$locArray = array(
						"order_number" => $orderNumber,
						"payment_status_id" => 0,
						"total_amount" => $amount,
						"website_id" => $websiteId,
						"order_status" => 0
					);
			$fields = "";
			$fieldsVal = "";
			foreach($locArray as $keys => $vals){
				$fields .= $keys . ", ";
				$fieldsVal .= "'" . fun_db_input($vals). "', ";
			}
			
			$sqlInsertFeature = "INSERT INTO " . TABLE_ORDERS . "(order_id,user_id, ".$fields." added_date) " ;
			 $sqlInsertFeature .= " VALUES(null,".$userId.", ".$fieldsVal." '".date("Y-m-d H:i:s")."')";
			$result=$this->dbObj->fun_db_query($sqlInsertFeature) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Users table.</font>");
			
			return $this->dbObj->fun_db_last_inserted_id($result);
		}

	function funGetOrderInfo($cateID){
		$locArray = array();
		$sql = "SELECT * FROM " . TABLE_ORDERS . " WHERE order_id='".(int)$cateID."'";
		
		$result = $this->dbObj->fun_db_query($sql) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
		if(!$result || $this->dbObj->fun_db_get_num_rows($result) < 1){
			return; // user does not exists
		}
		$rowsCategory =  $this->dbObj->fun_db_fetch_rs_object($result);
		$locArray = array(
							"order_id" => fun_db_output($rowsCategory->order_id),
							"user_id" => fun_db_output($rowsCategory->user_id),
							"order_number" => fun_db_output($rowsCategory->order_number),
							"payment_status_id" => fun_db_output($rowsCategory->payment_status_id),
							"total_amount" => fun_db_output($rowsCategory->total_amount),
							"transactionid" => fun_db_output($rowsCategory->transactionid),
							"mail_sent" => fun_db_output($rowsCategory->mail_sent),
							"website_id" => fun_db_output($rowsCategory->website_id),
							"order_status" => fun_db_output($rowsCategory->order_status),
							"added_date" => fun_db_output($rowsCategory->added_date)
						 );
		return $locArray;
	}
	
function OrderPaymentModify($bookingID,$transactionid)
	{
		
		$cateArray = array(
						"payment_status_id" => 1,
						"order_status" => 1,
						"transactionid" => $transactionid,
					);
	
			
			$fields = "";
			$fieldsVal = "";
			foreach($cateArray as $keys => $vals){
				$fields .= $keys . "='" . fun_db_input($vals). "', ";
			}
			$fields = trim($fields);
			if($fields!=""){
				$fields = substr($fields,0,strlen($fields)-1);
				  $sqlUpdate = "UPDATE " . TABLE_ORDERS . " SET " . $fields . " WHERE order_id ='".(int)$bookingID."'";
				$this->dbObj->fun_db_query($sqlUpdate) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
				return $this->dbObj->fun_db_get_affected_rows();
		}
		
	}
function OrderMailStatusModify($bookingID)
	{
		
		$cateArray = array(
						"mail_sent" => 'yes',
					);
	
			
			$fields = "";
			$fieldsVal = "";
			foreach($cateArray as $keys => $vals){
				$fields .= $keys . "='" . fun_db_input($vals). "', ";
			}
			$fields = trim($fields);
			if($fields!=""){
				$fields = substr($fields,0,strlen($fields)-1);
				  $sqlUpdate = "UPDATE " . TABLE_ORDERS . " SET " . $fields . " WHERE order_id ='".(int)$bookingID."'";
				$this->dbObj->fun_db_query($sqlUpdate) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
				return $this->dbObj->fun_db_get_affected_rows();
		}
		
	}

}
?>