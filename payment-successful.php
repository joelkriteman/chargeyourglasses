<?php
require_once("includes/application-top.php");
$pp_hostname = "www.paypal.com"; // Change to www.sandbox.paypal.com to test against sandbox
// read the post from PayPal system and add 'cmd'
$req = 'cmd=_notify-synch';

$tx_token = $_GET['tx'];
$auth_token = "dJnDmYwhHqJAx9iuW13GGwFPTArHmCv-HgCfqYwTTod0TMztPtk2aiPOgzm";
$req .= "&tx=$tx_token&at=$auth_token";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://$pp_hostname/cgi-bin/webscr");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
//set cacert.pem verisign certificate path in curl using 'CURLOPT_CAINFO' field here,
//if your server does not bundled with default verisign certificates.
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: $pp_hostname"));
$res = curl_exec($ch);
curl_close($ch);

if(!$res){
	//HTTP ERROR
}else{
	// parse the data
	$lines = explode("\n", $res);
	$keyarray = array();
	if (strcmp ($lines[0], "SUCCESS") == 0) {
		for ($i=1; $i<count($lines);$i++){
			list($key,$val) = explode("=", $lines[$i]);
			$keyarray[urldecode($key)] = urldecode($val);
		}

		$firstname = $keyarray['first_name'];
		$lastname = $keyarray['last_name'];
		$payer_email = $keyarray['payer_email'];
		$amount = $keyarray['mc_gross'];
		//$tax = $keyarray['tax'];
		//$discount = $keyarray['discount'];
		$payment_date = $keyarray['payment_date'];
		$payment_status = $keyarray['payment_status'];
		$payment_type = $keyarray['payment_type'];
		$pending_reason = $keyarray['pending_reason'];
		$mc_currency = $keyarray['mc_currency'];
		$transactionid = $keyarray['txn_id'];
		$orderId = base64_decode($keyarray['custom']);
		$backUrl = $keyarray['back_url'];
		 
		$orderDetails = $OrderObj->funGetOrderInfo($orderId);
		$day = $generalInfoDetail['time_days'];

		if($payment_status=="Completed"){
			$orderUpdate = $OrderObj->OrderPaymentModify($orderId,$transactionid);
			$userDetailsUpdate = $customerobj->ModifyUserPayment($orderDetails['user_id'],$day);
			$sqlInsertFeature = "INSERT INTO " . TABLE_SUCCESSFUL_ORDERS . "(success_order_id,order_id,first_name,last_name,payer_email,amount,payment_status,payment_type,payment_date) " ;
			$sqlInsertFeature .= " VALUES(null,'".$orderId."', '".$firstname."','".$lastname."','".$payer_email."','".$amount."','".$payment_status."','".$payment_type."','".$payment_date."')";
			$result=$dbObj->fun_db_query($sqlInsertFeature) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Users table.</font>");
		}

		if($orderDetails['website_id']==2){
			redirectURL(SITE_URL."groomlist/payments-successful.php?key=".md5("SUCCESSFULL PAYMENT")."&order_id=".$keyarray['custom']);
		} elseif ($orderDetails['website_id']==3){
			redirectURL(SITE_URL."confetti/payments-successful.php?key=".md5("SUCCESSFULL PAYMENT")."&order_id=".$keyarray['custom']);
		} else {
			redirectURL(SITE_URL."payments-successful.php?key=".md5("SUCCESSFULL PAYMENT")."&order_id=".$keyarray['custom']);
		}
	}
	else if (strcmp ($lines[0], "FAIL") == 0) {
		// log for manual investigation
	}
}

?>