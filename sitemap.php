<?php include_once("includes/header.php");
$pageId=8;
$sql="SELECT * FROM ".TABLE_PAGES." WHERE status='1' AND pages_id='".$pageId."'";
$result=mysql_query($sql);
$rowsResult=mysql_fetch_array($result);

$sqlFeedback="SELECT * FROM ".TABLE_FEEDBACKS." WHERE status='1' AND home_page_status='1' ORDER BY rand()";
$resultFeedback=mysql_query($sqlFeedback);
?>
<!--header end here-->

<section id="breadcrumbs" class="alpha-ver15">
  <div class="col_1280">
    <ul>
      <li><a href="<?php echo SITE_URL;?>">Home</a></li>
      <li>&gt;</li>
      <li>Sitemap</li>
    </ul>
  </div>
</section>
<section class="content-outer alpha-ver15">
  <div class="col_1280 about">
    <aside id="main-content" class="alignleft">
     <?php include_once("includes/subscribe-msg.php");?>
      <div class="sub-heading">
        <h1>Sitemap</h1>
      </div>
      <div class="dashed-border"></div>
      <aside class="sitemap-outer">
        <ul>
          <li><a href="<?php echo SITE_URL;?>">Home</a></li>
          <li><a href="<?php echo SITE_URL;?>about-us.php">About Us</a></li>
          <li><strong>Speech For</strong>
            <ul>
              <li><a href="<?php echo SITE_URL;?>father-of-the-bride-wedding-speeches.php">Father of the Bride</a></li>
              <li><a href="<?php echo SITE_URL;?>groom-wedding-speeches.php">Are you a Groom</a></li>
              <li><a href="<?php echo SITE_URL;?>best-man-wedding-speeches.php">Are you Best Man</a></li>
            </ul>
          </li>
          <li><a href="<?php echo SITE_URL;?>testmonials.php">Testimonials</a></li>
          <li><a href="<?php echo SITE_URL;?>contact.php">Contact Us</a></li>
           <li><strong>Take A Quiz</strong>
             <ul> <li><a href="<?php echo SITE_URL;?>how-well-do-you-know-the-groom-quiz.php">How well do you know the Groom?</a></li>
          <li><a href="<?php echo SITE_URL;?>how-well-do-you-know-your-bride-quiz.php">How well do you know your bride-to-be?</a></li>
          <li><a href="<?php echo SITE_URL;?>is-he-good-enough-for-your-daughter-quiz.php">Is he good enough for your daughter?</a></li></ul>
           </li>
         
          <li><a href="<?php echo SITE_URL;?>terms-condition.php">Terms and Conditions</a></li>
          <li><a href="<?php echo SITE_URL;?>privacy.php">Privacy</a></li>
<!--          <li><a href="<?php echo SITE_URL;?>">Login</a></li>
          <li><a href="<?php echo SITE_URL;?>">Register</a></li>
-->        </ul>
      </aside>
    </aside>
    <?php include_once("includes/sidebar.php");?>
  </div>
</section>
<?php include_once("includes/footer.php");?>
