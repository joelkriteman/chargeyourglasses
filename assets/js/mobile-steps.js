(function($) {

  var CYG = {};

  CYG.MOBILE_MAX_WIDTH = 785;



  CYG.step = function(element, index, callback) {

    var $element;


    function init() {
      $element = $(element);
      $element.on('click', '.step-links a', clickHandler);
    }

    function clickHandler() {
      callback(index);
    }

    init();


    return {
      getElement: function() { return $element; }
    };

  };



  CYG.steps = (function() {

    var steps = [];
    var stepsCount;
    var $scrollable;
    var $window;



    function init(container) {
      $(container).find('aside').each(initStep);
      stepsCount = steps.length;
      $scrollable = $('html, body');
      $window = $(window);
    }

    function initStep(index, element) {
      steps.push(CYG.step(element, index, linkClickHandler));
    }

    function getIsMobile() {
      return $window.width() <= CYG.MOBILE_MAX_WIDTH;
    }

    function linkClickHandler(index) {
      if(!getIsMobile()) { return; }
      var nextIndex = index + 1;
      if(nextIndex < stepsCount) {
        var $nextStep = steps[index+1].getElement();
        var y = $nextStep.offset().top;
        $scrollable.animate({ scrollTop: y }, 'swing');
      }
    }



    return {
      init: init
    };

  }());



  $(function() {
    var container = document.getElementById('js-steps');
    if(container) { CYG.steps.init(container); }
  });

}( jQuery ));