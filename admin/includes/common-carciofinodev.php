<?php
define("SITE_NAME", "Charge Your Glasses");
define("SITE_FULL_NAME", "Charge Your Glasses");

if($_SERVER["SERVER_NAME"]=="localhost"){
	define("MAIN_URL", "http://localhost");
	define("SITE_URL", "http://localhost/akhil_projects/cyg/demo/");
	define("SITE_SECURE_URL", "http://localhost/akhil_projects/cyg/demo/");
	define("SITE_ADMIN_URL", "http://localhost/akhil_projects/cyg/admin/");
	define("SITE_ADMIN_SECURE_URL", "http://localhost/akhil_projects/cyg/admin/");
	define("SITE_DOC_ROOT", "/Applications/XAMPP/xamppfiles/htdocs/akhil_projects/cyg/demo/");
}
else{
	define("MAIN_URL", "http://cyg.carciofinodev.co.uk/");
	define("SITE_URL", "http://cyg.carciofinodev.co.uk/");
	define("SITE_SECURE_URL", "http://cyg.carciofinodev.co.uk/");
	define("SITE_ADMIN_URL", "http://cyg.carciofinodev.co.uk/admin/");
	define("SITE_ADMIN_SECURE_URL", "http://cyg.carciofinodev.co.uk/admin/");
	define("SITE_DOC_ROOT", $_SERVER['DOCUMENT_ROOT']);
}

define("SITE_IMAGES", SITE_URL . "images/");
define("SITE_IMAGES_BANNER", SITE_IMAGES . "banner/");
define("SITE_PDF", SITE_URL . "pdf/");
define("SITE_EMAIL_TAMPLATE", SITE_URL . "email-template/");

define("SITE_EMAIL_TAMPLATE_WS", SITE_DOC_ROOT . "email-template/");
define("EMAIL_LOGO", SITE_EMAIL_TAMPLATE."cyg_logo.jpg");
define("SITE_PAYMENT_EMAIL_ID", 'enquiry@chargeyourglasses.com,katie@greatspeechwriting.co.uk,nikhil@isglobalweb.com');


define("SITE_IMAGES_WS", SITE_DOC_ROOT . "images/");
define("SITE_IMAGES_BANNER_WS", SITE_IMAGES_WS . "banner/");
define("SITE_IMAGES_PDF_WS", SITE_DOC_ROOT . "pdf/");


define("EMAIL_ID_REG_EXP_PATTERN", "/^[A-Za-z0-9-_]+(\.[A-Za-z0-9-_]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$/");

define("SITE_INFO_EMAIL_ID", 'chargeyourglassescontact@gmail.com,enquiry@chargeyourglasses.com,katie@greatspeechwriting.co.uk,nikhil@isglobalweb.com');
define("SITE_SUPPORT_EMAIL_ID", 'tom@chargeyourglasses.com');
define("SITE_RETURN_EMAIL_ID", "nikhil@isglobalweb.com");

define("SITE_COOKIE_EXPIRATION_DAY", 7);
define("NO_IMAGE_FILE", "nofound.gif");
define("STATUS_ACTIVE", "<font color='#006600'>Active</font>");
define("STATUS_INACTIVE", "<font color='#FF0000'>In-Active</font>");

?>
