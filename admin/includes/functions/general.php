<?php
function redirectURL($rurl){
	header("Location: " . $rurl);
	exit;
}
function fun_db_output($str){
	return stripslashes($str);
}
function fun_db_input($str){
	$str = trim($str);
	if(!get_magic_quotes_gpc()){
		return addslashes($str);
	}else{
		return $str;
	}
}


function fun_get_commas_values($str){ // if ,4,2,3,6, will be converted to 4,2,3,6
	$newStr = "";
	$str = trim($str);
	if(str!="" && strlen($str) > 2){
		$newStr = substr($str,1,strlen($str)-2);
	}
	return $newStr;
}

function fun_site_date_format($strDate){
	$dateFormat = "";
	if($strDate!=""){
		$dateFormat = date("d M, Y", strtotime($strDate));
	}
	return $dateFormat;
}
function fun_site_date_format_for_email($strDate){
	$dateFormat = "";
	if($strDate!=""){
		$dateFormat = date("d/m/Y", strtotime($strDate));
	}
	return $dateFormat;
}

function fun_currency_format($curr=0){
	return number_format($curr, 2);
}
function fun_check_date($yyyy, $mm, $dd){
	$dateCode = array();
	if($mm < 1 || $mm > 12){
		$dateCode['code'] = false;
		$dateCode['codemsg'] = "The month date must be between 1 and 12!";
		return $dateCode;
	}
	if($dd < 1 || $dd > 31){
		$dateCode['code'] = false;
		$dateCode['codemsg'] = "The day date must be between 1 and 31!";
		return $dateCode;
	}
	if($dd==31 && ($mm==4 || $mm==6 || $mm==9 || $mm==11)){
		$dateCode['code'] = false;
		$dateCode['codemsg'] = "The month for your date doesn't have 31 days!";
		return $dateCode;
	}
	if($mm==2){
		$learYear = false;
		if($yyyy % 4 == 0 && ($yyyy % 100 != 0 || $yyyy % 400 == 0)){
			$learYear = true;
		}
		if($dd > 29 || ($dd==29 && !$learYear)){
			$dateCode['code'] = false;
			$dateCode['codemsg'] = "The month for your date doesn't have ".$dd." days for year ".$yyyy."!";
			return $dateCode;
		}
	}
	$dateCode['code'] = true;
	$dateCode['codemsg'] = "";
	return $dateCode;
}
function fun_create_number_options($startVal=0, $endVal=0, $selVal=''){
	$selected = "";
	for($i=$startVal; $i <= $endVal; $i++){
		if($i == $selVal && $selVal!=''){
			$selected = " selected";
		}else{
			$selected = "";
		}
		echo "<option value=\"".$i."\" ".$selected.">" . $i . "</option>\n";
	}
}
function fun_created_month_option($selVal=''){
	$monthsArray = array();
	$monthsArray['1'] = "January";
	$monthsArray['2'] = "February";
	$monthsArray['3'] = "March";
	$monthsArray['4'] = "April";
	$monthsArray['5'] = "May";
	$monthsArray['6'] = "June";
	$monthsArray['7'] = "July";
	$monthsArray['8'] = "August";
	$monthsArray['9'] = "September";
	$monthsArray['10'] = "October";
	$monthsArray['11'] = "November";
	$monthsArray['12'] = "December";
	foreach($monthsArray as $keys => $vals){
		if($keys == $selVal){
			$selected = " selected";
		}else{
			$selected = "";
		}
		echo "<option value=\"".$keys."\" ".$selected.">" . $vals . "</option>\n";
	}
}

function fun_created_month_option_in_Number($selVal=''){
	$monthsArray = array();
	$monthsArray['01'] = "01";
	$monthsArray['02'] = "02";
	$monthsArray['03'] = "03";
	$monthsArray['04'] = "04";
	$monthsArray['05'] = "05";
	$monthsArray['06'] = "06";
	$monthsArray['07'] = "07";
	$monthsArray['08'] = "08";
	$monthsArray['09'] = "09";
	$monthsArray['10'] = "10";
	$monthsArray['11'] = "11";
	$monthsArray['12'] = "12";
	foreach($monthsArray as $keys => $vals){
		if($keys == $selVal){
			$selected = " selected";
		}else{
			$selected = "";
		}
		echo "<option value=\"".$keys."\" ".$selected.">" . $vals . "</option>\n";
	}
}

function fun_created_year_option($selVal=''){
	$TodayYear= date('Y');
     $endYear=  date('Y', strtotime('+10 year'));
	 while($endYear>$TodayYear)
	 {
		 if($TodayYear == $selVal){
			$selected = " selected";
		}else{
			$selected = "";
		}
		 echo "<option value=\"".$TodayYear."\" ".$selected.">" . $TodayYear . "</option>\n";
		 $TodayYear=$TodayYear+1;
	 }
}
function fun_created_year_report_option($selVal=''){
	$TodayYear= date('Y');
	$endYear1=  date('Y', strtotime('-5 year'));
	 while($TodayYear>$endYear1)
	 {
		 if($endYear1 == $selVal){
			$selected = " selected";
		}else{
			$selected = "";
		}
		 $TodayYear1 .= "<option value=\"".$endYear1."\" ".$selected.">" . $endYear1 . "</option>\n";
		 $endYear1=$endYear1+1;
	 }
     $endYear=  date('Y', strtotime('+6 year'));
	 while($endYear>$TodayYear)
	 {
		 if($TodayYear == $selVal){
			$selected = " selected";
		}else{
			$selected = "";
		}
		 $todayYear2 .= "<option value=\"".$TodayYear."\" ".$selected.">" . $TodayYear . "</option>\n";
		 $TodayYear=$TodayYear+1;
	 }
	 echo $TodayYear1.$todayYear2;
}

	
function fun_Payment_Start_year_option($selVal=''){
	
	$TodayYear= date('Y');
     $endYear=  date('Y', strtotime('-10 year'));
	 while($TodayYear>$endYear)
	 {
		 if($endYear == $selVal){
			$selected = " selected";
		}else{
			$selected = "";
		}
		 echo "<option value=\"".$endYear."\" ".$selected.">" . $endYear . "</option>\n";
		 $endYear=$endYear+1;
	 }
			
	
}
function fun_getFileContent($fileName){
	$fileContent = "";
	
	$fp = fopen($fileName, "r");
	if($fp){
		$fileContent = fread($fp, filesize($fileName));
	}
	fclose($fp);
	return $fileContent;
}

function trimBodyText($theText, $lmt=70, $s_chr="\n", $s_cnt=1){
	  $pos = 0;
	  $trimmed = FALSE;
	  for($i=0; $i <= $s_cnt; $i++){
		  if($tmp = strpos($theText, $s_chr, $pos)){
			  $pos = $tmp;
			  $trimmed = TRUE;
		  }else{
			  $pos = strlen($theText);
			  $trimmed = FALSE;
			  break;
		  }
	  }
	  $theText = substr($theText, 0, $pos);
	  if(strlen($theText) > $lmt){
		  $theText = substr($theText, 0, $lmt);
		  $theText = substr($theText, 0, strrpos($theText, ' '));
		  $trimmed = TRUE;
	  }
	  if($trimmed){
		  $theText .= "...";
	  }
	  return $theText;
  }

function paginate($limit, $tot_rows){
	if($limit=="")
	{
	$limit=10;
	}
	 $numrows = $tot_rows;
	$pagelinks = "<div class=\"pagelinks\">";
	if($numrows > $limit){
		if(isset($_GET['page'])){
			$page = $_GET['page'];
		}else{
			$page = 1;
		}

		$currpage = $_SERVER['PHP_SELF'] . "?" . $_SERVER['QUERY_STRING'];
		$currpage = str_replace("&page=".$page,"",$currpage);

		if($page == 1){
			$pagelinks .= "<span class=\"pagelinks\">&lt; PREV </span>";
		}else{
			$pageprev = $page - 1;
			$pagelinks .= "<a class=\"pagelinks\" href=\"" . $currpage . "&amp;page=". $pageprev . "\">&lt; PREV </a>";
		}

		 $numofpages = ceil($numrows / $limit);
		$range = 7;
		$lrange = max(1, $page-(($range-1)/2));
		$rrange = min($numofpages, $page+(($range-1)/2));
		if(($rrange - $lrange) < ($range - 1)){
			if($lrange == 1){
				$rrange = min($lrange + ($range-1), $numofpages);
			}else{
				$lrange = max($rrange - ($range-1), 0);
			}
		}
		
		if($lrange > 1){
			$pagelinks .= " .. ";
		}else{
			$pagelinks .= " &nbsp;&nbsp; ";
		}
		for($i = 1; $i <= $numofpages; $i++){
			if($i == $page){
				$pagelinks .= "<span class=\"currentpagelinks\">$i</span>";
			}else{
				if($lrange <= $i && $i <= $rrange){
					$pagelinks .= " <a class=\"pagelinks\" href=\"".$currpage."&amp;page=".$i."\">" . $i . "</a>  ";
				}
			}
		}
		
		if($rrange < $numofpages){
			$pagelinks .= " .. ";
		}else{
			$pagelinks .= " &nbsp;&nbsp; ";
		}

		if(($numrows - ($limit * $page)) > 0){
			$pagenext = $page + 1;
			$pagelinks .= "<a class=\"pagelinks\" href=\"". $currpage . "&amp;page=" . $pagenext . "\"> NEXT &gt;</a>";
		}else{
			$pagelinks .= " <span class=\"taxtxt\"> NEXT &gt;</span>";
		}

	}else{
		//$pagelinks .= "<span class=\"pagelinks\">&lt; PREV</span>&nbsp;&nbsp;";
		//$pagelinks .= "<span class=\"pagelinks\">&nbsp;&nbsp;&nbsp;NEXT &gt;</span>&nbsp;&nbsp;";
	}

	$pagelinks .= "</div>";	
  	
return $pagelinks;
}

function paginateList($limit, $tot_rows){
	if($limit=="")
	{
	$limit=10;
	}
	 $numrows = $tot_rows;
	$pagelinks = "<div class=\"pagination\"> Page";
	if($numrows > $limit){
		if(isset($_GET['page'])){
			$page = $_GET['page'];
		}else{
			$page = 1;
		}

		$currpage = $_SERVER['PHP_SELF'] . "?" . $_SERVER['QUERY_STRING'];
		$currpage = str_replace("&page=".$page,"",$currpage);

		if($page == 1){
			$pagelinks .= "<span class=\"pagelinks\">&lt; PREV </span>";
		}else{
			$pageprev = $page - 1;
			$pagelinks .= "<a class=\"pagelinks\" href=\"" . $currpage . "&page=". $pageprev . "\">&lt; PREV </a>";
		}

		$numofpages = ceil($numrows / $limit);
		$range = 3;
		$lrange = max(1, $page-(($range-1)/2));
		$rrange = min($numofpages, $page+(($range-1)/2));
		if(($rrange - $lrange) < ($range - 1)){
			if($lrange == 1){
				$rrange = min($lrange + ($range-1), $numofpages);
			}else{
				$lrange = max($rrange - ($range-1), 0);
			}
		}
		
		if($lrange > 1){
			$pagelinks .= " .. ";
		}else{
			$pagelinks .= " &nbsp;&nbsp; ";
		}
		for($i = 1; $i <= $numofpages; $i++){
			if($i == $page){
				$pagelinks .= "<span class=\"currentpagelinks\">$i</span>";
			}else{
				if($lrange <= $i && $i <= $rrange){
					$pagelinks .= " <a class=\"pagelinks\" href=\"".$currpage."&page=".$i."\">" . $i . "</a>  ";
				}
			}
		}
		
		if($rrange < $numofpages){
			$pagelinks .= " .. ";
		}else{
			$pagelinks .= " &nbsp;&nbsp; ";
		}

		if(($numrows - ($limit * $page)) > 0){
			$pagenext = $page + 1;
			$pagelinks .= "<a class=\"pagelinks\" href=\"". $currpage . "&page=" . $pagenext . "\"> NEXT &gt;</a>";
		}else{
			$pagelinks .= " <span class=\"taxtxt\"> NEXT &gt;</span>";
		}
	}else{
		//$pagelinks .= "<span class=\"pagelinks\">&lt; PREV</span>&nbsp;&nbsp;";
		//$pagelinks .= "<span class=\"pagelinks\">&nbsp;&nbsp;&nbsp;NEXT &gt;</span>&nbsp;&nbsp;";
	}
	$pagelinks .= "</div>";		
return $pagelinks;
}




function fun_mail($to,$subject,$info_detail, $from)
{
$chk  = 'MIME-Version: 1.0' . "\r\n";
$chk .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$chk .= 'From:'.$from.' \r\n';
@mail($to , $subject, $info_detail, $chk);	
}
function fun_admin_user_type_array(){
	$auType = array(
					"1" => "Super Admin",
					"3"=> "Provider",
					"2" => "System Operator"
				);
	return $auType;
}		

function fun_get_user_type_option($uTypeNo=''){
	$userTypeArray = fun_admin_user_type_array();
	foreach($userTypeArray as $keys => $vals){
		if($keys==$uTypeNo){
			$selected = " selected";
		}else{
			$selected = "";
		}
		echo "<option value=\"".$keys."\" ".$selected.">";
		echo $vals;
		echo "</option>\n";
	}
}

function fun_get_user_type_name($uTypeNo=''){
	$userTypeArray = fun_admin_user_type_array();
	$userTypeName = "";
	foreach($userTypeArray as $keys => $vals){
		if($keys==$uTypeNo){
			$userTypeName = $vals;
		}
	}
	return $userTypeName;
}

function fun_cus_title_option($title){
	echo "<option value=\"Mr.\"";
	if($title=="Mr."){
		echo " selected";
	}
	echo ">Mr.</option>\n";
	
	echo "<option value=\"Mrs.\"";
	if($title=="Mrs."){
		echo " selected";
	}
	echo ">Mrs.</option>\n";
	
	echo "<option value=\"Miss\"";
	if($title=="Miss"){
		echo " selected";
	}
	echo ">Miss</option>\n";
	
	echo "<option value=\"Dr.\"";
	if($title=="Dr."){
		echo " selected";
	}
	echo ">Dr.</option>\n";
}



function funOrderStatusArray(){
	$osArray = array(
					OS_PENDING => "Pending",
					OS_CONFIRM => "In progress",
					//OS_DELIVER => "Completed",
					//OS_SHIPPED => "Shipped",
					OS_COMPLETE => "Completed",
					//OS_CANCELLED => "Cancelled",
				);
	return $osArray;
}
function funOrderStatusOption($osNo){
	$osArray = funOrderStatusArray();
	foreach($osArray as $keys => $vals){
		if((int)$keys==(int)$osNo){
			$selected = " selected";
		}else{
			$selected = "";
		}
		echo "<option value=\"".$keys."\" ".$selected.">";
		echo $vals;
		echo "</option>\n";
	}
}
function funOrderStatusName($osNo){
	$osArray = funOrderStatusArray();
	$osName = "";
	foreach($osArray as $keys => $vals){
		if((int)$keys==(int)$osNo){
			$osName = $vals;
		}
	}
	return $osName;
}


function funPaymentStatusArray(){
	$psArray = array(
					PS_PENDING => "Not Paid",
					PS_CONFIRM => "Paid",
					//PS_INPROCESS => "In process",
					//PS_CLEAR => "Cleared",
					//PS_CANCELLED => "Cancelled",
				);
	return $psArray;
}
function funPaymentStatusOption($psNo){
	$psArray = funPaymentStatusArray();
	foreach($psArray as $keys => $vals){
		if((int)$keys==(int)$psNo){
			$selected = " selected";
		}else{
			$selected = "";
		}
		echo "<option value=\"".$keys."\" ".$selected.">";
		echo $vals;
		echo "</option>\n";
	}
}
function funPaymentStatusName($psNo){
	$psArray = funPaymentStatusArray();
	$psName = "";
	foreach($psArray as $keys => $vals){
		if((int)$keys==(int)$psNo){
			$psName = $vals;
		}
	}
	return $psName;
}

function funOrderStatusColorArray(){
	$osColorArray = array(
					OS_PENDING => "#ff0000",
					OS_CONFIRM => "#336600",
					//OS_SHIPPED => "#0033CC",
					OS_COMPLETE => "#993399",
					//OS_CANCELLED => "#000000"
	);
	return $osColorArray;
}
function funOrderStatusColor($osNo){
	$ocArray = funOrderStatusColorArray();
	$colorName = "";
	foreach($ocArray as $keys => $vals){
		if((int)$keys==(int)$osNo){
			$colorName = $vals;
		}
	}
	return $colorName;
}

function funPaymentStatusColorArray(){
	$psColorArray = array(
					PS_PENDING => "#ff0000",
					PS_CONFIRM => "#336600",
					//PS_INPROCESS => "#0033CC",
					//PS_CLEAR => "#993399",
					//PS_CANCELLED => "#000000"
	);
	return $psColorArray;
}
function funPaymentStatusColor($psNo){
	$pcArray = funPaymentStatusColorArray();
	$colorName = "";
	foreach($pcArray as $keys => $vals){
		if((int)$keys==(int)$psNo){
			$colorName = $vals;
		}
	}
	return $colorName;
}

function funGetGMTDateTimeDatebase(){

	return gmdate("Y-m-d H:i");

}
function fun_get_email($to,$subject,$content,$from,$fromContent)
{
$bounceEmail=SITE_RETURN_EMAIL_ID;
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= 'From: '.$fromContent.'<' . $from  . "> \r\n";
$headers .= 'Return-Path: ' . $bounceEmail  . " \r\n";
$headers .= 'Errors-To: ' . $bounceEmail  . " \r\n";	
$mailSentStatus = mail($to,$subject,$content,$headers);
return	$mailSentStatus;
}


function funGetGMTDateTimeOutput($strDate){

	$dateFormat = "";

	if($strDate!="" && $strDate!="0000-00-00 00:00:00"){

		$dateFormat = gmdate("D, d M Y H:i", strtotime($strDate));

	}

	return $dateFormat;

}

function funContentStatusArray($status){
	echo "<option value=\"1\"";
	if($status=="1"){
		echo " selected";
	}
	echo ">Missing</option>\n";
	
	echo "<option value=\"2\"";
	if($status=="2"){
		echo " selected";
	}
	echo ">Review</option>\n";
	
	echo "<option value=\"3\"";
	if($status=="3"){
		echo " selected";
	}
	echo ">Completed</option>\n";
}


function strip_only($str)
{
  //$str = strip_tags($str,"<p>,<br>,<a>,<b>,<strong>");
  //$str= preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/i",'<$1$2>', $str);
  $str = str_replace('<p>&nbsp;</p>','',$str);
  return $str;
}
function encrypt_decrypt_extra($str,$ky=''){ 
if($ky=='')return $str; 
$ky=str_replace(chr(32),'',$ky); 
if(strlen($ky)<8)exit('key error'); 
$kl=strlen($ky)<32?strlen($ky):32; 
$k=array();for($i=0;$i<$kl;$i++){ 
$k[$i]=ord($ky{$i})&0x1F;} 
$j=0;for($i=0;$i<strlen($str);$i++){ 
$e=ord($str{$i}); 
$str{$i}=$e&0xE0?chr($e^$k[$j]):chr($e); 
$j++;$j=$j==$kl?0:$j;} 
return $str; 
}

function tooltipInfo($title,$page)
{
 $sqlTip="SELECT * FROM shortlets_tool_tips WHERE title='".$title."' AND page_name='".$page."'";	
 $result=mysql_query($sqlTip);
 $reultTip=mysql_fetch_array($result);
 return $reultTip['description'];
}

		function processAdminLog($action,$pagename='')
		{
			$pagearray=explode('/',$_SERVER['PHP_SELF']);
			$countPageattribute=count($pagearray);
			if($pagename!='')
			{
			$page_name=$pagearray[$countPageattribute-1].$pagename;
			}
			else
			{
			$page_name=$pagearray[$countPageattribute-1];
			}
			$logArray = array(
						"au_id" => $_SESSION['session_admin_userid'],
						"au_type" => $_SESSION['session_admin_usertype'],
						"action" => $action,
						"page_name" => $page_name
					);
	
		
			$fields = "";
			$fieldsVal = "";
			foreach($logArray as $keys => $vals){
				$fields .= $keys . ", ";
				$fieldsVal .= "'" . fun_db_input($vals). "', ";
			}
			$sqlInsert = "INSERT INTO " . TABLE_ADMINS_LOG . "(logs_id, ".$fields." last_modified) " ;
			$sqlInsert .= " VALUES(null, ".$fieldsVal." '".date("Y-m-d H:i:s")."')";
			mysql_query($sqlInsert) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
			//return $dbObj->fun_db_get_affected_rows();
		
		}

?>