<?php
class Users{
	var $dbObj;
	
	function Users(){ // class constructor
		$this->dbObj = new DB();
		$this->dbObj->fun_db_connect();
	} 
	
	function funGetUserInfo($cateID){
		$locArray = array();
		$sql = "SELECT * FROM " . TABLE_USERS . " WHERE user_id='".(int)$cateID."'";
		
		$result = $this->dbObj->fun_db_query($sql) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
		if(!$result || $this->dbObj->fun_db_get_num_rows($result) < 1){
			return; // user does not exists
		}
		$rowsCategory =  $this->dbObj->fun_db_fetch_rs_object($result);
		$locArray = array(
							"user_id" => fun_db_output($rowsCategory->user_id),
							"user_title" => fun_db_output($rowsCategory->user_title),
							"user_fname" => fun_db_output($rowsCategory->user_fname),
							"user_lname" => fun_db_output($rowsCategory->user_lname),
							"user_wed_date" => fun_db_output($rowsCategory->user_wed_date),
							"user_role" => fun_db_output($rowsCategory->user_role),
							"user_email" => fun_db_output($rowsCategory->user_email),
							"total_orders" => fun_db_output($rowsCategory->total_orders),
							"last_login" => fun_db_output($rowsCategory->last_login),
							"status" => fun_db_output($rowsCategory->status),
							"added_date" => fun_db_output($rowsCategory->added_date),
							"expired_on" => fun_db_output($rowsCategory->expired_on),
						 );
		return $locArray;
	}
}
?>