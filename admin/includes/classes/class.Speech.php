<?php
class Speech{
	var $dbObj;
	
	function Speech(){ // class constructor
		$this->dbObj = new DB();
		$this->dbObj->fun_db_connect();
		   		//$this->image = new SimpleImage();

	}
	
	function processSpeech($cateID=0, $actionMode='ADD'){

		if($cateID==""){
			$cateID = 0;
		}
		$locArray = array(
						"speech_section_id" => $_POST['speech_section_id'],
						"speech_text" => $_POST['speech_text'],
						"speech_para_id" => $_POST['speech_para_id'],
						"person_id" => $_POST['person_id'],
						"list_order" => $_POST['list_order'],
						"status"  => $_POST['status'],
						"last_modified" => date("Y-m-d H:i:s")
					);
		
		if($actionMode=='EDIT'){
			
			$fields = "";
			$fieldsVal = "";
			foreach($locArray as $keys => $vals){
				$fields .= $keys . "='" . fun_db_input($vals). "', ";
			}
			$fields = trim($fields);
			if($fields!=""){
				$fields = substr($fields,0,strlen($fields)-1);
				$sqlUpdate = "UPDATE " . TABLE_SPEECH . " SET " . $fields . " WHERE speech_id='".(int)$cateID."'";
				$this->dbObj->fun_db_query($sqlUpdate) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
 	
				return $this->dbObj->fun_db_get_affected_rows();
			}
		}
		
		if($actionMode=='ADD'){
			$fields = "";
			$fieldsVal = "";
			foreach($locArray as $keys => $vals){
				$fields .= $keys . ", ";
				$fieldsVal .= "'" . fun_db_input($vals). "', ";
			}
			
			$sqlInsertFeature = "INSERT INTO " . TABLE_SPEECH . "(speech_id, ".$fields." added_date) " ;
			 $sqlInsertFeature .= " VALUES(null, ".$fieldsVal." '".date("Y-m-d H:i:s")."')";
			$result=$this->dbObj->fun_db_query($sqlInsertFeature) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
			
			return $this->dbObj->fun_db_get_affected_rows();
		}
	}
	
	function funGetSpeechInfo($cateID){
		$locArray = array();
		$sql = "SELECT * FROM " . TABLE_SPEECH . " WHERE speech_id='".(int)$cateID."'";
		
		$result = $this->dbObj->fun_db_query($sql) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
		if(!$result || $this->dbObj->fun_db_get_num_rows($result) < 1){
			return; // user does not exists
		}
		$rowsCategory =  $this->dbObj->fun_db_fetch_rs_object($result);
		$locArray = array(
							"speech_id" => fun_db_output($rowsCategory->speech_id),
							"speech_section_id" => fun_db_output($rowsCategory->speech_section_id),
							"speech_para_id" => fun_db_output($rowsCategory->speech_para_id),
							"person_id" => fun_db_output($rowsCategory->person_id),
						    "list_order" => fun_db_output($rowsCategory->list_order),
							"status" => fun_db_output($rowsCategory->status),
							"speech_text"=> fun_db_output($rowsCategory->speech_text),
							"last_modified" => fun_db_output($rowsCategory->last_modified),
							"added_date" => fun_db_output($rowsCategory->added_date)
						 );
		return $locArray;
	}
	
	
	function funDeleteSpeech($cateID){
		
		$sqlDel = "DELETE FROM " . TABLE_SPEECH . " WHERE speech_id=" . (int)$cateID;
		$this->dbObj->fun_db_query($sqlDel) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
		return $this->dbObj->fun_db_get_affected_rows();
	}
	
	function fun_get_num_rows($sql){
		$totalRows = 0;
		$selected = "";
		$sql = trim($sql);
		if($sql==""){
			die("<font color='#ff0000' face='verdana' face='2'>Error: Query is Empty!</font>");
			exit;
		}
		$result = $this->dbObj->fun_db_query($sql);
		$totalRows = $this->dbObj->fun_db_get_num_rows($result);
		$this->dbObj->fun_db_free_resultset($result);
		return $totalRows;
	}
	
	
	
}
?>