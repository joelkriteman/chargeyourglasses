<?php
class Feedback{
	var $dbObj;
	
	function Feedback(){ // class constructor
		$this->dbObj = new DB();
		$this->dbObj->fun_db_connect();
	}
	
	function processFeedback($cateID=0, $actionMode='ADD'){

		if($cateID==""){
			$cateID = 0;
		}
		$booking_request_date=$_POST['date'];
		$booking_date=explode('/',$booking_request_date);

		$locArray = array(
						"client_name" => $_POST['client_name'],
						"location" => $_POST['location'],
						"feedback_text" => $_POST['feedback_text'],
						"date" => $booking_date[2]."-".$booking_date[1]."-".$booking_date[0],
						"home_page_status" => $_POST['home_page_status'],
						"groom_page_status" => $_POST['groom_page_status'],
						"list_order" => $_POST['list_order'],
						"status"  => $_POST['status'],
						"last_modified" => date("Y-m-d H:i:s")
					);
		
		if($actionMode=='EDIT'){
			
			$fields = "";
			$fieldsVal = "";
			foreach($locArray as $keys => $vals){
				$fields .= $keys . "='" . fun_db_input($vals). "', ";
			}
			$fields = trim($fields);
			if($fields!=""){
				$fields = substr($fields,0,strlen($fields)-1);
				$sqlUpdate = "UPDATE " . TABLE_FEEDBACKS . " SET " . $fields . " WHERE feedback_id='".(int)$cateID."'";
				$this->dbObj->fun_db_query($sqlUpdate) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
 	
				return $this->dbObj->fun_db_get_affected_rows();
			}
		}
		
		if($actionMode=='ADD'){
			$fields = "";
			$fieldsVal = "";
			foreach($locArray as $keys => $vals){
				$fields .= $keys . ", ";
				$fieldsVal .= "'" . fun_db_input($vals). "', ";
			}
			
			$sqlInsertFeature = "INSERT INTO " . TABLE_FEEDBACKS . "(feedback_id, ".$fields." added_date) " ;
			 $sqlInsertFeature .= " VALUES(null, ".$fieldsVal." '".date("Y-m-d H:i:s")."')";
			$result=$this->dbObj->fun_db_query($sqlInsertFeature) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
			
			return $this->dbObj->fun_db_get_affected_rows();
		}
	}
	
	function funGetFeedbackInfo($cateID){
		$locArray = array();
		$sql = "SELECT * FROM " . TABLE_FEEDBACKS . " WHERE feedback_id='".(int)$cateID."'";
		
		$result = $this->dbObj->fun_db_query($sql) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
		if(!$result || $this->dbObj->fun_db_get_num_rows($result) < 1){
			return; // user does not exists
		}
		$rowsCategory =  $this->dbObj->fun_db_fetch_rs_object($result);
		$locArray = array(
							"feedback_id" => fun_db_output($rowsCategory->feedback_id),
							"client_name" => fun_db_output($rowsCategory->client_name),
							"location" => fun_db_output($rowsCategory->location),
							"feedback_text" => fun_db_output($rowsCategory->feedback_text),
							"date" => fun_db_output($rowsCategory->date),
						    "list_order" => fun_db_output($rowsCategory->list_order),
							"status" => fun_db_output($rowsCategory->status),
							"home_page_status"=> fun_db_output($rowsCategory->home_page_status),
							"groom_page_status" => fun_db_output($rowsCategory->groom_page_status),
							"last_modified" => fun_db_output($rowsCategory->last_modified),
							"added_date" => fun_db_output($rowsCategory->added_date)
						 );
		return $locArray;
	}
	
	function funDeleteFeedback($cateID){
		
		$sqlDel = "DELETE FROM " . TABLE_FEEDBACKS . " WHERE feedback_id=" . (int)$cateID;
		$this->dbObj->fun_db_query($sqlDel) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
		return $this->dbObj->fun_db_get_affected_rows();
	}
	
	function fun_get_num_rows($sql){
		$totalRows = 0;
		$selected = "";
		$sql = trim($sql);
		if($sql==""){
			die("<font color='#ff0000' face='verdana' face='2'>Error: Query is Empty!</font>");
			exit;
		}
		$result = $this->dbObj->fun_db_query($sql);
		$totalRows = $this->dbObj->fun_db_get_num_rows($result);
		$this->dbObj->fun_db_free_resultset($result);
		return $totalRows;
	}
	
	
	
}
?>