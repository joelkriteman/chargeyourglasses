<?php
class Orders{
	var $dbObj;
	
	function Orders(){ // class constructor
		$this->dbObj = new DB();
		$this->dbObj->fun_db_connect();
	} 
	
	function funGetOrderInfo($cateID){
		$locArray = array();
		$sql = "SELECT * FROM " . TABLE_ORDERS . " WHERE order_id='".(int)$cateID."'";
		
		$result = $this->dbObj->fun_db_query($sql) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
		if(!$result || $this->dbObj->fun_db_get_num_rows($result) < 1){
			return; // user does not exists
		}
		$rowsCategory =  $this->dbObj->fun_db_fetch_rs_object($result);
		$locArray = array(
							"order_id" => fun_db_output($rowsCategory->order_id),
							"user_id" => fun_db_output($rowsCategory->user_id),
							"order_number" => fun_db_output($rowsCategory->order_number),
							"payment_status_id" => fun_db_output($rowsCategory->payment_status_id),
							"total_amount" => fun_db_output($rowsCategory->total_amount),
							"website_id" => fun_db_output($rowsCategory->website_id),
							"order_status" => fun_db_output($rowsCategory->order_status),
							"added_date" => fun_db_output($rowsCategory->added_date)
						 );
		return $locArray;
	}
}
?>