<?php
class Setting{
	var $dbObj;
	
	function Setting(){ // class constructor
		$this->dbObj = new DB();
		$this->dbObj->fun_db_connect();
	}
	
	function processSetting($cateID=0, $actionMode='ADD'){

		if($cateID==""){
			$cateID = 0;
		}
		$locArray = array(
						"time_days" => $_POST['time_days'],
						"home_line_of_month_text" => $_POST['home_line_of_month_text'],
						/*"groomlist_line_of_month_text" => $_POST['groomlist_line_of_month_text'],*/
						"amount" => $_POST['amount'],
						/*"groomlist_amount" => $_POST['groomlist_amount'],*/
						"status"  => $_POST['status'],
					);
		$allowedExts = array(
			  "pdf", 
			  "doc", 
			  "docx"
			); 
		$allowedMimeTypes = array( 
			  'application/msword',
			  'text/pdf',
			  'application/pdf',
			  'image/gif',
			  'image/jpeg',
			  'image/png'
			);

		//Sample Pdf			
		$MainimageFile= $_FILES[sample_speech_file][name];
		$Mainfile_check  = basename($_FILES[sample_speech_file][name]);
		if($Mainfile_check != "")
			{
			$extension = end(explode(".", $_FILES["sample_speech_file"]["name"]));
			//@$db_name_main=mktime()."_".str_replace(" ","_",basename($_FILES[sample_speech_file][name]));
			@$db_name_main=str_replace(" ","_",basename($_FILES[sample_speech_file][name]));
			$uploadMaindir=SITE_IMAGES_PDF_WS.$db_name_main;

			if (in_array( $_FILES["sample_speech_file"]["type"], $allowedMimeTypes ) ) 
			{      
			 move_uploaded_file($_FILES["sample_speech_file"]["tmp_name"], $uploadMaindir); 
			}
			$Mainnamearray=explode(".",$db_name_main);
		  	$Mainfilename=$Mainnamearray[0].".".$Mainnamearray[1];
			$locArray['sample_speech_file']=$Mainfilename;
 		}
	else
		{
		$locArray['sample_speech_file'] = $_REQUEST['sample_speech_file_1'];
		}
		//Sample Pdf
		//Groom Professional Speech Pdf
		$MainimageFile1= $_FILES[sample_groom_speech_file][name];
		$Mainfile_check1  = basename($_FILES[sample_groom_speech_file][name]);
		if($Mainfile_check1 != "")
			{
				$extension = end(explode(".", $_FILES["sample_groom_speech_file"]["name"]));
			//@$db_name_main=mktime()."_".str_replace(" ","_",basename($_FILES[sample_speech_file][name]));
			@$db_name_main1=str_replace(" ","_",basename($_FILES[sample_groom_speech_file][name]));
			$uploadMaindir1=SITE_IMAGES_PDF_WS.$db_name_main1;
			if (in_array( $_FILES["sample_groom_speech_file"]["type"], $allowedMimeTypes ) ) 
			{      
			 move_uploaded_file($_FILES["sample_groom_speech_file"]["tmp_name"], $uploadMaindir1); 
			}
			$Mainnamearray1=explode(".",$db_name_main1);
		  	$Mainfilename1=$Mainnamearray1[0].".".$Mainnamearray1[1];
			$locArray['sample_groom_speech_file']=$Mainfilename1;
 		}
		else
			{
				$locArray['sample_groom_speech_file'] = $_REQUEST['sample_groom_speech_file_1'];
			}
		//Groom Professional Speech Pdf
		//Best Man Professional Speech Pdf
		$MainimageFile2= $_FILES[sample_bestman_speech_file][name];
		$Mainfile_check2  = basename($_FILES[sample_bestman_speech_file][name]);
		if($Mainfile_check2 != "")
			{
				$extension = end(explode(".", $_FILES["sample_bestman_speech_file"]["name"]));
			//@$db_name_main=mktime()."_".str_replace(" ","_",basename($_FILES[sample_speech_file][name]));
			@$db_name_main2=str_replace(" ","_",basename($_FILES[sample_bestman_speech_file][name]));
			$uploadMaindir2=SITE_IMAGES_PDF_WS.$db_name_main2;
			if (in_array( $_FILES["sample_bestman_speech_file"]["type"], $allowedMimeTypes ) ) 
			{      
			 move_uploaded_file($_FILES["sample_bestman_speech_file"]["tmp_name"], $uploadMaindir2); 
			}
			$Mainnamearray2=explode(".",$db_name_main2);
		  	$Mainfilename2=$Mainnamearray2[0].".".$Mainnamearray2[1];
			$locArray['sample_bestman_speech_file']=$Mainfilename2;
 		}
		else
			{
				$locArray['sample_bestman_speech_file'] = $_REQUEST['sample_bestman_speech_file_1'];
			}
		//Best Man Professional Speech Pdf
		//Father of the Bride Professional Speech Pdf
		$MainimageFile3= $_FILES[sample_fob_speech_file][name];
		$Mainfile_check3  = basename($_FILES[sample_fob_speech_file][name]);
		if($Mainfile_check3 != "")
			{
				$extension = end(explode(".", $_FILES["sample_fob_speech_file"]["name"]));
			//@$db_name_main=mktime()."_".str_replace(" ","_",basename($_FILES[sample_speech_file][name]));
			@$db_name_main3=str_replace(" ","_",basename($_FILES[sample_fob_speech_file][name]));
			$uploadMaindir3=SITE_IMAGES_PDF_WS.$db_name_main3;
			if (in_array( $_FILES["sample_fob_speech_file"]["type"], $allowedMimeTypes ) ) 
			{      
			 move_uploaded_file($_FILES["sample_fob_speech_file"]["tmp_name"], $uploadMaindir3); 
			}
			$Mainnamearray3=explode(".",$db_name_main3);
		  	$Mainfilename3=$Mainnamearray3[0].".".$Mainnamearray3[1];
			$locArray['sample_fob_speech_file']=$Mainfilename3;
 		}
		else
			{
				$locArray['sample_fob_speech_file'] = $_REQUEST['sample_fob_speech_file_1'];
			}
		//Father of the Bride Professional Speech Pdf
		if($actionMode=='EDIT'){
			
			$fields = "";
			$fieldsVal = "";
			foreach($locArray as $keys => $vals){
				$fields .= $keys . "='" . fun_db_input($vals). "', ";
			}
			$fields = trim($fields);
			if($fields!=""){
				$fields = substr($fields,0,strlen($fields)-1);
				$sqlUpdate = "UPDATE " . TABLE_GENERAL_SETTING . " SET " . $fields . " WHERE setting_id='".(int)$cateID."'";
				$this->dbObj->fun_db_query($sqlUpdate) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
 	
				return $this->dbObj->fun_db_get_affected_rows();
			}
		}
		
	}
	
	function funGetSettingInfo($cateID){
		$locArray = array();
		$sql = "SELECT * FROM " . TABLE_GENERAL_SETTING . " WHERE setting_id='".(int)$cateID."'";
		
		$result = $this->dbObj->fun_db_query($sql) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
		if(!$result || $this->dbObj->fun_db_get_num_rows($result) < 1){
			return; // user does not exists
		}
		$rowsCategory =  $this->dbObj->fun_db_fetch_rs_object($result);
		$locArray = array(
							"setting_id" => fun_db_output($rowsCategory->setting_id),
							"time_days" => fun_db_output($rowsCategory->time_days),
							"amount" => fun_db_output($rowsCategory->amount),
							"home_line_of_month_text" => fun_db_output($rowsCategory->home_line_of_month_text),
							"groomlist_line_of_month_text" => fun_db_output($rowsCategory->groomlist_line_of_month_text),
						    "groomlist_amount" => fun_db_output($rowsCategory->groomlist_amount),
							"status" => fun_db_output($rowsCategory->status),
							"sample_speech_file" => fun_db_output($rowsCategory->sample_speech_file),
							"sample_groom_speech_file" => fun_db_output($rowsCategory->sample_groom_speech_file),
							"sample_bestman_speech_file" => fun_db_output($rowsCategory->sample_bestman_speech_file),
							"sample_fob_speech_file" => fun_db_output($rowsCategory->sample_fob_speech_file),
							"last_modified" => fun_db_output($rowsCategory->last_modified),
							"added_date" => fun_db_output($rowsCategory->added_date)
						 );
		return $locArray;
	}
	
	function fun_get_num_rows($sql){
		$totalRows = 0;
		$selected = "";
		$sql = trim($sql);
		if($sql==""){
			die("<font color='#ff0000' face='verdana' face='2'>Error: Query is Empty!</font>");
			exit;
		}
		$result = $this->dbObj->fun_db_query($sql);
		$totalRows = $this->dbObj->fun_db_get_num_rows($result);
		$this->dbObj->fun_db_free_resultset($result);
		return $totalRows;
	}
	
	
	
}
?>