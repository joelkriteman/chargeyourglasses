<?php
class SitePages{
	var $dbObj;
	
	function SitePages(){ // class constructor
		$this->dbObj = new DB();
		$this->dbObj->fun_db_connect();
	}
	
	function processSitePages($pagesArray, $pageID=0, $actionMode='ADD'){
		if($pageID==""){
			$pageID = 0;
		}
		
		if($actionMode=='EDIT'){
			
			$fields = "";
			$fieldsVal = "";
			foreach($pagesArray as $keys => $vals){
				$fields .= $keys . "='" . fun_db_input($vals). "', ";
			}
			$fields = trim($fields);
			if($fields!=""){
				$fields = substr($fields,0,strlen($fields)-1);
				$sqlUpdate = "UPDATE " . TABLE_PAGES . " SET " . $fields . " WHERE pages_id='".(int)$pageID."'";
				$this->dbObj->fun_db_query($sqlUpdate) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On SitePages table.</font>");
				
				return $this->dbObj->fun_db_get_affected_rows();
			}
		}
		
		if($actionMode=='ADD'){
			$fields = "";
			$fieldsVal = "";
			foreach($pagesArray as $keys => $vals){
				$fields .= $keys . ", ";
				$fieldsVal .= "'" . fun_db_input($vals). "', ";
			}
			 $sqlInsert = "INSERT INTO " . TABLE_PAGES . "(pages_id, ".$fields." pages_added_date) " ;
			 $sqlInsert .= " VALUES(null, ".$fieldsVal." '".date("Y-m-d H:i:s")."')";
			$this->dbObj->fun_db_query($sqlInsert) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On SitePages table.</font>");
			return $this->dbObj->fun_db_get_affected_rows(); 
		}
	}
	
	function funMakeSitePages($pageID){
		$spDetsArray = $this->funGetSitePagesInfo($pageID);
		$ext = ".php";
		if(is_array($spDetsArray) && sizeof($spDetsArray)){
			$fileTamplatePath = SITE_DOC_ROOT;
			if($spDetsArray["page_title"]!=""){
				$fileName = preg_replace("/\s+/i", "-", strtolower($spDetsArray["page_title"]));
				if(file_exists(SITE_DOC_ROOT . $fileName . $ext)){
					@chmod(SITE_DOC_ROOT . $fileName.$ext, 0777);
					@unlink(SITE_DOC_ROOT . $fileName.$ext);
				}
				
				$fileContent = fun_getFileContent(SITE_DOC_ROOT. "site-page-tamplate.php");
				$fileContent = str_replace("[%PAGE_ID%]", $pageID, $fileContent);
				
				if (!$handle = fopen(SITE_DOC_ROOT . $fileName.$ext, 'w')) {
					 return false;
				}
			
				// Write $somecontent to our opened file.
				if (fwrite($handle, $fileContent) === FALSE) {
					 return false;
				}
				fclose($handle);
				return true;
			}
		}
	}
	
	function funGetSitePagesInfo($pageID){
		$pagesArray = array();
		$sql = "SELECT * FROM " . TABLE_PAGES . " WHERE pages_id='".(int)$pageID."'";
		
		$result = $this->dbObj->fun_db_query($sql) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On SitePages table.</font>");
		if(!$result || $this->dbObj->fun_db_get_num_rows($result) < 1){
			return; // user does not exists
		}
		$rowsSitePages =  $this->dbObj->fun_db_fetch_rs_object($result);
		$pagesArray = array(
							"pages_id" => fun_db_output($rowsSitePages->pages_id),
							"section_id" => fun_db_output($rowsSitePages->section_id),
							"page_url" => (fun_db_output($rowsSitePages->page_url)),
							"page_desc" => fun_db_output($rowsSitePages->page_desc),
							"pages_title" => (fun_db_output($rowsSitePages->pages_title)),
							"pages_content" => fun_db_output($rowsSitePages->pages_content),
							"page_linkbacktext" => fun_db_output($rowsSitePages->page_linkbacktext),
							"page_body_content" => fun_db_output($rowsSitePages->page_body_content),
							"page_subtitle" => (fun_db_output($rowsSitePages->page_subtitle)),
							"page_tips_intro" => fun_db_output($rowsSitePages->page_tips_intro),
							"page_you_should_also" => (fun_db_output($rowsSitePages->page_you_should_also)),
							"page_do" => fun_db_output($rowsSitePages->page_do),
							"home_midText" => fun_db_output($rowsSitePages->home_midText),
							"page_dont" => fun_db_output($rowsSitePages->page_dont),
							"page_howit_works" => fun_db_output($rowsSitePages->page_howit_works),
							"page_quizlinktext" => (fun_db_output($rowsSitePages->page_quizlinktext)),
							"page_prospeechlinktext" => fun_db_output($rowsSitePages->page_prospeechlinktext),
							"page_first_intro_title" => (fun_db_output($rowsSitePages->page_first_intro_title)),
							"page_HowItWorksList" => fun_db_output($rowsSitePages->page_HowItWorksList),
							"page_first_intro_footer" => fun_db_output($rowsSitePages->page_first_intro_footer),
							"template_id" => fun_db_output($rowsSitePages->template_id),
							"home_main_title" => fun_db_output($rowsSitePages->home_main_title),
							"home_LeftIntro" => (fun_db_output($rowsSitePages->home_LeftIntro)),
							"home_LawrenceQuote" => fun_db_output($rowsSitePages->home_LawrenceQuote),
							"home_LawrenceQuoteCredit" => (fun_db_output($rowsSitePages->home_LawrenceQuoteCredit)),
							"home_BBCNote" => fun_db_output($rowsSitePages->home_BBCNote),
							"home_line_of_month_bottom_text" => fun_db_output($rowsSitePages->home_line_of_month_bottom_text),
							"home_line_of_month_text" => fun_db_output($rowsSitePages->home_line_of_month_text),
							"page_seo_meta_title" => fun_db_output($rowsSitePages->page_seo_meta_title),
							"page_seo_meta_description" => fun_db_output($rowsSitePages->page_seo_meta_description),
							"page_seo_meta_keyword" => fun_db_output($rowsSitePages->page_seo_meta_keyword),
							"status" => fun_db_output($rowsSitePages->status),
							"pages_last_modified" => fun_db_output($rowsSitePages->pages_last_modified),
							"pages_added_date" => fun_db_output($rowsSitePages->pages_added_date)
						 );
		return $pagesArray;
	}
	
	function funIsSubPagesExists($pageID){
		$scExists = false;
		$pageID = (int)$pageID;
		if($pageID){
			$sql = "SELECT * FROM ".TABLE_PAGES."  WHERE parent_id=" . $pageID;
			if($this->fun_get_num_rows($sql)){
				$scExists = true;
				
			}
			
		}
		return $scExists;
	}
	
	function funIsMainPage($pageID){
		$scExists = false;
		$pageID = (int)$pageID;
		if($pageID){
			$sql = "SELECT parent_id FROM ".TABLE_PAGES."  WHERE page_id=" . $pageID;
			$result = $this->dbObj->fun_db_query($sql) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On SitePages table.</font>");
			$parentResult=$this->dbObj->fun_db_fetch_rs_object($result);
			if(($parentResult->parent_id)=='0')
			{
				$scExists = true;
				
			}
			
		}
		return $scExists;
	}
	
	
	function fun_get_sitepages_hierarchical($parent_id = '0', $spacing = '', $exclude = '', $sitepages_tree_array = '', $include_itself = false){
		if(!is_array($sitepages_tree_array)){
			$sitepages_tree_array = array();
		}
		
			$sql = "SELECT * FROM " . TABLE_PAGES . " WHERE parent_id='".(int)$parent_id."'  AND status = '1'";
		
	 $sql .=  "ORDER BY pages_title";
		
		$result = $this->dbObj->fun_db_query($sql) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On SitePages table.</font>");
		if(!$result || $this->dbObj->fun_db_get_num_rows($result) < 1){
			return $sitepages_tree_array;
		}
		while($rowsCate = $this->dbObj->fun_db_fetch_rs_object($result)){
			if($exclude != $rowsCate->pages_id){
				$sitepages_tree_array[] = array('id' => $rowsCate->pages_id, 'text' => $spacing . $rowsCate->pages_title);
			}
			$sitepages_tree_array = $this->fun_get_sitepages_hierarchical($rowsCate->pages_id, $spacing . '&nbsp;&nbsp;-&nbsp;', $exclude, $sitepages_tree_array);
		}
		return $sitepages_tree_array;
	}	
	
	function funBreadcrumb($locID){
		if($locID){
			$last="false";
			$newLocID;
			$locName;
			$i=0;
			$k=0;
			$cmt=0;
			while($cmt<10){
				$sqlCate = "SELECT page_id, page_title, parent_id FROM ".TABLE_SITE_PAGES."  WHERE page_id= '" . (int)$locID . "'";
				
				$resultTmp = $this->dbObj->fun_db_query($sqlCate) or die("Error: Unable to execute request!<br>Invalid Query On SitePages table.");
				while($rowsCate = $this->dbObj->fun_db_fetch_rs_object($resultTmp)){
					$newLocID[$i] = "view-page.php?cPath=" . $rowsCate->page_id;
					$locName[$i]= fun_db_input($rowsCate->page_title);
					$locID = (int)fun_db_input($rowsCate->parent_id);
					$i=$i+1;
					
					if($locID==0){
						break;
					}
				}
				$cmt++;
			}
			echo "<a href=\"view-page.php\" class=\"bText_link\">Home</a><span class=\"bText_link\">&nbsp;&nbsp;&raquo;&nbsp;&nbsp;</span>";
			for($k=sizeof($locName) -1;$k>=0;$k--){
				if ($k!=0){
					echo("<a href=\"$newLocID[$k]\" class=\"bText_link\">$locName[$k]</a>");
					echo("<span class=\"bText_link\">&nbsp;&nbsp;&raquo;&nbsp;&nbsp;</span>");
				}else{
					if($last=="false"){	
						echo("<span class=\"bText_link\">$locName[$k]</span>");
					}else{
						echo("<a href=$newLocID[$k] class=\"bText_link\">$locName[$k]</a>");
					}
				}
			}
		}
	}
	
		
	function fun_sitepages_hierarchical_option($pageID=''){
	
		$cateList = $this->fun_get_sitepages_hierarchical();
		for($i=0, $n=sizeof($cateList); $i<$n; $i++){
			if($cateList[$i]['id']==$pageID){
				$selected = " selected";
			}else{
				$selected = "";
			}
			echo "<option value=\"".$cateList[$i]['id'] ."\" ".$selected.">".$cateList[$i]['text']."</option>\n";
		}
	}
	
	
	
	
	function funDeleteSitePages($pageID){
		
		 $sql = "DELETE FROM " . TABLE_PAGES . " WHERE pages_id='".(int)$pageID."'";
		$result = $this->dbObj->fun_db_query($sql) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On SitePages table.</font>");
		return true;
	}
	function fun_get_num_rows($sql){
		$totalRows = 0;
		$selected = "";
		$sql = trim($sql);
		if($sql==""){
			die("<font color='#ff0000' face='verdana' face='2'>Error: Query is Empty!</font>");
			exit;
		}
		$result = $this->dbObj->fun_db_query($sql);
		$totalRows = $this->dbObj->fun_db_get_num_rows($result);
		$this->dbObj->fun_db_free_resultset($result);
		return $totalRows;
	}
	function funGetSiteTopPages($pageID){
		$pagesArray = array();
		 $sql = "SELECT * FROM " . TABLE_PAGES . " WHERE parent_id='".(int)$pageID."' AND page_status='1'";
		
		$result = $this->dbObj->fun_db_query($sql) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On SitePages table.</font>");
		
		return $result;
		
		}	
		function funGetSiteTopPageID($pageId){
		$pagesArray = array();
		
			  $sql = "SELECT page_id FROM ".TABLE_PAGES." WHERE  page_id='".$pageId."' order by page_last_modified DESC";
		
		$result = $this->dbObj->fun_db_query($sql) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On SitePages table.</font>");
		$rowsResult=$this->dbObj->fun_db_fetch_rs_object($result);
		return $rowsResult->pages_id;
		
		}	
		
		function funTotSiteTopPages($pageID){
		$pagesArray = array();
		 $sql = "SELECT * FROM " . TABLE_PAGES . " WHERE parent_id='".(int)$pageID."' AND page_status='1'";
		
		$result = $this->dbObj->fun_db_query($sql) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On SitePages table.</font>");
		
		$totalRows = $this->dbObj->fun_db_get_num_rows($result);
		$this->dbObj->fun_db_free_resultset($result);
		return $totalRows;
		
		}	
	
		
}
?>