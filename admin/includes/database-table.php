<?php
define("TABLE_ADMINS_USERS", "admin_users");
define("TABLE_ADMINS_TYPE", "admin_type");
define("TABLE_GENERAL_SETTING", "general_settings");
define("TABLE_NEWSLETTER", "newsletter");
define("TABLE_PERSON", "person");
define("TABLE_PAGES", "site_pages");
define("TABLE_USERS", "site_users");
define("TABLE_SPEECH", "speech");
define("TABLE_SPEECH_PARA", "speech_para");
define("TABLE_SPEECH_SECTION", "speech_section");
define("TABLE_QUIZ", "quiz_question");
define("TABLE_ORDERS", "site_orders");
define("TABLE_FEEDBACKS", "site_feedbacks");

?>