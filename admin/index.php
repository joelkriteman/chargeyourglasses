<?php
require_once("includes/application-top.php");
$sessionID =session_id();

if(isset($_POST['securityKey']) && $_POST['securityKey']==md5("AKHILIMADMINLETMELOGIN")){
	$objAdmin = new Admins();
	$adminUname = fun_db_output($_POST['admin_username']);
	$adminPass = fun_db_output($_POST['admin_password']);
	if(!$objAdmin->fun_check_username_admin_existance($adminUname))
	{
	redirectURL(SITE_ADMIN_URL."index.php?msg=".urlencode("Please enter correct username!"));
	}
	
	if(!$objAdmin->fun_check_pwd_admin_existance(md5($adminPass)))
	{
	redirectURL(SITE_ADMIN_SECURE_URL."index.php?msg=".urlencode("Your password does not match with our records!"));
	}
	
	if($objAdmin->fun_verify_admins($adminUname, md5($adminPass))){
	
		$adminInfo = $objAdmin->fun_getAdminUserInfo(0, $adminUname);
		$adminCorrectInfo=$objAdmin->fun_authenticate_admin_correct($adminUname,$adminInfo['au_id']);
		$adminTypeId= $adminInfo['au_type_id'];

		if(sizeof($adminInfo)){
			$adminTypeInfo = $objAdmin->fun_getAdminUserTypeInfo($adminTypeId);
			if($adminInfo['status']=="1"){
				$_SESSION['session_admin_userid'] =  $adminInfo['au_id'];
				$_SESSION['session_admin_username'] = $adminInfo['au_username'];
				$_SESSION['session_admin_password'] = md5($adminInfo['au_password']);
				$_SESSION['session_admin_usertype'] =  $adminTypeInfo['au_type'];

				redirectURL(SITE_ADMIN_SECURE_URL."dashboard.php");
			}else{
				unset($_SESSION['session_admin_userid']);
				unset($_SESSION['session_admin_username']);
				unset($_SESSION['session_admin_password']);
				unset($_SESSION['session_admin_usertype']);
				redirectURL(SITE_ADMIN_URL."index.php?msg=".urlencode("You account has been suspended due to some reason!"));
			}
		}else{
			redirectURL(SITE_ADMIN_URL."index.php?msg=".urlencode("Invalid email-address or password!"));
		}
	}else{
		redirectURL(SITE_ADMIN_URL."index.php?msg=".urlencode("Invalid email-address or password!"));
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo SITE_NAME?> :: Admin Panel</title>
<link type="text/css" rel="stylesheet" media="all" href="css/base.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/jquery-ui.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/grid.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/visualize.css" />
<style>
body { overflow-x:hidden; }
td{ border:none;}
.header-bg1{ }
.login-bg1{ }
.forgot{ color:#be0100; text-decoration:none;}
</style>

	<script language="javascript" type="text/javascript">
	function funSetCursor(){
		var admin_username = document.getElementById("admin_username");
		admin_username.focus();
	}
	
	function validatefrm()
	{
	var frm = document.adminLoginFrm;
	if((frm.admin_username.value=="")||(frm.admin_username.value=="Username")){
			alert("Error: Please enter your username!");
			frm.admin_username.focus();
			return false;
		}
		if((frm.admin_password.value=="")||(frm.admin_password.value=="Password")){
			alert("Error: Please enter password!");
			frm.admin_password.focus();
			return false;
		}
	return true;
	}
	</script>
</head>
<body onLoad="javascript: funSetCursor();">
<div class="header-bg1"><div class="logo-outer">
    <img id="logo" src="images/cyg-logo.png" alt="CHARGEYOURGLASSes.com | Create the Perfect Wedding Speeches" /> 
    <?php /*?><?php echo SITE_NAME?><?php */?>
    </div>  <div style="clear:both;"></div></div>
    <div style="clear:both;"></div>
<div style="width:400px; margin:10% auto 0;">
<table border="0" cellpadding="0" cellspacing="0">

<tr >
	<!-- middle start -->
	<td height="456" align="center">
		
		
		<table cellpadding="0" cellspacing="10" width="40%" border="0" bgcolor="#ffb72d" class="login-table">
        <tr><td width="31%" height="35" colspan="4" align="center" class="login-bg1">
        <div class="login-div">Login</div>
        </td></tr>
        <?php
		if(($_GET['msg']!="")){
		?>
        <tr><td>
		<div style="font-size:11px; margin-bottom:25px; font-size:12px; font-family:Verdana, Arial, Helvetica, sans-serif; color:#333333;">
		<b><?php echo urldecode($_GET['msg'])?></b><br>
		</div>
        </td></tr>
		<?php
		}
		?>
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" class="center-login">
				<tr>
					
					<td>
                    <form name="adminLoginFrm" method="post" action="" onSubmit="return validatefrm();">
                     <input type="hidden" name="ip" value="<?php echo  $_SERVER['REMOTE_ADDR'];?>" />
                     <input type="hidden" name="status" value="<?php echo $_REQUEST['status'];?>" />
                     <input type="hidden" name="cateID" value="<?php echo $_REQUEST['enquiry_id'];?>" />
                       <input type="hidden" name="backUrl" value="<?php echo  $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];?>" />
						<table width="100%" border="0" cellpadding="0" cellspacing="0" >
						
							<tr><td width="31%"><b>Username:</b></td></tr>
							<tr><td width="69%"><div class="pos-ico"><img src="images/user-ico.jpg" /><input type="text" name="admin_username" id="admin_username" value="Username" size="20"  onClick="this.value=''" /></div></td>
						</tr>
						
							<tr><td><b>Password:</b></td></tr>
							<tr><td><div class="pos-ico"><img src="images/pwd-ico.jpg" /><input type="password" name="admin_password" value="Password" size="20" onFocus="javascript: this.className='onFocusInput'" onBlur="javascript: this.className='normalInput'"  onClick="this.value=''" /></div></td>
						</tr>
                        <tr><td height="10"></td></tr>
						<tr>
							
							<td>
                            
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left"><input type="submit"  name="subBttn" value="Log in" class="button" /></td>
    <td align="right" style="text-align:right"> <input type="reset"  name="reset" value="Reset" class="button-reset" /> <input type="hidden" name="securityKey" value="<?php echo md5("AKHILIMADMINLETMELOGIN")?>" /><a href="forget-pwd.php" class="forgot">| Forgot Password</a></td>
  </tr>
</table>

                            
                           </td>
                            
						</tr>
						 <tr><td height="20"></td></tr>
					  </table></form>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
	<!-- middle end -->

</table></div>



</body>
</html>