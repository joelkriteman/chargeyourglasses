<?php
require_once("includes/application-top.php");
require_once("includes/classes/class.SitePages.php");
$objAdmin = new Admins();
$objAdmin->fun_authenticate_admin();
$objPage = new SitePages();

if(isset($_GET['cPath'])){
	$cID = $_GET['cPath'];
}else{
	$cID = 0;
}
if(!empty($_GET['page'])){
	$page = $_GET['page'];
}else{
	$page = 1;
}
$pID = $_REQUEST['pageID'];

$newsDetails = $objPage->funGetSitePagesInfo($pID);

if($_POST['submit'] == "Submit" || $_POST['submit'] == "Save"){
	$pageDets = array(
					"website_id" => $_POST["website_id"],
					"section_id" => $_POST["section_id"],
					"page_url" => ($_POST["page_url"]),
					"page_desc" => ($_POST["page_desc"]),
					"pages_title" => ($_POST["pages_title"]),
					"pages_content" => $_POST["pages_content"],
					"page_linkbacktext" => $_POST["page_linkbacktext"],
					"page_body_content" => ($_POST["page_body_content"]),
					"page_subtitle" => ($_POST["page_subtitle"]),
					"page_tips_intro" => $_POST["page_tips_intro"],
					"page_you_should_also" => $_POST["page_you_should_also"],
					"page_do" => $_POST["page_do"],
					"page_dont" => ($_POST["page_dont"]),
					"home_midText" => ($_POST["home_midText"]),
					"page_howit_works" => ($_POST["page_howit_works"]),
					"page_quizlinktext" => $_POST["page_quizlinktext"],
					"page_prospeechlinktext" => $_POST["page_prospeechlinktext"],
					"page_first_intro_title" => ($_POST["page_first_intro_title"]),
					"page_HowItWorksList" => $_POST["page_HowItWorksList"],
					"home_LeftIntro" => $_POST["home_LeftIntro"],
					"home_LawrenceQuote" => $_POST["home_LawrenceQuote"],
					"home_LawrenceQuoteCredit" => $_POST["home_LawrenceQuoteCredit"],
					"home_BBCNote" => $_POST["home_BBCNote"],
					"home_line_of_month_text" => $_POST["home_line_of_month_text"],
					"home_line_of_month_bottom_text" => $_POST["home_line_of_month_bottom_text"],
					"home_main_title" => $_POST["home_main_title"],
					"page_first_intro_footer" => $_POST["page_first_intro_footer"],
					"template_id" => $_POST["template_id"],
					"page_seo_meta_title" => $_POST["page_seo_meta_title"],
					"page_seo_meta_keyword" => $_POST["page_seo_meta_keyword"],
					"page_seo_meta_description" => $_POST["page_seo_meta_description"],
				);
	
	$pageStaus=$_POST["status"];
	$pageDets["status"] = $pageStaus;
	$pageDets["pages_last_modified"] = date("Y-m-d H:i:s");
	if($_POST['securityKey']==md5("ADDAREA")){ // EDIT news
		$affectedRows = $objPage->processSitePages($pageDets, $pID, 'EDIT');
		if($affectedRows < 0){
			$msg = "Unable to add page details! Please try again.";
			$msgType = "1";
		}else{
			$msg = "Page details have been added successfully!";
			$msgType = "2";
		}
	}
	redirectURL("page-confetti.php?msgtype=".$msgType."&msg=".urlencode($msg));
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>WEB ADMIN SECTION</title>
<link type="text/css" rel="stylesheet" media="all" href="css/base.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/jquery-ui.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/grid.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/visualize.css" />
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/_samples/sample.js" type="text/javascript"></script>
<link href="ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
<script src="tinymce/jscripts/tiny_mce/tiny_mce_dev.js" type="text/javascript"></script>
<script type="text/javascript">
tinyMCE.init({
        // General options
       mode : "specific_textareas",
       editor_selector : "mceEditor",
       theme : "advanced",
	   plugins : "style",
       //theme_advanced_buttons1 : "",
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
    theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,jbimages,styleprops,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
       
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,
		relative_urls : false,

        // Skin options
        skin : "o2k7",
        skin_variant : "silver",

        // Replace values for the template plugin
        template_replace_values : {
                username : "Some User",
                staffid : "991234"
        }
});
</script>
<script type="text/javascript" src="jscript/jquery.js"></script>
<script language="javascript" type="text/javascript">
function validateForm(){
	var frm = document.form1;
		if(frm.pages_title.value==""){
			alert("Error: Please enter page title!");
			frm.pages_title.focus();
			return false;
		}
}
function chngeTemplate(val)
{
	if((val==0) || (val==1))
	{
		document.getElementById('tr_body_content').style.display = 'block';
		document.getElementById('tr_subtitle').style.display = 'none';
		document.getElementById('tr_tips_intro').style.display = 'none';
		document.getElementById('tr_you_should_also').style.display = 'none';
		document.getElementById('tr_do').style.display = 'none';
		document.getElementById('tr_dont').style.display = 'none';
		document.getElementById('tr_howit_works').style.display = 'none';
		document.getElementById('tr_quizlinktext').style.display = 'none';
		document.getElementById('tr_prospeechlinktext').style.display = 'none';
		document.getElementById('tr_first_intro_title').style.display = 'none';
		document.getElementById('tr_HowItWorksList').style.display = 'none';
		document.getElementById('tr_first_intro_footer').style.display = 'none';
	}
	if(val==3)
	{
		document.getElementById('tr_body_content').style.display = 'none';
		document.getElementById('tr_subtitle').style.display = 'none';
		document.getElementById('tr_tips_intro').style.display = 'none';
		document.getElementById('tr_you_should_also').style.display = 'none';
		document.getElementById('tr_do').style.display = 'none';
		document.getElementById('tr_dont').style.display = 'none';
		document.getElementById('tr_howit_works').style.display = 'none';
		document.getElementById('tr_quizlinktext').style.display = 'none';
		document.getElementById('tr_prospeechlinktext').style.display = 'none';
		document.getElementById('tr_first_intro_title').style.display = 'none';
		document.getElementById('tr_HowItWorksList').style.display = 'none';
		document.getElementById('tr_first_intro_footer').style.display = 'none';
	
	}
	if(val==2)
	{
		document.getElementById('tr_body_content').style.display = 'none';
		document.getElementById('tr_subtitle').style.display = 'none';
		document.getElementById('tr_tips_intro').style.display = 'none';
		document.getElementById('tr_you_should_also').style.display = 'none';
		document.getElementById('tr_do').style.display = 'none';
		document.getElementById('tr_dont').style.display = 'none';
		document.getElementById('tr_howit_works').style.display = 'block';
		document.getElementById('tr_quizlinktext').style.display = 'block';
		document.getElementById('tr_prospeechlinktext').style.display = 'block';
		document.getElementById('tr_first_intro_title').style.display = 'block';
		document.getElementById('tr_HowItWorksList').style.display = 'block';
		document.getElementById('tr_first_intro_footer').style.display = 'block';
	
	}
	if(val==4)
	{
		document.getElementById('tr_body_content').style.display = 'none';
		document.getElementById('tr_subtitle').style.display = 'block';
		document.getElementById('tr_tips_intro').style.display = 'block';
		document.getElementById('tr_you_should_also').style.display = 'block';
		document.getElementById('tr_do').style.display = 'block';
		document.getElementById('tr_dont').style.display = 'block';
		document.getElementById('tr_howit_works').style.display = 'none';
		document.getElementById('tr_quizlinktext').style.display = 'none';
		document.getElementById('tr_prospeechlinktext').style.display = 'none';
		document.getElementById('tr_first_intro_title').style.display = 'none';
		document.getElementById('tr_HowItWorksList').style.display = 'none';
		document.getElementById('tr_first_intro_footer').style.display = 'none';
	
	}
}
</script>
</head>

<body id="actcategory">
<div id="header">
  <div class="header-top tr">
    <p>logged in as <?php echo $_SESSION['session_admin_username'];?>, <?php echo date("D j M Y");?></p>
  </div>
  <div class="header-middle"> 
    
    <!-- Start Top Nav -->
    <?php include_once("includes/top_nav.php");?>
    <!-- End Top Nav -->
    
    <div class="clear"> </div>
  </div>
</div>
<div id="page-wrapper">
  <div class="page"> 
    
    <!-- Start Sidebar -->
    <?php include_once("includes/dashboard-header.php");?>
    <!-- End Sidebar --> 
    
    <!-- Star Page Content  -->
    <div id="page-content"> 
      <!-- Start Page Header -->
      <div id="page-header">
        <h1>Edit Page Content</h1>
      </div>
      <?php if($_REQUEST['msg']!=""){?>
      <div class="notification success"> <span class="strong">SUCCESS!</span> <?php echo $_REQUEST['msg'];?> </div>
      <?php }?>
      <!-- End Page Header --> 
      
      <!-- Start Grid -->
      <div class="container_12"> 
        
        <!-- Start Quick Index -->
        <div class="grid_12">
          <div class="box-header">Edit Page Content</div>
          <form action="" method="post" enctype="multipart/form-data" name="form1" onSubmit="return validateForm();">
            <input type="hidden" name="securityKey" value="<?php echo md5("ADDAREA");?>" />
            <input type="hidden" name="au_id" value="<?php echo $_SESSION['session_admin_userid']?>" />
            <input type="hidden" name="template_id" value="<?php echo $newsDetails['template_id'];?>" />
            <input type="hidden" name="website_id" value="3" />
            <div class="box table">
              <table width="100%"  border="0" cellspacing="0" cellpadding="0" class="module-d tbllist" style="border: solid 1px #cccccc;">
                <tr>
                  <td><label class="tip" >Select Template</label></td>
                  <td><select name="template_id" onchange="javascript: chngeTemplate(this.value);" disabled="disabled">
                      <option value="0" <?php if($newsDetails['template_id']==0){?> selected="selected" <?php }?>>General Template</option>
                      <option value="1" <?php if($newsDetails['template_id']==1){?> selected="selected" <?php }?>>Professional Speech</option>
                      <option value="2" <?php if($newsDetails['template_id']==2){?> selected="selected" <?php }?>>Wedding Speech</option>
                      <option value="3" <?php if($newsDetails['template_id']==3){?> selected="selected" <?php }?>>Quiz</option>
                      <option value="4" <?php if($newsDetails['template_id']==4){?> selected="selected" <?php }?>>Tips</option>
                       <option value="7" <?php if($newsDetails['template_id']==7){?> selected="selected" <?php }?>>Home</option>
                    </select></td>
                </tr>
                <tr>
                  <td><label class="tip" >Select Section</label></td>
                  <td><select name="section_id">
                      <option value="0" <?php if($newsDetails['section_id']==0){?> selected="selected" <?php }?>>Home / Restricted</option>
                      <option value="1" <?php if($newsDetails['section_id']==1){?> selected="selected" <?php }?>>Best Man</option>
                      <option value="2" <?php if($newsDetails['section_id']==2){?> selected="selected" <?php }?>>Groom</option>
                      <option value="3" <?php if($newsDetails['section_id']==3){?> selected="selected" <?php }?>>Father of the Bride</option>
                    </select></td>
                </tr>
                <tr>
                  <td><label class="tip" >Page Description:</label></td>
                  <td><input name="page_desc" type="text" class="textbox_long" id="page_desc" value="<?php echo $newsDetails['page_desc']; ?>" /></td>
                </tr>
                <?php if($newsDetails['template_id']==7){?>
                <tr>
                  <td><label class="tip" >Main Title:</label></td>
                  <td><textarea class="mceEditor" id="home_main_title" name="home_main_title" style="height:200px; width:550px;"><?php echo $newsDetails['home_main_title'];?></textarea></td>
                </tr>
                 <tr>
                  <td ><label class="tip" >LeftIntro:</label></td>
                  <td ><textarea class="mceEditor" id="home_LeftIntro" name="home_LeftIntro" style="height:250px; width:550px;"><?php echo $newsDetails['home_LeftIntro'];?></textarea></td>
                </tr>
                 <tr>
                  <td ><label class="tip" >MiddleText:</label></td>
                  <td ><input name="home_midText" type="text" class="textbox_long" id="home_midText" value="<?php echo $newsDetails['home_midText']; ?>" /></td>
                </tr>
                <tr>
                  <td><label class="tip" >LawrenceQuote:</label></td>
                  <td><input name="home_LawrenceQuote" type="text" class="textbox_long" id="home_LawrenceQuote" value="<?php echo $newsDetails['home_LawrenceQuote']; ?>" /></td>
                </tr>
                <tr>
                  <td><label class="tip" >LawrenceQuoteCredit:</label></td>
                  <td><input name="home_LawrenceQuoteCredit" type="text" class="textbox_long" id="home_LawrenceQuoteCredit" value="<?php echo $newsDetails['home_LawrenceQuoteCredit']; ?>" /></td>
                </tr>
                <tr>
                  <td><label class="tip" >BBCNote:</label></td>
                  <td><input name="home_BBCNote" type="text" class="textbox_long" id="home_BBCNote" value="<?php echo $newsDetails['home_BBCNote']; ?>" /></td>
                </tr>
                <!--<tr>
                  <td><label class="tip" >LineOfTheMonth:</label></td>
                  <td><textarea class="mceEditor" id="home_line_of_month_text" name="home_line_of_month_text" style="height:250px; width:550px;"><?php echo $newsDetails['home_line_of_month_text'];?></textarea></td>
                </tr>-->
                <?php  } else {?>
                <tr>
                  <td><label class="tip" >Title:</label></td>
                  <td><input name="pages_title" type="text" class="textbox_long" id="pages_title" value="<?php echo $newsDetails['pages_title']; ?>" /></td>
                </tr>
                <tr>
                  <td ><label class="tip" >Intro:</label></td>
                  <td ><textarea class="mceEditor" id="pages_content" name="pages_content" style="height:250px; width:550px;"><?php echo $newsDetails['pages_content'];?></textarea></td>
                </tr>
                <?php if(($newsDetails['template_id']==0) || ($newsDetails['template_id']==1)) {?>
                <tr id="tr_body_content" >
                  <td ><label class="tip" >Body:</label></td>
                  <td ><textarea class="mceEditor" id="page_body_content" name="page_body_content" style="height:250px; width:550px;"><?php echo $newsDetails['page_body_content'];?></textarea></td>
                </tr>
                <?php  } ?>
                <?php if($newsDetails['template_id']==4){?>
                <tr id="tr_subtitle" >
                  <td><label class="tip" >SubTitle:</label></td>
                  <td><input name="page_subtitle" type="text" class="textbox_long" id="page_subtitle" value="<?php echo $newsDetails['page_subtitle']; ?>" /></td>
                </tr>
                <tr id="tr_tips_intro">
                  <td ><label class="tip" >Tips Intro:</label></td>
                  <td ><textarea class="mceEditor" id="page_tips_intro" name="page_tips_intro" style="height:250px; width:550px;"><?php echo $newsDetails['page_tips_intro'];?></textarea></td>
                </tr>
                <tr id="tr_you_should_also">
                  <td ><label class="tip" >You Should Also:</label></td>
                  <td ><textarea class="mceEditor" id="page_you_should_also" name="page_you_should_also" style="height:250px; width:550px;"><?php echo $newsDetails['page_you_should_also'];?></textarea></td>
                </tr>
                <tr id="tr_do">
                  <td><label class="tip" >Do's:</label></td>
                  <td><textarea class="mceEditor" id="page_do" name="page_do" style="height:250px; width:550px;"><?php echo $newsDetails['page_do'];?></textarea></td>
                </tr>
                <tr id="tr_dont">
                  <td ><label class="tip" >Dont's:</label></td>
                  <td ><textarea class="mceEditor" id="page_dont" name="page_dont" style="height:250px; width:550px;"><?php echo $newsDetails['page_dont'];?></textarea></td>
                </tr>
                <?php  } ?>
                <?php if($newsDetails['template_id']==2){?>
                <tr id="tr_howit_works">
                  <td><label class="tip" >HowItWorks:</label></td>
                  <td><textarea class="mceEditor" id="page_howit_works" name="page_howit_works" style="height:250px; width:550px;"><?php echo $newsDetails['page_howit_works'];?></textarea></td>
                </tr>
                <tr id="tr_quizlinktext">
                  <td ><label class="tip" >QuizLinkText:</label></td>
                  <td ><input name="page_quizlinktext" type="text" class="textbox_long" id="page_quizlinktext" value="<?php echo $newsDetails['page_quizlinktext']; ?>" /></td>
                </tr>
                <tr id="tr_prospeechlinktext">
                  <td><label class="tip" >ProspeechLinkText:</label></td>
                  <td><input name="page_prospeechlinktext" type="text" class="textbox_long" id="page_prospeechlinktext" value="<?php echo $newsDetails['page_prospeechlinktext']; ?>" /></td>
                </tr>
                <tr id="tr_first_intro_title">
                  <td ><label class="tip" >First_Intro_Title:</label></td>
                  <td ><input name="page_first_intro_title" type="text" class="textbox_long" id="page_first_intro_title" value="<?php echo $newsDetails['page_first_intro_title']; ?>" /></td>
                </tr>
                <tr id="tr_HowItWorksList">
                  <td><label class="tip" >HowItWorksList:</label></td>
                  <td><textarea class="mceEditor" id="page_HowItWorksList" name="page_HowItWorksList" style="height:250px; width:550px;"><?php echo $newsDetails['page_HowItWorksList'];?></textarea></td>
                </tr>
                <tr id="tr_first_intro_footer">
                  <td ><label class="tip" >First_Into_Footer:</label></td>
                  <td ><input name="page_first_intro_footer" type="text" class="textbox_long" id="page_first_intro_footer" value="<?php echo $newsDetails['page_first_intro_footer']; ?>" /></td>
                </tr>
                <?php }} ?>
                <tr>
                  <td><label>Page status</label></td>
                  <td><input type="checkbox" name="status" value="1" style="border:none" <?php if($newsDetails['status']=="1"){?> checked<?php }?>>
                    (Checked=Active; Uncheck=Inactive);</td>
                </tr>
                <tr>
                  <td ><label class="tip" >Meta Title Tag:</label></td>
                  <td ><textarea id="page_seo_meta_title" name="page_seo_meta_title" style="height:100px; width:550px;"><?php echo $newsDetails['page_seo_meta_title'];?></textarea></td>
                </tr>
                <tr>
                  <td ><label class="tip" >Meta Keyword Tags:</label></td>
                  <td ><textarea id="page_seo_meta_keyword" name="page_seo_meta_keyword" style="height:100px; width:550px;"><?php echo $newsDetails['page_seo_meta_keyword'];?></textarea></td>
                </tr>
                <tr>
                  <td ><label class="tip" >Meta Description:</label></td>
                  <td ><textarea id="page_seo_meta_description" name="page_seo_meta_description" style="height:200px; width:550px;"><?php echo $newsDetails['page_seo_meta_description'];?></textarea></td>
                </tr>
                <tr>
                  <td colspan="2" class="tablesRowHeadingBG"></td>
                </tr>
              </table>
              <div class="clear"> </div>
            </div>
            <div align="center">
              <input name="button" type="reset" class="button small fl" value="Back" title="Back" onClick="javascript: window.location.href='page.php';" />
              <input name="submit" type="submit" class="button" title="Save" value="Save" />
            </div>
          </form>
        </div>
        <!-- End Quick Index --> 
        
      </div>
      <!-- End Grid --> 
      
    </div>
    <!-- End Page Content  -->
    <div class="clear"> </div>
  </div>
</div>
<div class="footer"> </div>
</body>
</html>
