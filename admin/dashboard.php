<?php
require_once("includes/application-top.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo SITE_NAME?>:: Admin Panel::</title>
<link type="text/css" rel="stylesheet" media="all" href="css/base.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/jquery-ui.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/grid.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/visualize.css" />
</head>
<body id="actcategory">
<div id="header">
  <div class="header-top tr">
    <p>logged in as <?php echo $_SESSION['session_admin_username'];?>, <?php echo date("D j M Y");?></p>
  </div>
  <div class="header-middle"> 
    <!-- Start Top Nav -->
    <?php include_once("includes/top_nav.php");?>
    <!-- End Top Nav -->
    <div class="clear"> </div>
  </div>
</div>
<div id="page-wrapper">
  <div class="page"> 
    <!-- Start Sidebar -->
    <?php include_once("includes/dashboard-header.php");?>
    <!-- End Sidebar --> 
    <!-- Star Page Content  -->
    <div id="page-content"> 
      <!-- Start Page Header -->
      <div id="page-header">
        <h1>Dashboard</h1>
      </div>
      <!-- End Page Header -->
      <?php if($_REQUEST['msg']!=""){?>
      <div class="notification success"> <span class="strong">SUCCESS!</span> <?php echo $_REQUEST['msg'];?> </div>
      <?php }?>
      <!-- Start Grid -->
      <div class="container_12"> 
        <!-- Start Quick Index -->
        <div class="grid_12">
          <div class="box-header">Options</div>
          <div class="box"> 
            <!-- Start dashboard List -->
            <div class="dashboard-list">
              <ul>
                <li><a href="<?php echo SITE_ADMIN_URL;?>orders.php"><img src="images/order-ico.png" /><span>Orders</span></a></li>
                <li><a href="<?php echo SITE_ADMIN_URL;?>purchasers.php"><img src="images/purchasers-ico.png" /><span>Purchasers</span></a></li>
                <?php if($_SESSION['session_admin_usertype']=="Super Admin") { ?>
                <li><a href="<?php echo SITE_ADMIN_URL;?>page.php"><img src="images/web-content-ico.png"/><span>Website Content</span></a></li>
                  <li><a href="<?php echo SITE_ADMIN_URL;?>page-confetti.php"><img src="images/web-content-ico.png"/><span>Confetti Website Content</span></a></li>
                <li><a href="<?php echo SITE_ADMIN_URL;?>speech-para.php"><img src="images/speech-ico.png" /><span>Speech Paragraph</span></a></li>
                <li><a href="<?php echo SITE_ADMIN_URL;?>speech-section.php"><img src="images/speech-section-ico.png" /><span>Speech Section</span></a></li>
                <li><a href="<?php echo SITE_ADMIN_URL;?>speech.php"><img src="images/speech-para-ico.png" /><span>Speeches</span></a></li>
                <li><a href="<?php echo SITE_ADMIN_URL;?>quiz.php"><img src="images/quiz-question-ico.png" /><span>Quiz Questions</span></a></li>
                <li><a href="<?php echo SITE_ADMIN_URL;?>edit-setting.php?setting_id=1"><img src="images/general-setting-ico.png"/><span>General Settings</span></a></li>
                <li><a href="<?php echo SITE_ADMIN_URL;?>newsletter.php"><img src="images/new-subscribe-ico.png" /><span>Newsletter Subscribes</span></a></li>
                <li><a href="<?php echo SITE_ADMIN_URL;?>client-feedback.php"><img src="images/msg.png" /><span>Customer Feedbacks</span></a></li>
                 <li><a href="<?php echo SITE_ADMIN_URL;?>reset-orders.php"><img src="images/order-ico.png" /><span>Reset Orders</span></a></li>
				<?php }?>
                
              </ul>
            </div>
            <div style="clear:both;"></div>
            <!-- End dashboard List --> 
          </div>
        </div>
        <!-- End Contact Us --> 
        <br class="cl" />
      </div>
    </div>
    <!-- End Page Content  --> 
  </div>
</div>
<?php if($_SESSION['session_admin_usertype']!="Super Admin") { 
				$cssFootr = 'style="position:fixed; left:0; bottom:0; width:100%;"';
				}?>
<div class="footer" <?php echo $cssFootr;?>> Copyright &copy;<?php echo date('Y');?></div>
</body>
</html>
