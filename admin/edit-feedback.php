<?php
require_once("includes/application-top.php");
require_once("includes/classes/class.Feedback.php");

$objAdmin = new Admins();
$objAdmin->fun_authenticate_admin();
$objFeedback = new Feedback();


if(isset($_GET['cPath'])){
	$cID = $_GET['cPath'];
}else{
	$cID = 0;
}
if(!empty($_GET['page'])){
	$page = $_GET['page'];
}else{
	$page = 1;
}
$cateID = $_REQUEST['feedbackID'];

$newsDetails = $objFeedback->funGetFeedbackInfo($cateID);

if($_POST['submit'] == "Submit" || $_POST['submit'] == "Save"){
	
	if($_POST['securityKey']==md5("ADDAREA")){ // EDIT news
		$affectedRows = $objFeedback->processFeedback($_REQUEST['feedbackID'], 'EDIT');
		if($affectedRows < 0){
			$msg = "Unable to edit feedback! Please try again.";
			$msgType = "1";
		}else{
			$msg = "Feedback have been updated successfully!";
			$msgType = "2";
		}
	}
	redirectURL("client-feedback.php?msgtype=".$msgType."&msg=".urlencode($msg));
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>WEB ADMIN SECTION</title>
<link type="text/css" rel="stylesheet" media="all" href="css/base.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/jquery-ui.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/grid.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/visualize.css" />
<script src="tinymce/jscripts/tiny_mce/tiny_mce_dev.js" type="text/javascript"></script>
<script type="text/javascript">
tinyMCE.init({
        // General options
        mode : "textareas",
       theme : "advanced",
       theme_advanced_buttons1 : "bold,italic,underline,strikethrough,justifyleft,justifycenter,justifyright,justifyfull,bullist,numlist,cut,image,anchor,blockquote,separator,copy,paste,undo,redo,link,unlink,outdent,indent,code",
	   theme_advanced_buttons2 : "removeformat,formatselect,fontselect,fontsizeselect,styleselect,forecolor,backcolor,forecolorpicker,backcolorpicker",
	   theme_advanced_buttons3 : "",
        editor_selector : "mceSimple",
		 theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
       
        //theme_advanced_toolbar_location : "top",
        //theme_advanced_toolbar_align : "left",
        //theme_advanced_statusbar_location : "bottom",
        //theme_advanced_resizing : true,

        // Skin options
        skin : "o2k7",
        skin_variant : "silver",


        // Replace values for the template plugin
        template_replace_values : {
                username : "Some User",
                staffid : "991234"
        }
});
</script>
<script src="jscript/jquery-1.5.1.js" type="text/javascript"></script>
<script src="jscript/jquery.min.js" type="text/javascript"></script>
<script src="jscript/jquery-ui.min.js" type="text/javascript"></script>
<script type="text/javascript">
	$(function() {
		$( "#date" ).datepicker(
		{
		dateFormat: 'dd/mm/yy'
		}
		);
	});
	</script>
<script language="javascript" type="text/javascript">
function validateForm(){
	var frm = document.form1;
		if(frm.client_name.value==""){
			frm.client_name.style.borderColor='#FF0000';
			frm.client_name.focus();
			return false;
		}
		if(frm.location.value==""){
			frm.location.style.borderColor='#FF0000';
			frm.location.focus();
			return false;
		}
		if(frm.feedback_text.value==""){
			frm.feedback_text.style.borderColor='#FF0000';
			frm.feedback_text.focus();
			return false;
		}
		if(frm.date.value==""){
			frm.date.style.borderColor='#FF0000';
			frm.date.focus();
			return false;
		}
}
function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : event.keyCode;
          if (charCode != 46 && charCode != 45 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }

</script>
</head>

<body id="actcategory">
<div id="header">
  <div class="header-top tr">
    <p>logged in as <?php echo $_SESSION['session_admin_username'];?>, <?php echo date("D j M Y");?></p>
  </div>
  <div class="header-middle"> 
    
    <!-- Start Top Nav -->
    <?php include_once("includes/top_nav.php");?>
    <!-- End Top Nav -->
    
    <div class="clear"> </div>
  </div>
</div>
<div id="page-wrapper">
  <div class="page"> 
    
    <!-- Start Sidebar -->
    <?php include_once("includes/dashboard-header.php");?>
    <!-- End Sidebar --> 
    
    <!-- Star Page Content  -->
    <div id="page-content"> 
      <!-- Start Page Header -->
      <div id="page-header">
        <h1>Edit Quiz question</h1>
      </div>
      <?php if($_REQUEST['msg']!=""){?>
      <div class="notification success"> <span class="strong">SUCCESS!</span> <?php echo $_REQUEST['msg'];?> </div>
      <?php }?>
      <!-- End Page Header --> 
      
      <!-- Start Grid -->
      <div class="container_12"> 
        
        <!-- Start Quick Index -->
        <div class="grid_12">
          <div class="box-header">Edit Quiz question</div>
          <form action="" method="post" enctype="multipart/form-data" name="form1" onSubmit="return validateForm();">
            <input type="hidden" name="securityKey" value="<?php echo md5("ADDAREA");?>" />
            <input type="hidden" name="au_id" value="<?php echo $_SESSION['session_admin_userid']?>" />
            <input type="hidden" name="feedbackID" value="<?php echo $newsDetails['feedback_id']; ?>" />
            <div class="box table">
              <table width="100%"  border="0" cellspacing="0" cellpadding="0" class="module-d tbllist" style="border: solid 1px #cccccc;">
                <tr >
                  <td width="150"><label class="tip" >Customer Name:</label></td>
                  <td><input name="client_name" type="text" class="textbox_long" id="client_name" value="<?php echo $newsDetails['client_name']; ?>" /></td>
                </tr>
                <tr >
                  <td width="150"><label class="tip" >Location:</label></td>
                  <td><input name="location" type="text" class="textbox_long" id="location" value="<?php echo $newsDetails['location']; ?>" /></td>
                </tr>
                <tr>
                  <td ><label class="tip" >Feedback Text:</label></td>
                  <td ><textarea id="feedback_text" name="feedback_text" style="height:250px; width:550px;"><?php echo $newsDetails['feedback_text'];?></textarea></td>
                </tr>
                <?php 
				$booking_request_date=$newsDetails['date'];
				$booking_date=explode('-',$booking_request_date);?>
                <tr >
                  <td width="150"><label class="tip" >Date:</label></td>
                  <td><input name="date" type="text" class="textbox_long" id="date" value="<?php echo $booking_date[2]."/".$booking_date[1]."/".$booking_date[0];?>" /></td>
                </tr>
                <tr>
                  <td width="150"><label>Show on home page</label></td>
                  <td><input type="checkbox" name="home_page_status" value="1" style="border:none" <?php if($newsDetails['home_page_status']=="1"){?> checked<?php }?>>
                    (Checked=Yes; Uncheck=No);</td>
                </tr>
                <tr>
                  <td width="150"><label>Show on Groomlist site</label></td>
                  <td><input type="checkbox" name="groom_page_status" value="1" style="border:none" <?php if($newsDetails['groom_page_status']=="1"){?> checked<?php }?>>
                    (Checked=Yes; Uncheck=No);</td>
                </tr>
                <tr >
                  <td width="150"><label class="tip" >List Order:</label></td>
                  <td><input name="list_order" type="text" class="textbox_long" id="list_order" value="<?php echo $newsDetails['list_order']; ?>" /></td>
                </tr>
                <tr>
                  <td width="150"><label>Status</label></td>
                  <td><input type="checkbox" name="status" value="1" style="border:none" <?php if($newsDetails['status']=="1"){?> checked<?php }?>>
                    (Checked=Active; Uncheck=Inactive);</td>
                </tr>
                <tr>
                  <td colspan="2" class="tablesRowHeadingBG"></td>
                </tr>
              </table>
              <div class="clear"> </div>
            </div>
            <div align="center">
              <input name="button" type="reset" class="button small fl" value="Back" title="Back" onClick="javascript: window.location.href='client-feedback.php';" />
              <input name="submit" type="submit" class="button" title="Save" value="Save" />
            </div>
          </form>
        </div>
        <!-- End Quick Index --> 
        
      </div>
      <!-- End Grid --> 
      
    </div>
    <!-- End Page Content  -->
    <div class="clear"> </div>
  </div>
</div>
<div class="footer"> </div>
</body>
</html>
