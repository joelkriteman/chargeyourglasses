<?php
require_once("includes/application-top.php");

$objAdmin = new Admins();
$objAdmin->fun_authenticate_admin();

$dbObj = new DB();
$dbObj->fun_db_connect();

	
 $sqlSelCate = "SELECT * FROM site_contact";
  
if($searchWhere!="")
{
$sqlSelCate .= $searchWhere;
}
$cateResult = $dbObj->fun_db_query($sqlSelCate);
$totRecords = $dbObj->fun_db_get_num_rows($cateResult);
if($totRecords>$limit)
{
 $pagelinks = paginate($limit, $totRecords);
 }else
 {
 $pagelinks=1;
 }
$dbObj->fun_db_free_resultset($cateResult);

$sqlSelCate .= " ORDER BY contact_id DESC ";
$cateResult = $dbObj->fun_db_query($sqlSelCate);
$Total = $dbObj->fun_db_get_num_rows($cateResult);

$strData="Name, Email,Phone Number,Speech For,Message, Date\n";

while($rowsTesti = $dbObj->fun_db_fetch_rs_object($cateResult))
{
	
if(fun_db_output($rowsTesti->speech_for)==1)
 $speech_for= "Best Man"; 
 elseif(fun_db_output($rowsTesti->speech_for)==2)
  $speech_for= "Father Of the Bride"; 
  else 
  $speech_for= "Groom";	
$strData.=fun_db_output($rowsTesti->name).",".fun_db_output($rowsTesti->email) .",".fun_db_output($rowsTesti->phone_number) .",".fun_db_output($speech_for) .",".fun_db_output($rowsTesti->message) ." ,".fun_db_output($rowsTesti->added_date);
$strData.="\n";

}
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
header("Content-Type: application/octet-stream");
header("Content-Transfer-Encoding: binary"); 
header("Content-Disposition: attachment; filename=contact-us-enquiries.csv");	
echo $strData;
exit();

?>
