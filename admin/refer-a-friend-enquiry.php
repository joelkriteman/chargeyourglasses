<?php
require_once("includes/application-top.php");

$objAdmin = new Admins();
$objAdmin->fun_authenticate_admin();

$dbObj = new DB();
$dbObj->fun_db_connect();

$searchStr = "";
$searchCon = "";
$searchWhere = "";

	
 $sqlSelCate = "SELECT * FROM site_refer_friend";
  
if($searchWhere!="")
{
$sqlSelCate .= $searchWhere;
}
$cateResult = $dbObj->fun_db_query($sqlSelCate);
$totRecords = $dbObj->fun_db_get_num_rows($cateResult);
if($totRecords>$limit)
{
 $pagelinks = paginate($limit, $totRecords);
 }else
 {
 $pagelinks=1;
 }
$dbObj->fun_db_free_resultset($cateResult);

$sqlSelCate .= " ORDER BY id DESC ";
$cateResult = $dbObj->fun_db_query($sqlSelCate);
$Total = $dbObj->fun_db_get_num_rows($cateResult);


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>WEB ADMIN SECTION</title>
	<link type="text/css" rel="stylesheet" media="all" href="css/base.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/jquery-ui.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/grid.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/visualize.css" />
	<script type="text/javascript" src="jscript/jquery.js"></script>
	<script type="text/javascript" src="fancybox/jquery.mousewheel-3.0.2.pack.js"></script>
	<script type="text/javascript" src="fancybox/jquery.fancybox-1.3.1.js"></script>
	<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox-1.3.1.css" media="screen" />
	<script type="text/javascript">
		$(document).ready(function() {
			$(".various1").fancybox({
				'titleShow'		: false,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none'
			});
		});
	</script>
	</head>

	<body id="actcategory">
<div id="header">
      <div class="header-top tr">
    <p>logged in as <?php echo $_SESSION['session_admin_username'];?>, <?php echo date("D j M Y");?></p>
  </div>
      <div class="header-middle"> 
    
    <!-- Start Top Nav -->
    <?php include_once("includes/top_nav.php");?>
    <!-- End Top Nav -->
    
    <div class="clear"> </div>
  </div>
    </div>
<div id="page-wrapper">
      <div class="page"> 
    
    <!-- Start Sidebar -->
    <?php include_once("includes/dashboard-header.php");?>
    <!-- End Sidebar --> 
    
    <!-- Star Page Content  -->
    <div id="page-content"> 
          <!-- Start Page Header -->
          <div id="page-header">
        <h1>Refer a friend Enquiries</h1>
      </div>
          <!-- End Page Header -->
          <?php if($_REQUEST['msg']!=""){?>
          <div class="notification success"> <span class="strong">SUCCESS!</span> <?php echo $_REQUEST['msg'];?> </div>
          <?php }?>
          
          <!-- Start Grid -->
          <div class="container_12"> 
        
        <!-- Start Quick Index -->
        <div class="grid_12"> 
              <!--<div class="box-header">Refine list by</div>--> 
              <!--<div class="box table tablebg">
            <form name="searchFrm" action="" method="get">
              <input name="cPath" type="hidden"  style="width:180px" class="textbox_long" value="<?php echo $_GET['cPath']; ?>" />
              <table border="0" cellspacing="0" cellpadding="0" class="search">
                <thead>
                  <tr>
                    <td ><label>Page Name:</label></td>
                    <td>  <input type="hidden" name="cPath" value="<?php echo $_GET['cPath'];?>">
				<input name="spagetitle" type="text"  style="width:180px" class="textbox_long" value="<?php echo $spagetitle; ?>"></td>
                    <td ><label>Status:</label></td>
                    <td><input type="radio" name="spstatus" value="" <?php if($_REQUEST['spstatus']==''){echo "checked";} ?> /></td>
                    <td><label>All</label></td>
                    <td><input type="radio" name="spstatus" value="1" <?php if($_REQUEST['spstatus']=='1'){echo "checked";} ?> /></td>
                    <td><label><?php echo STATUS_ACTIVE?></label></td>
                    <td><input type="radio" name="spstatus" value="0" <?php if($_REQUEST['spstatus']=='0'){echo "checked";} ?> /></td>
                    <td><label><?php echo STATUS_INACTIVE?></label></td>
                    <td ><input type="submit" value="Update" class="button small" name="searchBttn" alt="Search" title="Search"/></td>
                  </tr>
                </thead>
              </table>
            </form>
          </div>--> 
              
              <!-- End Quick Index --> 
              
              <!-- Start Open Enquiries -->
              
              <div class="box-header">
            <table class="td-middle">
                  <tr>
                <td><b>Refer a friend Enquiries</b></td>
                <td width="150"><form name="dateRange" method="post" action="refer-friend-export.php" class="fr">
                    <input type="submit" name="searchBttn" value="Export to CSV" class="button small fr" />
                  </form></td>
              </tr>
                </table>
          </div>
              <div class="box table">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <thead>
                <tr class="bgcolor">
                      <td width="15%">Name</td>
                      <td width="22%" >Email</td>
                      <td width="18%" >Friend's Name</td>
                      <td width="23%" >Friend's E-mail</td>
                      <td width="13%" >Message</td>
                      <td width="9%">Date</td>
                    </tr>
              </thead>
                  <tbody>
                <?php
									if($totRecords>0)
		{
			$cnt = 0;
	if(($_GET['page'])>1)
	{
	 $page=$_GET['page'];
	  $i=(($page-1)*10)+1;
	}
	else
	{
	$i=1;										
	}										
		
	while($rowsCate = $dbObj->fun_db_fetch_rs_object($cateResult)){
		$cnt++;
		if($cnt % 2 == 0){
			$alternateStyle="tablesRowBG_1";
		}else{
			$alternateStyle="tablesRowBG_2";
		}
	?>
                <tr >
                      <td><?php echo fun_db_output($rowsCate->yourname);?></td>
                      <td><?php echo fun_db_output($rowsCate->youremail);?></td>
                      <td><?php echo fun_db_output($rowsCate->friendname);?></td>
                      <td><?php echo fun_db_output($rowsCate->friendemail);?></td>
                      <td><a class="various1" href="view-refer-a-friend-message.php?ID=<?php echo $rowsCate->id;?>">View message</a></td>
                      <td><?php echo fun_site_date_format(fun_db_output($rowsCate->added_date))?></td>
                    </tr>
                <?php $i=$i+1;} }
    else
  {
	echo "<tr><td><td colspan=\"6\"><font color=\"#FF0000\">No Results Found.</font></td></tr>";  
  }
  ?>
              </tbody>
                  <thead>
                <tr class="bgcolor">
                      <td colspan="6" class="tl">Total Records: <?php echo $Total;?></td>
                    </tr>
              </thead>
                </table>
            <div class="clear"> </div>
          </div>
            </div>
        <div class="clear"> </div>
      </div>
          <!-- End Open Enquiries -->
          <div class="clear"> </div>
        </div>
    <!-- End Grid -->
    <div class="clear"> </div>
  </div>
      <!-- End Page Content  -->
      <div class="clear"> </div>
    </div>
<div class="clear"> </div>
<div class="footer"> </div>
</body>
</html>
