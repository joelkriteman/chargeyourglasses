<?php
require_once("includes/application-top.php");
require_once("includes/classes/class.Quiz.php");
require_once("includes/classes/class.Person.php");

$objAdmin = new Admins();
$objAdmin->fun_authenticate_admin();
$objQuiz = new Quiz();
$objPerson = new Person();


if(isset($_GET['cPath'])){
	$cID = $_GET['cPath'];
}else{
	$cID = 0;
}
if(!empty($_GET['page'])){
	$page = $_GET['page'];
}else{
	$page = 1;
}

if($_POST['submit'] == "Submit" || $_POST['submit'] == "Save"){
	
	if($_POST['securityKey']==md5("ADDAREA")){ // EDIT news
		$affectedRows = $objQuiz->processQuiz();
		if($affectedRows < 0){
			$msg = "Unable to add quiz question details! Please try again.";
			$msgType = "1";
		}else{
			$msg = "Quiz question details have been added successfully!";
			$msgType = "2";
		}
	}
	redirectURL("quiz.php?msgtype=".$msgType."&msg=".urlencode($msg));
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>WEB ADMIN SECTION</title>
<link type="text/css" rel="stylesheet" media="all" href="css/base.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/jquery-ui.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/grid.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/visualize.css" />
<script language="javascript" type="text/javascript">
function validateForm(){
	var frm = document.form1;
		if(frm.quiz_ques_text.value==""){
			frm.quiz_ques_text.style.borderColor='#FF0000';
			frm.quiz_ques_text.focus();
			return false;
		}
		if(frm.quiz_score_1.value==""){
			frm.quiz_score_1.style.borderColor='#FF0000';
			frm.quiz_score_1.focus();
			return false;
		}
		if(frm.quiz_score_2.value==""){
			frm.quiz_score_2.style.borderColor='#FF0000';
			frm.quiz_score_2.focus();
			return false;
		}
		if(frm.quiz_score_3.value==""){
			frm.quiz_score_3.style.borderColor='#FF0000';
			frm.quiz_score_3.focus();
			return false;
		}
		if(frm.quiz_score_4.value==""){
			frm.quiz_score_4.style.borderColor='#FF0000';
			frm.quiz_score_4.focus();
			return false;
		}

}
function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : event.keyCode;
          if (charCode != 46 && charCode != 45 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }

</script>
</head>

<body id="actcategory">
<div id="header">
  <div class="header-top tr">
    <p>logged in as <?php echo $_SESSION['session_admin_username'];?>, <?php echo date("D j M Y");?></p>
  </div>
  <div class="header-middle"> 
    
    <!-- Start Top Nav -->
    <?php include_once("includes/top_nav.php");?>
    <!-- End Top Nav -->
    
    <div class="clear"> </div>
  </div>
</div>
<div id="page-wrapper">
  <div class="page"> 
    
    <!-- Start Sidebar -->
    <?php include_once("includes/dashboard-header.php");?>
    <!-- End Sidebar --> 
    
    <!-- Star Page Content  -->
    <div id="page-content"> 
      <!-- Start Page Header -->
      <div id="page-header">
        <h1>Add New Quiz question</h1>
      </div>
      <?php if($_REQUEST['msg']!=""){?>
      <div class="notification success"> <span class="strong">SUCCESS!</span> <?php echo $_REQUEST['msg'];?> </div>
      <?php }?>
      <!-- End Page Header --> 
      
      <!-- Start Grid -->
      <div class="container_12"> 
        
        <!-- Start Quick Index -->
        <div class="grid_12">
          <div class="box-header">Add New Quiz question</div>
          <form action="" method="post" enctype="multipart/form-data" name="form1" onSubmit="return validateForm();">
            <input type="hidden" name="securityKey" value="<?php echo md5("ADDAREA");?>" />
            <input type="hidden" name="au_id" value="<?php echo $_SESSION['session_admin_userid']?>" />
            <div class="box table">
              <table width="100%"  border="0" cellspacing="0" cellpadding="0" class="module-d tbllist" style="border: solid 1px #cccccc;">
                <tr >
                  <td width="150"><label class="tip" >Person:</label></td>
                  <td><select name="person_id">
                      <?php echo $objPerson->fun_getPersonListNameOptions();?>
                    </select></td>
                </tr>
                <tr >
                  <td width="150"><label class="tip" >Question:</label></td>
                  <td><input name="quiz_ques_text" type="text" class="textbox_long" id="quiz_ques_text" value="<?php echo $newsDetails['quiz_ques_text']; ?>" /></td>
                </tr>
                <tr >
                  <td width="150"><label class="tip" >Score (Definitely):</label></td>
                  <td><input name="quiz_score_1" type="text" class="" id="quiz_score_1" value="<?php echo $newsDetails['quiz_score_1']; ?>" onkeypress="return isNumberKey(event);"/>
                    </td>
                </tr>
                <tr >
                  <td width="150"><label class="tip" >Score (Pretty Sure):</label></td>
                  <td><input name="quiz_score_2" type="text" class="" id="quiz_score_2" value="<?php echo $newsDetails['quiz_score_2']; ?>" onkeypress="return isNumberKey(event);"/>
                    </td>
                </tr>
                <tr >
                  <td width="150"><label class="tip" >Score (Possibly):</label></td>
                  <td><input name="quiz_score_3" type="text" class="" id="quiz_score_3" value="<?php echo $newsDetails['quiz_score_3']; ?>" onkeypress="return isNumberKey(event);"/>
                    </td>
                </tr>
                <tr >
                  <td width="150"><label class="tip" >Score (Your Joking?):</label></td>
                  <td><input name="quiz_score_4" type="text" class="" id="quiz_score_4" value="<?php echo $newsDetails['quiz_score_4']; ?>" onkeypress="return isNumberKey(event);"/>
                    </td>
                </tr>
                <tr >
                  <td width="150"><label class="tip" >List Order:</label></td>
                  <td><input name="list_order" type="text" class="textbox_long" id="list_order" value="<?php echo $newsDetails['list_order']; ?>" /></td>
                </tr>
                <tr>
                  <td width="150"><label>Status</label></td>
                  <td><input type="checkbox" name="status" value="1" style="border:none" <?php if($newsDetails['status']=="1"){?> checked<?php }?>>
                    (Checked=Active; Uncheck=Inactive);</td>
                </tr>
                <tr>
                  <td colspan="2" class="tablesRowHeadingBG"></td>
                </tr>
              </table>
              <div class="clear"> </div>
            </div>
            <div align="center">
              <input name="button" type="reset" class="button small fl" value="Back" title="Back" onClick="javascript: window.location.href='quiz.php';" />
              <input name="submit" type="submit" class="button" title="Save" value="Save" />
            </div>
          </form>
        </div>
        <!-- End Quick Index --> 
        
      </div>
      <!-- End Grid --> 
      
    </div>
    <!-- End Page Content  -->
    <div class="clear"> </div>
  </div>
</div>
<div class="footer"> </div>
</body>
</html>
