<?php
require_once("includes/application-top.php");
require_once("includes/classes/class.SpeechPara.php");
require_once("includes/classes/class.Person.php");
require_once("includes/classes/class.SpeechSection.php");
require_once("includes/classes/class.Speech.php");

$objAdmin = new Admins();
$objAdmin->fun_authenticate_admin();
$objSpeechPara = new SpeechPara();
$objPerson = new Person();
$objSpeechSection = new SpeechSection();
$objSpeech = new Speech();

if(isset($_GET['cPath'])){
	$cID = $_GET['cPath'];
}else{
	$cID = 0;
}
if(!empty($_GET['page'])){
	$page = $_GET['page'];
}else{
	$page = 1;
}
$cateID = $_REQUEST['speechID'];

$newsDetails = $objSpeech->funGetSpeechInfo($cateID);

if($_POST['submit'] == "Submit" || $_POST['submit'] == "Save"){
	
	if($_POST['securityKey']==md5("ADDAREA")){ // EDIT news
		$affectedRows =  $objSpeech->processSpeech($_REQUEST['speechID'], 'EDIT');
		if($affectedRows < 0){
			$msg = "Unable to edit speech description! Please try again.";
			$msgType = "1";
		}else{
			$msg = "Speech description have been updated successfully!";
			$msgType = "2";
		}
	}
	redirectURL("speech.php?msgtype=".$msgType."&msg=".urlencode($msg));
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>WEB ADMIN SECTION</title>
<link type="text/css" rel="stylesheet" media="all" href="css/base.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/jquery-ui.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/grid.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/visualize.css" />
<script src="tinymce/jscripts/tiny_mce/tiny_mce_dev.js" type="text/javascript"></script>
<script type="text/javascript">
tinyMCE.init({
        // General options
        mode : "textareas",
       theme : "advanced",
       theme_advanced_buttons1 : "bold,italic,underline,strikethrough,justifyleft,justifycenter,justifyright,justifyfull,bullist,numlist,cut,image,anchor,blockquote,separator,copy,paste,undo,redo,link,unlink,outdent,indent,code",
	   theme_advanced_buttons2 : "removeformat,formatselect,fontselect,fontsizeselect,styleselect,forecolor,backcolor,forecolorpicker,backcolorpicker",
	   theme_advanced_buttons3 : "",
        editor_selector : "mceSimple",
		 theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
       
        //theme_advanced_toolbar_location : "top",
        //theme_advanced_toolbar_align : "left",
        //theme_advanced_statusbar_location : "bottom",
        //theme_advanced_resizing : true,

        // Skin options
        skin : "o2k7",
        skin_variant : "silver",


        // Replace values for the template plugin
        template_replace_values : {
                username : "Some User",
                staffid : "991234"
        }
});
</script>
<script type="text/javascript" src="jscript/jquery.js"></script>
<script language="javascript" type="text/javascript">
function validateForm(){
	var frm = document.form1;
	if(frm.person_id.value==""){
			frm.person_id.style.borderColor='#FF0000';
			frm.person_id.focus();
			return false;
		}
		if(frm.speech_para_id.value==""){
			frm.speech_para_id.style.borderColor='#FF0000';
			frm.speech_para_id.focus();
			return false;
		}
		if(frm.speech_section_id.value==""){
			frm.speech_section_id.style.borderColor='#FF0000';
			frm.speech_section_id.focus();
			return false;
		}

		if(frm.speech_section_text.value==""){
			frm.speech_section_text.style.borderColor='#FF0000';
			frm.speech_section_text.focus();
			return false;
		}

}
function chngeParagraph(val)
{
	var ajaxaction='getSpeechParaByPerson';
	$.post('ajaxcon2.php', {ajaxaction:ajaxaction,person_id:val}, function(data) {
    //alert(data);
	$('#speech_para_id').html(data);
});
chngeSpeechSection();
}
function chngeSpeechSection(val)
{
	var ajaxaction='getSpeechSectionByPara';
	$.post('ajaxcon2.php', {ajaxaction:ajaxaction,speech_para_id:val}, function(data) {
    //alert(data);
	$('#speech_section_id').html(data);
});
}

</script>
</head>

<body id="actcategory">
<div id="header">
  <div class="header-top tr">
    <p>logged in as <?php echo $_SESSION['session_admin_username'];?>, <?php echo date("D j M Y");?></p>
  </div>
  <div class="header-middle"> 
    
    <!-- Start Top Nav -->
    <?php include_once("includes/top_nav.php");?>
    <!-- End Top Nav -->
    
    <div class="clear"> </div>
  </div>
</div>
<div id="page-wrapper">
  <div class="page"> 
    
    <!-- Start Sidebar -->
    <?php include_once("includes/dashboard-header.php");?>
    <!-- End Sidebar --> 
    
    <!-- Star Page Content  -->
    <div id="page-content"> 
      <!-- Start Page Header -->
      <div id="page-header">
        <h1>Edit Speech </h1>
      </div>
      <?php if($_REQUEST['msg']!=""){?>
      <div class="notification success"> <span class="strong">SUCCESS!</span> <?php echo $_REQUEST['msg'];?> </div>
      <?php }?>
      <!-- End Page Header --> 
      
      <!-- Start Grid -->
      <div class="container_12"> 
        
        <!-- Start Quick Index -->
        <div class="grid_12">
          <div class="box-header">Edit Speech</div>
          <form action="" method="post" enctype="multipart/form-data" name="form1" onSubmit="return validateForm();">
            <input type="hidden" name="securityKey" value="<?php echo md5("ADDAREA");?>" />
            <input type="hidden" name="au_id" value="<?php echo $_SESSION['session_admin_userid']?>" />
            <input type="hidden" name="speechID" value="<?php echo $newsDetails['speech_id']; ?>" />
            <div class="box table">
              <table width="100%"  border="0" cellspacing="0" cellpadding="0" class="module-d tbllist" style="border: solid 1px #cccccc;">
                <tr >
                  <td width="150"><label class="tip" >Person:</label></td>
                  <td><select name="person_id" onchange="javascript: chngeParagraph(this.value);">
                      <?php echo $objPerson->fun_getPersonListNameOptions($newsDetails['person_id']);?>
                    </select></td>
                </tr>
                <tr >
                  <td width="150"><label class="tip" >Speech Paragraph:</label></td>
                  <td><select name="speech_para_id" id="speech_para_id" onchange="javascript: chngeSpeechSection(this.value);">
                      <?php echo $objSpeechPara->fun_getSpeechParaListNameOptions($newsDetails['speech_para_id']);?>
                    </select></td>
                </tr>
                <tr >
                  <td width="150"><label class="tip" >Speech Section:</label></td>
                  <td><select name="speech_section_id" id="speech_section_id">
                  <option value="">Please select speech paragraph first</option>
                      <?php echo $objSpeechSection->fun_getSpeechSectionListNameOptions($newsDetails['speech_section_id']);?>
                    </select></td>
                </tr>
               <tr >
                  <td width="150"><label class="tip" >Speech Description:</label></td>
                  <td><textarea class="mceEditor" id="speech_text" name="speech_text" style="height:250px; width:550px;"><?php echo $newsDetails['speech_text'];?></textarea></td>
                </tr>
                <tr >
                  <td width="150"><label class="tip" >List Order:</label></td>
                  <td><input name="list_order" type="text" class="textbox_long" id="list_order" value="<?php echo $newsDetails['list_order']; ?>" /></td>
                </tr>
                <tr>
                  <td width="150"><label>Status</label></td>
                  <td><input type="checkbox" name="status" value="1" style="border:none" <?php if($newsDetails['status']=="1"){?> checked<?php }?>>
                    (Checked=Active; Uncheck=Inactive);</td>
                </tr>
                <tr>
                  <td colspan="2" class="tablesRowHeadingBG"></td>
                </tr>
              </table>
              <div class="clear"> </div>
            </div>
            <div align="center">
              <input name="button" type="reset" class="button small fl" value="Back" title="Back" onClick="javascript: window.location.href='speech.php';" />
              <input name="submit" type="submit" class="button" title="Save" value="Save" />
            </div>
          </form>
        </div>
        <!-- End Quick Index --> 
        
      </div>
      <!-- End Grid --> 
      
    </div>
    <!-- End Page Content  -->
    <div class="clear"> </div>
  </div>
</div>
<div class="footer"> </div>
</body>
</html>
