<?php
require_once("includes/application-top.php");

if(isset($_POST['securityKey']) && $_POST['securityKey']==md5("AKHILFORGOTPWD")){
	$objAdmin = new Admins();
	
	$adminEmail = fun_db_output($_POST['au_email']);
	if(($adminEmail!="")&&($adminEmail!="Enter your username"))
	{
	if(!$objAdmin->fun_check_username_admin_existance($adminEmail))
	{
	redirectURL(SITE_ADMIN_SECURE_URL."forget-pwd.php?msg=".urlencode("Please enter correct username!"));
	}
		else
	{
		$resetPwd="EMS-".rand(1000,100000);
		$objAdmin->resetPwd($adminEmail,$resetPwd);
		$adminInfo = $objAdmin->fun_getAdminUserInfo(0, $adminEmail);
		$adminTypeId= $adminInfo['au_type_id'];
		$adminTypeInfo = $objAdmin->fun_getAdminUserTypeInfo($adminTypeId);
		$body ="Dear User,\r\n\r\n";
		$body .= "Your password has been changed successfully. Please see the details.\r\n\r\n";
		$body .= 'Username: ' . $adminEmail . "\r\n";
		$body .= 'Password: ' . $resetPwd . "\r\n";
		$body .= "Many Thanks,\r\n";
		$body .= "Support Team\r\n";
  
		$to=$adminEmail;
	  	$from=SITE_SUPPORT_EMAIL_ID;
	 	$subject = 'Your Access Password - Casapicola';
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: Support <' . $from  . "> \r\n";
		$mailSentStatus = mail($to , $subject, $body, $headers);
		redirectURL(SITE_ADMIN_SECURE_URL."index.php?msg=".urlencode("We have successfully sent your login details in your email!"));


	}

	}

}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?php echo SITE_NAME?> :: Admin Panel:: </title>
<link type="text/css" rel="stylesheet" media="all" href="css/base.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/jquery-ui.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/grid.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/visualize.css" />
<style>
body
{ overflow-x:hidden;
}
td{ border:none;}
.header-bg1{ background:url(images/header-middle.gif) repeat-x;-moz-box-shadow: 0px 0px 5px #000; /* Firefox */
  -webkit-box-shadow: 0px 0px 5px #000; /* Safari and Chrome */
  box-shadow: 0px 0px 5px #000; border:#505050 4px solid; border-left:0; border-right:0;}
.login-bg1{ background:url(images/boxheader.gif) repeat-x #CDCDCD; border-top-left-radius: 1em;
    border-top-right-radius: 1em;border-bottom:#aeb1b0 1px solid;}
.forgot{ color:#be0100; text-decoration:none;}
</style>
	<script language="javascript" type="text/javascript">
	
	function funSetCursor(){
		var au_email = document.getElementById("au_email");
		au_email.focus();
	}
	
	function validatefrm()
	{
	var frm = document.adminLoginFrm;
	if(((frm.au_email.value=="")||(frm.au_email.value=="Enter your username"))){
			alert("Error: Please enter username !");
			frm.au_email.focus();
			return false;
		}
		
	return true;
	}
	</script>
</head>
<body onLoad="javascript: funSetCursor();">
<div class="header-bg1">
    <div style="position:relative; left:0px; top:-1px; padding-left:10px; background-color:#F1F1F1;  ">
    <img id="logo" src="images/shortlets-logo5.jpg" alt="Short Lets Logo" /></a> 
    </div>  
    <div style="clear:both;"></div>
</div>
    <div style="clear:both;"></div>
<div style="width:750px; margin:0 auto;">

<table  border="0" cellpadding="0" cellspacing="0">

<tr >
	<!-- middle start -->
	<td height="456"  align="center">
		<?php
		if(isset($_GET['msg'])){
		?>
		<div style="font-size:11px; margin-bottom:25px; font-size:12px; font-family:Verdana, Arial, Helvetica, sans-serif; color:#333333;">
		<b><?php echo urldecode($_GET['msg'])?></b><br>
		</div>
		<?php
		}
		?>
		
		<table cellpadding="0" cellspacing="10" width="40%" border="0" bgcolor="#FFFFFF" style="border:1px solid #333333; border-radius:1em;">
        <tr><td width="31%" height="35" colspan="4" align="center" class="login-bg1"><b style="text-shadow: 1px 1px 1px #FFFFFF;"><font size="+1">Forgot Password</font></b></td></tr>
		<tr>
			<td>
				<table cellpadding="0" cellspacing="1"  border="0">
				<tr>
					<td colspan="2"><img src="images/client-login.png"/></td>
					<td width="77%">
                     
                    <form name="adminLoginFrm" method="post" action="" onSubmit="return validatefrm();">
                     <input type="hidden" name="ip" value="<?php echo  $_SERVER['REMOTE_ADDR'];?>" />
                       <input type="hidden" name="backUrl" value="<?php echo  $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];?>" />
                       <input type="hidden" name="securityKey" value="<?php echo md5("AKHILFORGOTPWD")?>" />
						<table width="100%" border="0" cellpadding="4" cellspacing="0" >
                       
                        <tr>
							<td width="31%" height="35"><b>Username</b></td>
							<td width="69%"><input type="text" name="au_email" id="au_email" value="Enter your username" size="20"  style="height:20px; width:180px;" onClick="this.value=''" />
						</tr>
                      
						<tr>
							<td>&nbsp;</td>
							<td><input type="submit"  name="subBttn" value="Reset Password" class="button small " /></td>
                            
						</tr>
						
					  </table></form>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
	<!-- middle end -->

</table></td></td></td></div>

</body>
</html>