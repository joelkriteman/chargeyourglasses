<?php
require_once("../includes/application-top-inner.php");
$action="User has logged out";
//$log=processAdminLog($action);

	unset($_SESSION['session_admin_userid']);
	unset($_SESSION['session_admin_username']);
	unset($_SESSION['session_admin_password']);
	unset($_SESSION['session_admin_usertype']);
	unset($_SESSION['location_id']);
	session_unset();
	
	session_destroy();
	
	redirectURL(SITE_ADMIN_SECURE_URL."index.php?msg=".urlencode("You have logged out successfully"));
?>