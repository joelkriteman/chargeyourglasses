<?php
require_once("includes/application-top.php");
require_once("includes/classes/class.Feedback.php");

$objAdmin = new Admins();
$objAdmin->fun_authenticate_admin();

$dbObj = new DB();
$dbObj->fun_db_connect();
$objFeedback = new Feedback();

$searchStr = "";
$searchCon = "";
$searchWhere = "";
if($_GET['action']==md5("DeleteFeedback"))
{
	$objFeedback->funDeleteFeedback($_REQUEST['FeedId']);
	?>
<script type="text/javascript">
	 window.location.href="client-feedback.php?msg=Selected feedback has been deleted successfully";
</script>
<?php
}


$spagetitle = fun_db_output($_REQUEST['spagetitle']);
$spstatus = fun_db_output($_REQUEST['spstatus']);

$searchStr = "&page=" . $page  . "&spagetitle=" . urlencode($spagetitle)."&spstatus=" . urlencode($spstatus);


if($_REQUEST['person_id']!=""){
	$searchTxt .= $searchCon . " person_id = '".fun_db_input($_REQUEST['person_id'])."' ";
	$searchCon = " AND ";
}
if($spagetitle!=""){
	$searchTxt .= $searchCon . " client_name LIKE '%".fun_db_input($spagetitle)."%' ";
	$searchCon = " AND ";
}

if($spstatus!=""){
	$searchTxt .= $searchCon . "status='".fun_db_input($spstatus)."' ";
	$searchCon .= " AND ";
}

if($searchTxt!=""){
	$searchWhere = " WHERE " . $searchTxt;
}
	
 $sqlSelCate = "SELECT * FROM " . TABLE_FEEDBACKS ;
  
if($searchWhere!="")
{
$sqlSelCate .= $searchWhere;
}
$cateResult = $dbObj->fun_db_query($sqlSelCate);
$totRecords = $dbObj->fun_db_get_num_rows($cateResult);
if($totRecords>$limit)
{
 $pagelinks = paginate($limit, $totRecords);
 }else
 {
 $pagelinks=1;
 }
$dbObj->fun_db_free_resultset($cateResult);

$sqlSelCate .= " ORDER BY feedback_id DESC ";
$cateResult = $dbObj->fun_db_query($sqlSelCate);
$Total = $dbObj->fun_db_get_num_rows($cateResult);


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>WEB ADMIN SECTION</title>
<link type="text/css" rel="stylesheet" media="all" href="css/base.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/jquery-ui.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/grid.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/visualize.css" />
<script type="text/javascript" src="../jscript/jquery.js"></script>
<script type="text/javascript">
function DeleteFeedback(FeedId)
{
	var r=confirm("Are you sure to delete!");
if (r==true)
  {
	  
	  window.location.href="client-feedback.php?action=<?php echo md5("DeleteFeedback")?>&FeedId="+FeedId;
  }
}
</script>
</head>

<body id="actcategory">
<div id="header">
  <div class="header-top tr">
    <p>logged in as <?php echo $_SESSION['session_admin_username'];?>, <?php echo date("D j M Y");?></p>
  </div>
  <div class="header-middle"> 
    
    <!-- Start Top Nav -->
    <?php include_once("includes/top_nav.php");?>
    <!-- End Top Nav -->
    
    <div class="clear"> </div>
  </div>
</div>
<div id="page-wrapper">
  <div class="page"> 
    
    <!-- Start Sidebar -->
    <?php include_once("includes/dashboard-header.php");?>
    <!-- End Sidebar --> 
    
    <!-- Star Page Content  -->
    <div id="page-content"> 
      <!-- Start Page Header -->
      <div id="page-header">
        <h1>Customer Feedbacks</h1>
      </div>
      <!-- End Page Header -->
      <?php if($_REQUEST['msg']!=""){?>
      <div class="notification success"> <span class="strong">SUCCESS!</span> <?php echo $_REQUEST['msg'];?> </div>
      <?php }?>
      
      <!-- Start Grid -->
      <div class="container_12"> 
        
        <!-- Start Quick Index -->
        <div class="grid_12">
          <div class="box-header">Refine list by</div>
          <div class="box table tablebg">
            <form name="searchFrm" action="" method="get">
              <input name="cPath" type="hidden"  style="width:180px" class="textbox_long" value="<?php echo $_GET['cPath']; ?>" />
              <table border="0" cellspacing="0" cellpadding="0" class="search td-middle">
                <thead>
                  <tr>
                    <td ><label>Customer:</label></td>
                    <td><input name="spagetitle" type="text"  style="width:180px" class="textbox_long" value="<?php echo $spagetitle; ?>"></td>
                    <td ><label>Status:</label></td>
                    <td><input type="radio" name="spstatus" value="" <?php if($_REQUEST['spstatus']==''){echo "checked";} ?> />
                      <label>All</label></td>
                    <td><input type="radio" name="spstatus" value="1" <?php if($_REQUEST['spstatus']=='1'){echo "checked";} ?> />
                      <label><?php echo STATUS_ACTIVE?></label></td>
                    <td><input type="radio" name="spstatus" value="0" <?php if($_REQUEST['spstatus']=='0'){echo "checked";} ?> />
                      <label><?php echo STATUS_INACTIVE?></label></td>
                    <td ><input type="submit" value="Update" class="button small" name="searchBttn" alt="Search" title="Search"/></td>
                  </tr>
                </thead>
              </table>
            </form>
          </div>
          
          <!-- End Quick Index --> 
          
          <!-- Start Open Enquiries -->
          
          <div class="box-header">
            <table class="td-middle">
              <tr>
                <td><b>Customer Feedbacks</b></td>
                <td width="150"><input type="button" name="addArea" value="Add new Feedback" class="button small fr" onClick="javascript: window.location.href='add-feedback.php'" /></td>
              </tr>
            </table>
          </div>
          <div class="box table">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <thead>
                <tr class="bgcolor">
                  <td>Customer(Location)</td>
                  <td>Show on Groomlist</td>
                  <td>Show on home page</td>
                  <td>List Order</td>
                  <td>Status</td>
                  <td>Last Modified</td>
                  <td width="40">Actions</td>
                </tr>
              </thead>
              <tbody>
                <?php
									if($totRecords>0)
		{
			$cnt = 0;
	if(($_GET['page'])>1)
	{
	 $page=$_GET['page'];
	  $i=(($page-1)*10)+1;
	}
	else
	{
	$i=1;										
	}										
		
	while($rowsCate = $dbObj->fun_db_fetch_rs_object($cateResult)){
		$cnt++;
		if($cnt % 2 == 0){
			$alternateStyle="tablesRowBG_1";
		}else{
			$alternateStyle="tablesRowBG_2";
		}
	?>
                <tr >
                  <td><?php echo fun_db_output($rowsCate->client_name)."(".fun_db_output($rowsCate->location).")";?></td>
                   <td><?php if($rowsCate->groom_page_status==1){
					echo "<font color='#006600'>Yes</font>";
				}else{
					echo "<font color='#ff0000'>No</font>";
				}?></td>
                  <td><?php if($rowsCate->home_page_status==1){
					echo "<font color='#006600'>Yes</font>";
				}else{
					echo "<font color='#ff0000'>No</font>";
				}?></td>
                  <td><?php echo fun_db_output($rowsCate->list_order);?></td>
                  <td><?php
				if($rowsCate->status==1){
					echo "<font color='#006600'>Active</font>";
				}else{
					echo "<font color='#ff0000'>In-Active</font>";
				}
			?></td>
                  <td><?php echo fun_site_date_format(fun_db_output($rowsCate->last_modified))?></td>
                  <td width="10%"><a href="edit-feedback.php?feedbackID=<?php echo fun_db_output($rowsCate->feedback_id);?>"><img src="images/edit-ico.png" /></a>
                  <a class="padding5" onClick="javascript: DeleteFeedback('<?php echo fun_db_output($rowsCate->feedback_id);?>')"><img src="images/delete-ico.png" /></a></td>
                </tr>
                <?php $i=$i+1;} }
    else
  {
	echo "<tr><td><td colspan=\"7\"><font color=\"#FF0000\">No Results Found.</font></td></tr>";  
  }
  ?>
              </tbody>
              <thead>
                <tr class="bgcolor">
                  <td colspan="7" class="tl">Total Records: <?php echo $Total;?></td>
                </tr>
              </thead>
            </table>
            <div class="clear"> </div>
          </div>
        </div>
        <div class="clear"> </div>
      </div>
      <!-- End Open Enquiries -->
      <div class="clear"> </div>
    </div>
    <!-- End Grid -->
    <div class="clear"> </div>
  </div>
  <!-- End Page Content  -->
  <div class="clear"> </div>
</div>
<div class="clear"> </div>
<div class="footer"> </div>
</body>
</html>
