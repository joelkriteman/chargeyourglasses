<?php
require_once("includes/application-top.php");

$objAdmin = new Admins();
$objAdmin->fun_authenticate_admin();

$dbObj = new DB();
$dbObj->fun_db_connect();

	
 $sqlSelCate = "SELECT * FROM newsletter";
  
if($searchWhere!="")
{
$sqlSelCate .= $searchWhere;
}
$cateResult = $dbObj->fun_db_query($sqlSelCate);
$totRecords = $dbObj->fun_db_get_num_rows($cateResult);
if($totRecords>$limit)
{
 $pagelinks = paginate($limit, $totRecords);
 }else
 {
 $pagelinks=1;
 }
$dbObj->fun_db_free_resultset($cateResult);

$sqlSelCate .= " ORDER BY newsletter_id DESC ";
$cateResult = $dbObj->fun_db_query($sqlSelCate);
$Total = $dbObj->fun_db_get_num_rows($cateResult);

$strData="Name, Email, Date\n";

while($rowsTesti = $dbObj->fun_db_fetch_rs_object($cateResult))
{	
$strData.=fun_db_output($rowsTesti->name).",".fun_db_output($rowsTesti->email) ." ,".fun_db_output($rowsTesti->added_date);
$strData.="\n";

}
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
header("Content-Type: application/octet-stream");
header("Content-Transfer-Encoding: binary"); 
header("Content-Disposition: attachment; filename=newsletter.csv");	
echo "newsletter\n";
echo $strData;
exit();

?>
