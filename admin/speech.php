<?php
require_once("includes/application-top.php");
require_once("includes/classes/class.SpeechSection.php");
require_once("includes/classes/class.Person.php");
require_once("includes/classes/class.SpeechPara.php");
require_once("includes/classes/class.Speech.php");

$objAdmin = new Admins();
$objAdmin->fun_authenticate_admin();

$dbObj = new DB();
$dbObj->fun_db_connect();
$objSpeechSection = new SpeechSection();
$objPerson = new Person();
$objSpeechPara = new SpeechPara();
$objSpeech = new Speech();

$searchStr = "";
$searchCon = "";
$searchWhere = "";
if($_GET['action']==md5("DeleteSection"))
{
	$objSpeech->funDeleteSpeech($_REQUEST['speechID']);
	?>
<script type="text/javascript">
	 window.location.href="speech.php?msg=Selected speech details have been deleted successfully";
</script>
<?php
}


$spagetitle = fun_db_output($_REQUEST['spagetitle']);
$spstatus = fun_db_output($_REQUEST['spstatus']);

$searchStr = "&page=" . $page  . "&spagetitle=" . urlencode($spagetitle)."&spstatus=" . urlencode($spstatus);


if($_REQUEST['person_id']!=""){
	$searchTxt .= $searchCon . " person_id = '".fun_db_input($_REQUEST['person_id'])."' ";
	$searchCon = " AND ";
}
if($_REQUEST['speech_para_id']!=""){
	$searchTxt .= $searchCon . " speech_para_id = '".fun_db_input($_REQUEST['speech_para_id'])."' ";
	$searchCon = " AND ";
}
if($_REQUEST['speech_section_id']!=""){
	$searchTxt .= $searchCon . " speech_section_id = '".fun_db_input($_REQUEST['speech_section_id'])."' ";
	$searchCon = " AND ";
}


if($spagetitle!=""){
	$searchTxt .= $searchCon . " speech_text LIKE '%".fun_db_input($spagetitle)."%' ";
	$searchCon = " AND ";
}

if($spstatus!=""){
	$searchTxt .= $searchCon . "status='".fun_db_input($spstatus)."' ";
	$searchCon .= " AND ";
}

if($searchTxt!=""){
	$searchWhere = " WHERE " . $searchTxt;
}
	
 $sqlSelCate = "SELECT * FROM " . TABLE_SPEECH ;
  
if($searchWhere!="")
{
$sqlSelCate .= $searchWhere;
}
$cateResult = $dbObj->fun_db_query($sqlSelCate);
$totRecords = $dbObj->fun_db_get_num_rows($cateResult);
if($totRecords>$limit)
{
 $pagelinks = paginate($limit, $totRecords);
 }else
 {
 $pagelinks=1;
 }
$dbObj->fun_db_free_resultset($cateResult);

$sqlSelCate .= " ORDER BY speech_id DESC ";
$cateResult = $dbObj->fun_db_query($sqlSelCate);
$Total = $dbObj->fun_db_get_num_rows($cateResult);


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>WEB ADMIN SECTION</title>
<link type="text/css" rel="stylesheet" media="all" href="css/base.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/jquery-ui.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/grid.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/visualize.css" />
<script type="text/javascript" src="jscript/jquery.js"></script>
<script type="text/javascript">
function DeleteSpeech(speechID)
{
	var r=confirm("Are you sure to delete!");
if (r==true)
  {
	  
	  window.location.href="speech.php?action=<?php echo md5("DeleteSection")?>&speechID="+speechID;
  }
}
function chngeParagraph(val)
{
	var ajaxaction='getSpeechParaByPersonShowall';
	$.post('ajaxcon2.php', {ajaxaction:ajaxaction,person_id:val}, function(data) {
    //alert(data);
	$('#speech_para_id').html(data);
});
}
function chngeSpeechSection(val)
{
	var ajaxaction='getSpeechSectionByParaShowall';
	$.post('ajaxcon2.php', {ajaxaction:ajaxaction,speech_para_id:val}, function(data) {
    //alert(data);
	$('#speech_section_id').html(data);
});
}

</script>
</head>

<body id="actcategory">
<div id="header">
  <div class="header-top tr">
    <p>logged in as <?php echo $_SESSION['session_admin_username'];?>, <?php echo date("D j M Y");?></p>
  </div>
  <div class="header-middle"> 
    
    <!-- Start Top Nav -->
    <?php include_once("includes/top_nav.php");?>
    <!-- End Top Nav -->
    
    <div class="clear"> </div>
  </div>
</div>
<div id="page-wrapper">
  <div class="page"> 
    
    <!-- Start Sidebar -->
    <?php include_once("includes/dashboard-header.php");?>
    <!-- End Sidebar --> 
    
    <!-- Star Page Content  -->
    <div id="page-content"> 
      <!-- Start Page Header -->
      <div id="page-header">
        <h1>Speeches</h1>
      </div>
      <!-- End Page Header -->
      <?php if($_REQUEST['msg']!=""){?>
      <div class="notification success"> <span class="strong">SUCCESS!</span> <?php echo $_REQUEST['msg'];?> </div>
      <?php }?>
      
      <!-- Start Grid -->
      <div class="container_12"> 
        
        <!-- Start Quick Index -->
        <div class="grid_12">
          <div class="box-header">Refine list by</div>
          <div class="box table tablebg">
            <form name="searchFrm" action="" method="get">
              <input name="cPath" type="hidden"  style="width:180px" class="textbox_long" value="<?php echo $_GET['cPath']; ?>" />
              <table border="0" cellspacing="0" cellpadding="0" class="search td-middle">
                <thead>
                  <tr>
                    <td ><label>Person:</label></td>
                    <td colspan="2"><select name="person_id" onChange="javascript: chngeParagraph(this.value);">
                        <option value="">Show All</option>
                        <?php echo $objPerson->fun_getPersonListNameOptions($_REQUEST['person_id']);?>
                      </select></td>
                    <td colspan="2"><label>Speech Paragraph:</label></td>
                    <td colspan="2"><select name="speech_para_id" id="speech_para_id" onChange="javascript: chngeSpeechSection(this.value);">
                    <option value="">Show All</option>
                      <?php echo $objSpeechPara->fun_SearchSpeechParaListNameOptions($_REQUEST['speech_para_id'],$_REQUEST['person_id']);?>
                    </select></td>
                  </tr>
                  <tr>
                    <td ><label>Speech Section:</label></td>
                    <td><select name="speech_section_id" id="speech_section_id">
                    <option value="">Show All</option>
                      <?php echo $objSpeechSection->fun_SearchSpeechParaListNameOptions($_REQUEST['speech_section_id'],$_REQUEST['speech_para_id']);?>
                    </select></td>
                    <td ><label>Status:</label></td>
                    <td><input type="radio" name="spstatus" value="" <?php if($_REQUEST['spstatus']==''){echo "checked";} ?> />
                      <label>All</label></td>
                    <td><input type="radio" name="spstatus" value="1" <?php if($_REQUEST['spstatus']=='1'){echo "checked";} ?> />
                      <label><?php echo STATUS_ACTIVE?></label></td>
                    <td><input type="radio" name="spstatus" value="0" <?php if($_REQUEST['spstatus']=='0'){echo "checked";} ?> />
                      <label><?php echo STATUS_INACTIVE?></label></td>
                    <td ><input type="submit" value="Update" class="button small" name="searchBttn" alt="Search" title="Search"/></td>
                  </tr>
                </thead>
              </table>
            </form>
          </div>
          
          <!-- End Quick Index --> 
          
          <!-- Start Open Enquiries -->
          
          <div class="box-header">
            <table class="td-middle">
              <tr>
                <td><b>Speeches</b></td>
                <td width="150"><input type="button" name="addArea" value="Add new speech" class="button small fr" onClick="javascript: window.location.href='add-speech.php'" /></td>
              </tr>
            </table>
          </div>
          <div class="box table">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table-border">
              <thead>
                <tr class="bgcolor">
                  <td>Speech</td>
                  <td>Speech Section</td>
                  <td>Paragraph Title (Person)</td>
                  <td>List Order</td>
                  <td>Status</td>
                  <td>Last Modified</td>
                  <td width="40">Actions</td>
                </tr>
              </thead>
              <tbody>
                <?php
									if($totRecords>0)
		{
			$cnt = 0;
	if(($_GET['page'])>1)
	{
	 $page=$_GET['page'];
	  $i=(($page-1)*10)+1;
	}
	else
	{
	$i=1;										
	}										
		
	while($rowsCate = $dbObj->fun_db_fetch_rs_object($cateResult)){
		$cnt++;
		if($cnt % 2 == 0){
			$alternateStyle="tablesRowBG_1";
		}else{
			$alternateStyle="tablesRowBG_2";
		}
	?>
                <tr >
                  <td><?php echo fun_db_output($rowsCate->speech_text);?></td>
                  <td><?php  $speechsectionName= $objSpeechSection->funGetSpeechSectionInfo(fun_db_output($rowsCate->speech_section_id)); 
				  echo $speechsectionName['speech_section_text'];?></td>
                  <td><?php 
				  $speechParaName= $objSpeechPara->funGetParaInfo(fun_db_output($rowsCate->speech_para_id));
				  $personName= $objPerson->funGetPersonInfo(fun_db_output($rowsCate->person_id));
				  echo $speechParaName['speech_para_text']." (".$personName['person_name'].")";
				  ?></td>
                  <td><?php echo fun_db_output($rowsCate->list_order);?></td>
                  <td><?php
				if($rowsCate->status==1){
					echo "<font color='#006600'>Active</font>";
				}else{
					echo "<font color='#ff0000'>In-Active</font>";
				}
			?></td>
                  <td><?php echo fun_site_date_format(fun_db_output($rowsCate->last_modified))?></td>
                  <td width="10%"><a href="edit-speech.php?speechID=<?php echo fun_db_output($rowsCate->speech_id);?>"><img src="images/edit-ico.png" /></a>
                  <a class="padding5" onClick="javascript: DeleteSpeech('<?php echo fun_db_output($rowsCate->speech_id);?>')"><img src="images/delete-ico.png" /></a></td>
                </tr>
                <?php $i=$i+1;} }
    else
  {
	echo "<tr><td><td colspan=\"7\"><font color=\"#FF0000\">No Results Found.</font></td></tr>";  
  }
  ?>
              </tbody>
              <thead>
                <tr class="bgcolor">
                  <td colspan="7" class="tl">Total Records: <?php echo $Total;?></td>
                </tr>
              </thead>
            </table>
            <div class="clear"> </div>
          </div>
        </div>
        <div class="clear"> </div>
      </div>
      <!-- End Open Enquiries -->
      <div class="clear"> </div>
    </div>
    <!-- End Grid -->
    <div class="clear"> </div>
  </div>
  <!-- End Page Content  -->
  <div class="clear"> </div>
</div>
<div class="clear"> </div>
<div class="footer"> </div>
</body>
</html>
