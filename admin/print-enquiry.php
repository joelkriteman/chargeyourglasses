<?php
require_once("includes/application-top.php");
require_once("includes/classes/class.Orders.php");
require_once("includes/classes/class.Users.php");

$objAdmin = new Admins();
$objAdmin->fun_authenticate_admin();

$dbObj = new DB();
$dbObj->fun_db_connect();
$objOrders = new Orders();
$objUsers = new Users();

$returnUrl ="";

$cateID =$_REQUEST['order_id'];
$orderDetails = $objOrders->funGetOrderInfo($cateID);
$customerDetails= $objUsers->funGetUserInfo(fun_db_output($orderDetails['user_id']));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>WEB ADMIN SECTION</title>
	<link type="text/css" rel="stylesheet" media="all" href="css/base.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/jquery-ui.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/grid.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/visualize.css" />
	<script src="jscript/jquery-1.5.1.js" type="text/javascript"></script>
	<script src="jscript/jquery.min.js" type="text/javascript"></script>
	<script src="jscript/jquery-ui.min.js" type="text/javascript"></script>
	<script type="text/javascript">
	$(function() {
		$( "#datepicker" ).datepicker(
		{
		dateFormat: 'dd/mm/yy'
		}
		);
		$( "#datepicker1" ).datepicker(
		{
		dateFormat: 'dd/mm/yy'
		}
		);
	});
	</script>
	</head>

	<body id="actcategory" onload="print();">
<div id="page-wrapper">
      <div class="page"> 
    
    <!-- Start Sidebar --> 
    <!-- End Sidebar --> 
    
    <!-- Star Page Content  -->
    <div id="page-content"> 
          <!-- Start Page Header -->
          <div id="page-header">
        <h1>Order Details</h1>
      </div>
          <!-- End Page Header -->
          <?php if($_REQUEST['msg']!=""){?>
          <div class="notification success"> <span class="strong">SUCCESS!</span> <?php echo $_REQUEST['msg'];?> </div>
          <?php }?>
          
          <!-- Start Grid -->
          <div class="container_12"> 
        
        <!-- Start Main Content -->
        <div class="grid_12"> 
              
              <!-- End Page Header -->
              
              <div class="box-header"><span class="fr">Order Date : <?php echo fun_site_date_format($orderDetails['added_date'])?></span>
            <div class="tl">Order No: <?php echo fun_db_output($orderDetails['order_number'])?></div>
            <div class="clear"> </div>
          </div>
              <div class="box table">
            <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                  <tr>
                <td width="50%"><label class="fl">Total Amount :</label>
                      <span class="fr">&pound; <?php echo fun_db_output($orderDetails['total_amount']);?></span></td>
                <td><label class="fl">Payment Status:</label>
                      <span class="fr">
                  <?php
				if($orderDetails['payment_status_id']==1){
					echo "<font color='#006600'>Paid</font>";
				}else{
					echo "<font color='#ff0000'>Not Paid</font>";
				}
			?>
                  </span></td>
              </tr>
                  <tr>
                <td width="50%"><label class="fl">Website :</label>
                      <span class="fr">
                  <?php
				if($orderDetails['website_id']==2){
					echo "<font color='#ff0000'>Groomlist</font>";
				}else{
					echo "<font color='#006600'>General</font>";
				}
			?>
                  </span></td>
                <td>&nbsp;</td>
              </tr>
                </table>
          </div>
              
              <!-- Start Guest Details -->
              <div class="box-header">Customer Details</div>
              <div class="box table">
            <table cellspacing="0" cellpadding="3" width="100%" border="0">
                  <tr >
                <td width="45%"><label class="fl">Customer Name : </label>
                      <span class="fr"><?php echo $customerDetails['user_fname']."&nbsp;".$customerDetails['user_lname']?></span></td>
                <td width="5%"></td>
                <td ><label class="fl">Customer Email :</label>
                      <span class="fr"><?php echo $customerDetails['user_email']?></span></td>
              </tr>
                  <tr>
                <td><label class="fl">Wedding Date:</label>
                      <span class="fr"><?php echo $customerDetails['user_wed_date']?></span></td>
                <td></td>
                <td><label class="fl">Role:</label>
                      <span class="fr"><?php echo $customerDetails['user_role'];?></span></td>
              </tr>
                </table>
          </div>
              <!-- END Guest Details --> 
              
              <!-- Start Payment Details --> 
              
              <!-- END LogFile -->
              <div class="clear"> </div>
              
            </div>
        <!-- End Main Content --> 
        
      </div>
          <!-- End Open Enquiries -->
          <div class="clear"> </div>
        </div>
    <!-- End Grid -->
    <div class="clear"> </div>
  </div>
      <!-- End Page Content  -->
      <div class="clear"> </div>
    </div>
<div class="clear"> </div>
</body>
</html>
