<?php
require_once("includes/application-top.php");
require_once("includes/classes/class.Orders.php");
require_once("includes/classes/class.Users.php");
require_once("includes/classes/class.Setting.php");
require_once("includes/functions/general.php");
require_once("includes/common.php");
$setting = new Setting(); 
$objAdmin = new Admins();
$objAdmin->fun_authenticate_admin();
$dbObj = new DB();
$dbObj->fun_db_connect();
$objOrders = new Orders();
$objUsers = new Users();

if(!empty($_GET['page'])){
	$page = $_GET['page'];
} else {
	$page = 1;
}

$limit = 30;
$start = ($page - 1) * $limit;

$searchStr = "";
$searchCon = "";
$searchWhere = "";

$spagetitle = fun_db_output($_REQUEST['spagetitle']);
$spstatus = fun_db_output($_REQUEST['spstatus']);

$searchStr = "&page=" . $page  . "&spagetitle=" . urlencode($spagetitle)."&spstatus=" . urlencode($spstatus);
$date=explode('/',$_REQUEST['from_date']);
$fromDate=$date[2]."-".$date[1]."-".$date[0];
$date1=explode('/',$_REQUEST['till_date']);
$fromDate1=$date1[2]."-".$date1[1]."-".$date1[0];
$returnUrl ="";

if($_REQUEST['user_id']!=""){
	$searchTxt .= $searchCon . " user_id = '".fun_db_input($_REQUEST['user_id'])."' ";
	$searchCon = " AND ";
	$returnUrl = "user_id=".$_REQUEST['user_id'];
}
if($spagetitle!=""){
	$searchTxt .= $searchCon . " order_number LIKE '%".fun_db_input($spagetitle)."%' ";
	$searchCon = " AND ";
}
if(($_REQUEST['from_date']!="")&&($_REQUEST['from_date']!="dd/mm/yyyy")){
	$searchTxt .= $searchCon . " added_date >= '".$fromDate."' ";
	$searchCon = " AND ";	
}
if(($_REQUEST['till_date']!="")&&($_REQUEST['till_date']!="dd/mm/yyyy")){
	$searchTxt .= $searchCon . " added_date <= '".$fromDate1."' ";
	$searchCon = " AND ";	
}
if($_SESSION['session_admin_usertype']!="Super Admin") { 
	$searchTxt .= $searchCon . "website_id='".$_SESSION['session_admin_userid']."' ";
	$searchCon = " AND ";
}
if($_REQUEST['website_id']!=""){
	$searchTxt .= $searchCon . "website_id='".$_REQUEST['website_id']."' ";
	$searchCon = " AND ";
}

$searchTxt .= $searchCon . "payment_status_id='0' ";
$searchCon = " AND ";

if($searchTxt!=""){
	$searchWhere = " WHERE " . $searchTxt;
}

$sqlSelCate = "SELECT * FROM " . TABLE_ORDERS ;
  
if($searchWhere!=""){
	$sqlSelCate .= $searchWhere;
}

$cateResult = $dbObj->fun_db_query($sqlSelCate);
$totRecords = $dbObj->fun_db_get_num_rows($cateResult);

if($totRecords>$limit){
	$pagelinks = paginate($limit, $totRecords);
}
$dbObj->fun_db_free_resultset($cateResult);

$sqlSelCate .= " ORDER BY order_id DESC  LIMIT $start, $limit";
$cateResult = $dbObj->fun_db_query($sqlSelCate);
$Total = $dbObj->fun_db_get_num_rows($cateResult);

if($_REQUEST['ConfirmOrderId'])
{
	$generalInfoDetail=$setting->funGetSettingInfo(1);
	$orderId = $_REQUEST['ConfirmOrderId'];
	$orderDetails = $objOrders->funGetOrderInfo($orderId);
	$orderNumber = $orderDetails['order_number'];
	$userDetails = $objUsers->funGetUserInfo($orderDetails['user_id']);
	
	$UserId = $orderDetails['user_id']; 
	$DateArr = explode("-",$userDetails['added_date']);
	$DateArrTime = explode(" ",$DateArr[2]);
	$tomorrow = mktime(0, 0, 0, date("m"), date($DateArr[2])+$generalInfoDetail['time_days'], date("y"));
	$AddDay = date("Y-m-d",$tomorrow)." $DateArrTime[1]";

	$orderUpdate = "UPDATE " . TABLE_ORDERS . " SET payment_status_id=1, order_status=1, mail_sent='yes' WHERE order_id ='".(int)$orderId."'";
	$orderUpdateVal = $dbObj->fun_db_query($orderUpdate);
	$userUpdateSql = "UPDATE " . TABLE_USERS . " SET status =1, expired_on = '".$AddDay."', total_orders = 1  WHERE user_id ='".(int)$UserId."'";
	$userDetailsUpdate = $dbObj->fun_db_query($userUpdateSql);
	if($userDetailsUpdate){
		$emailRegisterFile = SITE_EMAIL_TAMPLATE_WS . "payment-confirmation.html";
		$pwdContent = fun_getFileContent($emailRegisterFile);
		$pwdContent = str_replace("[%SITE_NAME%]", SITE_NAME, $pwdContent);
		$pwdContent = str_replace("[%NAME%]", $userDetails['user_fname']." ".$userDetails['user_lname'], $pwdContent);
		$pwdContent = str_replace("[%EMAIL%]",  $userDetails['user_email'], $pwdContent);
		$pwdContent = str_replace("[%DATE%]",  date("d/m/Y", strtotime($AddDay)), $pwdContent);
		$pwdContent = str_replace("[%AMOUNT%]", $orderDetails['total_amount']." GBP", $pwdContent);
		$pwdContent = str_replace("[%ORDERID%]", $orderNumber, $pwdContent);
		$pwdContent = str_replace("[%PAYMENTSTATUS%]", "Completed", $pwdContent);
		$pwdContent = str_replace("[%SITE_LOGO%]", EMAIL_LOGO, $pwdContent);
		
		$emailPaymentAdminFile = SITE_EMAIL_TAMPLATE_WS . "payment-confirmation-admin.html";
		$payAdminContent = fun_getFileContent($emailPaymentAdminFile);
		$payAdminContent = str_replace("[%SITE_NAME%]", SITE_NAME, $payAdminContent);
		$payAdminContent = str_replace("[%NAME%]", $userDetails['user_fname']." ".$userDetails['user_lname'], $payAdminContent);
		$payAdminContent = str_replace("[%EMAIL%]",  $userDetails['user_email'], $payAdminContent);
		$payAdminContent = str_replace("[%AMOUNT%]",  $orderDetails['total_amount']." GBP", $payAdminContent);
		$payAdminContent = str_replace("[%ORDERID%]",$orderNumber, $payAdminContent);
		$payAdminContent = str_replace("[%PAYMENTSTATUS%]", "Completed", $payAdminContent);
		$payAdminContent = str_replace("[%DATE%]",  date("d/m/Y"), $payAdminContent);
		$payAdminContent = str_replace("[%SITE_LOGO%]", EMAIL_LOGO, $payAdminContent);
		
		$to=$userDetails['user_email'];
		$to1=SITE_PAYMENT_EMAIL_ID;
		//$to="yaswant10chauhan@gmail.com";
		//$to1="yaswant10chauhan@gmail.com";
		$subject = 'Your payment details for '.SITE_NAME.'';
		$subject1 = 'Chargeyourglasses Payment details - '.$userDetails['user_email'];
		$from=SITE_SUPPORT_EMAIL_ID;
		$fromContent="Charge Your Glasses";	
		$mailSentStatus = fun_get_email($to , $subject, $pwdContent, $from,$fromContent);
		$mailSentStatus1 = fun_get_email($to1 , $subject1, $payAdminContent, $from,$fromContent);
		redirectURL("reset-orders.php?OrderMsg=yes");
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>WEB ADMIN SECTION</title>
	<link type="text/css" rel="stylesheet" media="all" href="css/base.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/jquery-ui.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/grid.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/visualize.css" />
	<script src="jscript/jquery-1.5.1.js" type="text/javascript"></script>
	<script src="jscript/jquery.min.js" type="text/javascript"></script>
	<script src="jscript/jquery-ui.min.js" type="text/javascript"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		$( "#datepicker" ).datepicker({
			dateFormat: 'dd/mm/yy'
		});
		$( "#datepicker1" ).datepicker({
			dateFormat: 'dd/mm/yy'
		});
	});
	</script>
</head>
<body id="actcategory">
	<div id="header">
		<div class="header-top tr">
			<p>logged in as <?php echo $_SESSION['session_admin_username'];?>, <?php echo date("D j M Y");?></p>
		</div>
		<div class="header-middle">
			<!-- Start Top Nav -->
			<?php include_once("includes/top_nav.php");?>
			<!-- End Top Nav -->
			<div class="clear"> </div>
		</div>
	</div>

	<div id="page-wrapper">
		<div class="page"> 
			<!-- Start Sidebar -->
			<?php include_once("includes/dashboard-header.php");?>
			<!-- End Sidebar --> 

			<!-- Star Page Content  -->
			<div id="page-content">
				<!-- Start Page Header -->
				<div id="page-header">
					<h1>List of incomplete Orders</h1>
				</div>
			  <!-- End Page Header -->
			  <?php if($_REQUEST['OrderMsg']!=""){?>
			  <div class="notification success"> <span class="strong">SUCCESS!</span> <?php if($_REQUEST['OrderMsg']=="yes") { echo "Customer order have been reset successfully!.";}?> </div>
			  <?php }?>
			  
				<!-- Start Grid -->
				<div class="container_12"> 
				
					<!-- Start Quick Index -->
					<div class="grid_12">
						<div class="box-header">Refine list by</div>
						<div class="box table">
							<form name="searchFrm" action="" method="get">
								<input type="hidden" name="user_id" value="<?php echo $_REQUEST['user_id']; ?>" />
								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="td-middle">
									<thead>
										<tr>
										<td><label>From:</label></td>
										<td><input id="datepicker" name="from_date" type="text" value="<?php if($_REQUEST['from_date']!=""){ echo $_REQUEST['from_date'];} else {?>dd/mm/yyyy<?php }?>" /></td>
										<td ><label>To:</label></td>
										<td><input id="datepicker1" name="till_date" type="text" value="<?php if($_REQUEST['till_date']!=""){ echo $_REQUEST['till_date'];} else {?>dd/mm/yyyy<?php }?>"  /></td>
										<?php if($_SESSION['session_admin_usertype']=="Super Admin"):?>
										<td>Website:</td>
										<td>
											<select name="website_id">
											<option value="">Please select website</option>
											<option value="1" <?php if($_REQUEST['website_id']==1) { ?> selected="selected" <?php } ?>>General</option>
											<option value="2" <?php if($_REQUEST['website_id']==2) { ?> selected="selected" <?php } ?>>Groomlist</option>
											<option value="3" <?php if($_REQUEST['website_id']==3) { ?> selected="selected" <?php } ?>>Confetti</option>
											</select>
										</td>
										<?php else : ?>
										<td colspan="2">&nbsp;</td>
										<?php endif; ?>
										</tr>
										<tr>
											<td ><label>Order Number:</label></td>
											<td><input name="spagetitle" type="text" value="<?php echo $spagetitle; ?>"></td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td ><input type="submit" value="Update" class="button small" name="searchBttn" alt="Preview" title="Preview"/></td>
											<td>&nbsp;</td>
										</tr>
									</thead>
								</table>
							</form>
						</div>
						<!-- End Quick Index -->
						<!-- Start Open Enquiries -->
						<div class="box-header">
							<table>
								<tr>
									<td><b>List of incomplete Orders</b></td>
								</tr>
							</table>
						</div>
						<div class="box table">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<thead>
									<tr class="bgcolor">
										<td width="87">Date</td>
										<td width="192">Order no</td>
										<td width="137">Customer Name</td>
										<td width="199">Customer Email</td>
										<td width="125">Website</td>
										<td width="127">Status</td>
										<td colspan="2">Amount</td>
									</tr>
								</thead>
								<tbody>
								<?php
								if($totRecords>0){
									$cnt = 0;
									if(($_GET['page'])>1){
										$page=$_GET['page'];
										$i=(($page-1)*10)+1;
									} else{
										$i=1;
									}
									
									$userList = array();
									while($rowsCate = $dbObj->fun_db_fetch_rs_object($cateResult)){
										$cnt++;
										if($cnt % 2 == 0){
											$alternateStyle="tablesRowBG_1";
										}else{
											$alternateStyle="tablesRowBG_2";
										}
										$queryUniq = "SELECT * FROM " . TABLE_ORDERS." WHERE user_id=".$rowsCate->user_id." and order_number != '".$rowsCate->order_number."' and order_status = 1";
										$cateResultw = $dbObj->fun_db_query($queryUniq);
										$totRecordsresrt = $dbObj->fun_db_get_num_rows($cateResultw);
										array_push($userList,$rowsCate->user_id);
										$totRecordsresrt = NULL;
									}
								
									$getUserUniqueList = array_unique($userList);
									$countArr = count($getUserUniqueList);
									foreach($getUserUniqueList as $userId){
										$userDetails= $objUsers->funGetUserInfo(fun_db_output($userId));
										$SelectQuery = "SELECT * FROM " . TABLE_ORDERS." WHERE user_id=".$userId." ORDER BY order_id DESC";
										$cateResultUnqVal = $dbObj->fun_db_query($SelectQuery);
										$rowsCateUnqVal = $dbObj->fun_db_fetch_rs_object($cateResultUnqVal);
									?>
									<?php 
									$userDetails= $objUsers->funGetUserInfo(fun_db_output($userId)); 
									$expireDate = strtotime($userDetails['expired_on']);
									$queryDate = strtotime(fun_db_output($rowsCateUnqVal->added_date));
									if($expireDate < $queryDate) {
									?>
									<tr >
										<td><?php echo fun_site_date_format(fun_db_output($rowsCateUnqVal->added_date));?></td>
										<td><a href="order-details.php?order_id=<?php echo fun_db_output($rowsCateUnqVal->order_id)."&".$returnUrl;?>"><?php echo fun_db_output($rowsCateUnqVal->order_number);?></a></td>
										<td>
											<?php 
											echo $userDetails['user_fname']." ".$userDetails['user_lname'];?>
										</td>
										<td><?php echo $userDetails['user_email'];?></td>
										<td>
										<?php 
										if($rowsCate->website_id==2){
											echo "<font color='#ff0000'>Groomlist</font>";
										}elseif($rowsCateUnqVal->website_id==3){
											echo "<font color='#00a4b9'>Confetti</font>";
										}else{
											echo "<font color='#006600'>General</font>";
										}
										?>
										</td>
										<td>
										<?php
										if($rowsCate->payment_status_id==1){
											echo "<font color='#006600'>Paid</font>";
										}else{
											echo "<font color='#ff0000'>Not Paid</font>";
										}
										?>
										</td>
										<td width="53">&pound; <?php echo fun_db_output($rowsCateUnqVal->total_amount);?></td>
										<td width="144">
											<form name="frm" method="post" action="">
												<input type="hidden" name="ConfirmOrderId" value="<?php echo $rowsCateUnqVal->order_id;?>" />
												<input type="submit" value="Confirm order" class="button small" name="confirmOrder" alt="Confirm Order" title="Confirm Order" onclick="return confirm('Are you sure you want to confirm this order!')" />
											</form>
										</td>
									</tr>
									<?php
									}
									$i=$i+1;
									} 
								} else {
									echo "<tr><td><td colspan=\"6\"><font color=\"#FF0000\">No Results Found.</font></td></tr>";  
								}
								?>
								</tbody>
								<thead>
									<tr class="bgcolor">
										<td colspan="2" class="tl">Total Records: <?php echo $countArr;?></td><td colspan="6"><?php //echo $pagelinks;?></td>
									</tr>
								</thead>
							</table>
							<div class="clear"> </div>
						</div>
						<?php if($_REQUEST['user_id']!="") {?>
						<div align="center">
							<input name="button" type="reset" class="button small fl" value="Back" title="Back" onClick="javascript: window.location.href='purchasers.php';" />
						</div>
						<?php } ?>
					</div>
					<div class="clear"></div>
				</div>
				<!-- End Open Enquiries -->
				<div class="clear"> </div>
			</div>
			<!-- End Grid -->
			<div class="clear"> </div>
		</div>
		<!-- End Page Content  -->
		<div class="clear"> </div>
	</div>
	<div class="clear"> </div>
	<?php
	if($_SESSION['session_admin_usertype']!="Super Admin") { 
		$cssFootr = 'style="position:fixed; left:0; bottom:0; width:100%;"';
	}
	?>
	<div class="footer" <?php //echo $cssFootr;?>></div>
</body>
</html>