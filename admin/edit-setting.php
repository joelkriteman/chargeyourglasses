<?php
require_once("includes/application-top.php");
require_once("includes/classes/class.Setting.php");

$objAdmin = new Admins();
$objAdmin->fun_authenticate_admin();
$objSetting = new Setting();


if(isset($_GET['cPath'])){
	$cID = $_GET['cPath'];
}else{
	$cID = 0;
}
if(!empty($_GET['page'])){
	$page = $_GET['page'];
}else{
	$page = 1;
}
$cateID = $_REQUEST['setting_id'];

$newsDetails = $objSetting->funGetSettingInfo($cateID);

if($_POST['submit'] == "Submit" || $_POST['submit'] == "Update"){
	
	if($_POST['securityKey']==md5("ADDAREA")){ // EDIT news
		$affectedRows = $objSetting->processSetting($_REQUEST['setting_id'], 'EDIT');
		if($affectedRows < 0){
			$msg = "Unable to edit general setting! Please try again.";
			$msgType = "1";
		}else{
			$msg = "General setting have been updated successfully!";
			$msgType = "2";
		}
	}
	redirectURL("edit-setting.php?setting_id=".$_REQUEST['setting_id']."&msgtype=".$msgType."&msg=".urlencode($msg));
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>WEB ADMIN SECTION</title>
<link type="text/css" rel="stylesheet" media="all" href="css/base.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/jquery-ui.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/grid.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/visualize.css" />
<script src="tinymce/jscripts/tiny_mce/tiny_mce_dev.js" type="text/javascript"></script>
<script type="text/javascript">
tinyMCE.init({
        // General options
       mode : "specific_textareas",
       editor_selector : "mceEditor",
       theme : "advanced",
	   plugins : "style",
       //theme_advanced_buttons1 : "",
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
    theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,jbimages,styleprops,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
       
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,
		relative_urls : false,

        // Skin options
        skin : "o2k7",
        skin_variant : "silver",

        // Replace values for the template plugin
        template_replace_values : {
                username : "Some User",
                staffid : "991234"
        }
});
</script>
<script type="text/javascript" src="jscript/jquery.js"></script>
<script language="javascript" type="text/javascript">
function validateForm(){
	var frm = document.form1;
		if(frm.time_days.value==""){
			frm.time_days.style.borderColor='#FF0000';
			frm.time_days.focus();
			return false;
		}
		if(frm.amount.value==""){
			frm.amount.style.borderColor='#FF0000';
			frm.amount.focus();
			return false;
		}
		if(frm.groomlist_amount.value==""){
			frm.groomlist_amount.style.borderColor='#FF0000';
			frm.groomlist_amount.focus();
			return false;
		}

}
function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : event.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
</script>

<style type="text/css">
     
      ul#tabs { list-style-type: none; margin: 30px 0 0 0; padding: 0 0 0.3em 0; }
      ul#tabs li { display: inline; }
      ul#tabs li a { color: #42454a; background-color: #dedbde; border: 1px solid #c9c3ba; border-bottom: none; padding: 0.3em; text-decoration: none; }
      ul#tabs li a:hover { background-color: #f1f0ee; }
      ul#tabs li a.selected { color: #000; background-color: #f1f0ee; font-weight: bold; padding: 0.7em 0.3em 0.38em 0.3em; }
      div.tabContent { border: 1px solid #c9c3ba; padding: 0.5em; background-color: #f1f0ee; }
      div.tabContent.hide { display: none; }
    </style>

    <script type="text/javascript">
    //<![CDATA[

    var tabLinks = new Array();
    var contentDivs = new Array();

    function init() {

      // Grab the tab links and content divs from the page
      var tabListItems = document.getElementById('tabs').childNodes;
      for ( var i = 0; i < tabListItems.length; i++ ) {
        if ( tabListItems[i].nodeName == "LI" ) {
          var tabLink = getFirstChildWithTagName( tabListItems[i], 'A' );
          var id = getHash( tabLink.getAttribute('href') );
          tabLinks[id] = tabLink;
          contentDivs[id] = document.getElementById( id );
        }
      }

      // Assign onclick events to the tab links, and
      // highlight the first tab
      var i = 0;

      for ( var id in tabLinks ) {
        tabLinks[id].onclick = showTab;
        tabLinks[id].onfocus = function() { this.blur() };
        if ( i == 0 ) tabLinks[id].className = 'selected';
        i++;
      }

      // Hide all content divs except the first
      var i = 0;

      for ( var id in contentDivs ) {
        if ( i != 0 ) contentDivs[id].className = 'tabContent hide';
        i++;
      }
    }

    function showTab() {
      var selectedId = getHash( this.getAttribute('href') );

      // Highlight the selected tab, and dim all others.
      // Also show the selected content div, and hide all others.
      for ( var id in contentDivs ) {
        if ( id == selectedId ) {
          tabLinks[id].className = 'selected';
          contentDivs[id].className = 'tabContent';
        } else {
          tabLinks[id].className = '';
          contentDivs[id].className = 'tabContent hide';
        }
      }

      // Stop the browser following the link
      return false;
    }

    function getFirstChildWithTagName( element, tagName ) {
      for ( var i = 0; i < element.childNodes.length; i++ ) {
        if ( element.childNodes[i].nodeName == tagName ) return element.childNodes[i];
      }
    }

    function getHash( url ) {
      var hashPos = url.lastIndexOf ( '#' );
      return url.substring( hashPos + 1 );
    }

    //]]>
    </script>
</head>

<body id="actcategory">
<div id="header">
  <div class="header-top tr">
    <p>logged in as <?php echo $_SESSION['session_admin_username'];?>, <?php echo date("D j M Y");?></p>
  </div>
  <div class="header-middle"> 
    
    <!-- Start Top Nav -->
    <?php include_once("includes/top_nav.php");?>
    <!-- End Top Nav -->
    
    <div class="clear"> </div>
  </div>
</div>
<div id="page-wrapper">
  <div class="page"> 
    
    <!-- Start Sidebar -->
    <?php include_once("includes/dashboard-header.php");?>
    <!-- End Sidebar --> 
    
    <!-- Star Page Content  -->
    <div id="page-content"> 
      <!-- Start Page Header -->
      <div id="page-header">
        <h1>Edit General Setting</h1>
      </div>
      
      <?php if($_REQUEST['msg']!=""){?>
      <div class="notification success"> <span class="strong">SUCCESS!</span> <?php echo $_REQUEST['msg'];?> </div>
      <?php }?>
      <!-- End Page Header --> 
      
      <!-- Start Grid -->
      <div class="container_12"> 
        
        <!-- Start Quick Index -->
        <div class="grid_12">
          <div class="box-header">Edit General Setting</div>
       <ul id="tabs">
      <li> <a href="<?php echo SITE_ADMIN_URL;?>edit-setting.php?setting_id=1" <?php if($_REQUEST['setting_id']==1){ echo "class=\"selected\""; }?>>Edit Charge Your Glasses Setting</a></li>
      <li>  <a href="<?php echo SITE_ADMIN_URL;?>edit-setting.php?setting_id=2"<?php if($_REQUEST['setting_id']==2){ echo "class=\"selected\""; }?>>Edit Groomlist Setting</a></li>
      <li><a href="<?php echo SITE_ADMIN_URL;?>edit-setting.php?setting_id=3"<?php if($_REQUEST['setting_id']==3){ echo "class=\"selected\""; }?>>Edit Confetti Setting</a></li>
    </ul>
          
          <form action="" method="post" enctype="multipart/form-data" name="form1" onSubmit="return validateForm();">
            <input type="hidden" name="securityKey" value="<?php echo md5("ADDAREA");?>" />
            <input type="hidden" name="au_id" value="<?php echo $_SESSION['session_admin_userid']?>" />
            <input type="hidden" name="setting_id" value="<?php echo $newsDetails['setting_id']; ?>" />
            <div class="box table">
              <table width="100%"  border="0" cellspacing="0" cellpadding="0" class="module-d tbllist" style="border: solid 1px #cccccc;">
                <tr >
                  <td width="150"><label class="tip" >Max. Days:</label></td>
                  <td><input name="time_days" type="text" class="" id="time_days" value="<?php echo $newsDetails['time_days']; ?>" onkeypress="return isNumberKey(event);"/>
                    (days)</td>
                </tr>
                <tr >
                  <td width="150"><label class="tip" >Price (General):</label></td>
                  <td><input name="amount" type="text" class="" id="amount" value="<?php echo $newsDetails['amount']; ?>" onkeypress="return isNumberKey(event);"/>
                    GBP (including VAT)</td>
                </tr>
                <!--<tr >
                  <td width="150"><label class="tip" >Price (Groomlist):</label></td>
                  <td><input name="groomlist_amount" type="text" class="" id="groomlist_amount" value="<?php echo $newsDetails['groomlist_amount']; ?>" onkeypress="return isNumberKey(event);"/>
                    GBP (including VAT)</td>
                </tr>-->
                <tr>
                  <td><label class="tip" >LineOfTheMonth:</label></td>
                  <td><textarea class="mceEditor" id="home_line_of_month_text" name="home_line_of_month_text" style="height:250px; width:550px;"><?php echo $newsDetails['home_line_of_month_text'];?></textarea></td>
                </tr>
                 <!--<tr>
                  <td><label class="tip" >LineOfTheMonth (Groomlist):</label></td>
                  <td><textarea class="mceEditor" id="groomlist_line_of_month_text" name="groomlist_line_of_month_text" style="height:250px; width:550px;"><?php echo $newsDetails['groomlist_line_of_month_text'];?></textarea></td>
                </tr>-->
                <tr>
                  <td><label class="tip" >Sample Speech Pdf:</label></td>
                  <td><input type="file" name="sample_speech_file"  />
                    (Display on home page)</td>
                </tr>
                <?php if($_REQUEST['setting_id']!="") {?>
                <input type="hidden" name="sample_speech_file_1" value="<?php echo $newsDetails['sample_speech_file'];?>"  />
                <tr>
                  <td><label class="tip" > Pdf:</label></td>
                  <td><a href="<?php echo SITE_PDF.$newsDetails['sample_speech_file'];?>" target="_blank">View Pdf</a></td>
                </tr>
                <?php  } ?>
                <tr>
                  <td><label class="tip" >Upload Pdf:</label></td>
                  <td><input type="file" name="sample_groom_speech_file"  />
                    (Display on groom professional speech page)</td>
                </tr>
                <?php if($_REQUEST['setting_id']!="") {?>
                <input type="hidden" name="sample_groom_speech_file_1" value="<?php echo $newsDetails['sample_groom_speech_file'];?>"  />
                <tr>
                  <td><label class="tip" > Pdf:</label></td>
                  <td><a href="<?php echo SITE_PDF.$newsDetails['sample_groom_speech_file'];?>" target="_blank">View Pdf</a></td>
                </tr>
                <?php  } ?>
                <tr>
                  <td><label class="tip" >Upload Pdf:</label></td>
                  <td><input type="file" name="sample_bestman_speech_file"  />
                    (Display on best man professional speech page)</td>
                </tr>
                <?php if($_REQUEST['setting_id']!="") {?>
                <input type="hidden" name="sample_bestman_speech_file_1" value="<?php echo $newsDetails['sample_bestman_speech_file'];?>"  />
                <tr>
                  <td><label class="tip" > Pdf:</label></td>
                  <td><a href="<?php echo SITE_PDF.$newsDetails['sample_bestman_speech_file'];?>" target="_blank">View Pdf</a></td>
                </tr>
                <?php  } ?>
                <tr>
                  <td><label class="tip" >Upload Pdf:</label></td>
                  <td><input type="file" name="sample_fob_speech_file"  />
                    (Display on father of the bride professional speech page)</td>
                </tr>
                <?php if($_REQUEST['setting_id']!="") {?>
                <input type="hidden" name="sample_fob_speech_file_1" value="<?php echo $newsDetails['sample_fob_speech_file'];?>"  />
                <tr>
                  <td><label class="tip" > Pdf:</label></td>
                  <td><a href="<?php echo SITE_PDF.$newsDetails['sample_fob_speech_file'];?>" target="_blank">View Pdf</a></td>
                </tr>
                <?php  } ?>
                <tr>
                  <td width="150"><label>Status</label></td>
                  <td><input type="checkbox" name="status" value="1" style="border:none" <?php if($newsDetails['status']=="1"){?> checked<?php }?>>
                    (Checked=Active; Uncheck=Inactive);</td>
                </tr>
                <tr>
                  <td colspan="2" class="tablesRowHeadingBG"></td>
                </tr>
              </table>
              <div class="clear"> </div>
            </div>
            <div align="center">
              <input name="submit" type="submit" class="button" title="Save" value="Update" />
            </div>
          </form>
        </div>
        <!-- End Quick Index --> 
        
      </div>
      <!-- End Grid --> 
      
    </div>
    <!-- End Page Content  -->
    <div class="clear"> </div>
  </div>
</div>
<div class="footer"> </div>
</body>
</html>
