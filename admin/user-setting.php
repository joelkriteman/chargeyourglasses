<?php
require_once("includes/application-top.php");

$objAdmin = new Admins();
$objAdmin->fun_authenticate_admin();

$dbObj = new DB();
$dbObj->fun_db_connect();

$userDetails = $objAdmin->fun_getAdminUserInfo($_SESSION['session_admin_userid']);

if($_POST['submit'] == "Submit" || $_POST['submit'] == "Update"){
	if($_POST['securityKey']==md5("ADDAREA")){ 
	$_SESSION['session_admin_password'] = md5($_REQUEST['au_password']);
		$affectedRows = $objAdmin->editAdminUserDetail($_SESSION['session_admin_userid']);
		if($affectedRows < 0){
			$msg = "Unable to edit user setting! Please try again.";
			$msgType = "1";
		}else{
		
			$msg = "User settings have been updated successfully!";
			
			$msgType = "2";
		}
	}
	redirectURL("user-setting.php?msg=".$msg."");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>WEB ADMIN SECTION</title>
<link type="text/css" rel="stylesheet" media="all" href="css/base.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/jquery-ui.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/grid.css" />
<link type="text/css" rel="stylesheet" media="all" href="css/visualize.css" />
</head>

<body id="actcategory">
<div id="header">
  <div class="header-top tr">
    <p>logged in as <?php echo $_SESSION['session_admin_username'];?>, <?php echo date("D j M Y");?></p>
  </div>
  <div class="header-middle"> 
    
    <!-- Start Top Nav -->
    <?php include_once("includes/top_nav.php");?>
    <!-- End Top Nav -->
    
    <div class="clear"> </div>
  </div>
</div>
<div id="page-wrapper">
  <div class="page"> 
    
    <!-- Start Sidebar -->
    <?php include_once("includes/dashboard-header.php");?>
    <!-- End Sidebar --> 
    
    <!-- Star Page Content  -->
    <div id="page-content"> 
      <!-- Start Page Header -->
      <div id="page-header">
        <h1>Edit User Details</h1>
      </div>
      <!-- End Page Header -->
      <?php if($_REQUEST['msg']!=""){?>
      <div class="notification success"> <span class="strong">SUCCESS!</span> <?php echo $_REQUEST['msg'];?> </div>
      <?php }?>
      
      <!-- Start Grid -->
      <div class="container_12"> 
        
        <!-- Start Quick Index -->
        <div class="grid_12"> 
          
          <!-- End Quick Index --> 
          
          <!-- Start Open Enquiries -->
          
          <div class="box-header">Edit User Details</div>
          <form action="" method="post" enctype="multipart/form-data" name="form1" onSubmit="return validateForm();">
            <input type="hidden" name="securityKey" value="<?php echo md5("ADDAREA");?>" />
            <input type="hidden" name="au_id" value="<?php echo $_SESSION['session_admin_userid']?>" />
            <div class="box table">
              <table width="100%"  border="0" cellspacing="0" cellpadding="0" class="module-d tbllist" style="border: solid 1px #cccccc;">
                <tr>
                  <td width="150"><label>Username*</label></td>
                  <td ><input name="au_username" type="text" class="textbox_long" id="au_username" value="<?php echo $userDetails['au_username']; ?>" readonly></td>
                </tr>
                <tr >
                  <td ><label>First Name</label></td>
                  <td ><input name="au_first_name" type="text" class="textbox_long" id="au_first_name" value="<?php echo $userDetails['au_first_name']; ?>"></td>
                </tr>
                <tr>
                  <td><label>Last Name</label></td>
                  <td><input name="au_last_name" type="text" class="textbox_long" id="au_last_name" value="<?php echo $userDetails['au_last_name']; ?>"></td>
                </tr>
                <tr >
                  <td ><label>Email*</label></td>
                  <td ><input name="au_email" type="text" class="textbox_long" id="au_email" value="<?php echo $userDetails['au_email']; ?>"></td>
                </tr>
                <tr>
                  <td ><label>Password*</label></td>
                  <td><input name="au_password" type="password" class="textbox_long" id="au_password" value="<?php echo $userDetails['au_password']; ?>"></td>
                </tr>
                <tr>
                  <td ><label>Phone Number</label></td>
                  <td ><input name="au_phone_number" type="text" class="textbox_long" id="au_phone_number" value="<?php echo $userDetails['au_phone_number']; ?>"></td>
                </tr>
              </table>
              <div class="clear"> </div>
            </div>
            <div align="center">
              <input name="button" type="reset" class="button small fl" value="Back" title="Back" onClick="javascript: window.location.href='dashboard.php';" />
              <input name="submit" type="submit" class="button" title="Save" value="Update" />
            </div>
          </form>
        </div>
        <!-- End Quick Index --> 
        
      </div>
      <!-- End Grid --> 
      
    </div>
    <!-- End Page Content  -->
    <div class="clear"> </div>
  </div>
</div>
<?php if($_SESSION['session_admin_usertype']!="Super Admin") { 
				$cssFootr = 'style="position:fixed; left:0; bottom:0; width:100%;"';
				}?>
<div class="footer" <?php echo $cssFootr;?>> Copyright &copy;<?php echo date('Y');?></div>
</body>
</html>
