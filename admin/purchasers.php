<?php
require_once("includes/application-top.php");
require_once("includes/classes/class.Orders.php");
require_once("includes/classes/class.Users.php");

$objAdmin = new Admins();
$objAdmin->fun_authenticate_admin();

$dbObj = new DB();
$dbObj->fun_db_connect();
$objOrders = new Orders();
$objUsers = new Users();

if(!empty($_GET['page'])){
	$page = $_GET['page'];
}else{
	$page = 1;
}

$limit = 30;
$start = ($page - 1) * $limit;

$searchStr = "";
$searchCon = "";
$searchWhere = "";

$spagetitle = fun_db_output($_REQUEST['spagetitle']);
$spstatus = fun_db_output($_REQUEST['spstatus']);

$searchStr = "&page=" . $page  . "&spagetitle=" . urlencode($spagetitle)."&spstatus=" . urlencode($spstatus);
$date=explode('/',$_REQUEST['from_date']);
$fromDate=$date[2]."-".$date[1]."-".$date[0];
$date1=explode('/',$_REQUEST['till_date']);
$fromDate1=$date1[2]."-".$date1[1]."-".$date1[0];


if($_REQUEST['person_id']!=""){
	$searchTxt .= $searchCon . " user_id = '".fun_db_input($_REQUEST['user_id'])."' ";
	$searchCon = " AND ";
}
if($spagetitle!=""){
	$searchTxt .= $searchCon . " user_fname LIKE '%".fun_db_input($spagetitle)."%' ";
	$searchCon = " AND ";
}
if(($_REQUEST['from_date']!="")&&($_REQUEST['from_date']!="dd/mm/yyyy"))
{
$searchTxt .= $searchCon . " added_date >= '".$fromDate."' ";
$searchCon = " AND ";	
}
if(($_REQUEST['till_date']!="")&&($_REQUEST['till_date']!="dd/mm/yyyy"))
{
$searchTxt .= $searchCon . " added_date <= '".$fromDate1."' ";
$searchCon = " AND ";	
}

if($spstatus!=""){
	$searchTxt .= $searchCon . "status='".fun_db_input($spstatus)."' ";
	$searchCon .= " AND ";
}
/*if($_SESSION['session_admin_usertype']!="Super Admin") { 
	$searchTxt .= $searchCon . "website_id='2' ";
	$searchCon .= " AND ";
}*/

if($_SESSION['session_admin_usertype']=="groomlist user") { 
	$searchTxt .= $searchCon . "website_id='2' ";
	$searchCon .= " AND ";
}

if($_SESSION['session_admin_usertype']=="Confetti") { 
	$searchTxt .= $searchCon . "website_id='3' ";
	$searchCon .= " AND ";
}

if($_REQUEST['website_id']!="")
{
	$searchTxt .= $searchCon . "website_id='".$_REQUEST['website_id']."' ";
	$searchCon = " AND ";

}
if($_REQUEST['user_type']!="")
{
	if($_REQUEST['user_type']==3)
	{
	$searchTxt .= $searchCon . "total_orders='0' ";
	$searchCon = " AND ";
	}
	else if($_REQUEST['user_type']==2)
	{
	$searchTxt .= $searchCon . "total_orders > 0 AND expired_on < '".date('Y-m-d')."'";
	$searchCon = " AND ";
	}
	else if($_REQUEST['user_type']==1)
	{
	$searchTxt .= $searchCon . "total_orders > 0 AND expired_on >= '".date('Y-m-d')."' AND status =1";
	$searchCon = " AND ";
	}

}
if($searchTxt!=""){
	$searchWhere = " WHERE " . $searchTxt;
}
	
 $sqlSelCate = "SELECT * FROM " . TABLE_USERS ;
  
if($searchWhere!="")
{
$sqlSelCate .= $searchWhere;
}
$cateResult = $dbObj->fun_db_query($sqlSelCate);
$totRecords = $dbObj->fun_db_get_num_rows($cateResult);
if($totRecords>$limit)
{
 $pagelinks = paginate($limit, $totRecords);
 }
$dbObj->fun_db_free_resultset($cateResult);
$sqlSelCate .= " ORDER BY user_id DESC LIMIT $start, $limit";

$cateResult = $dbObj->fun_db_query($sqlSelCate);
$Total = $dbObj->fun_db_get_num_rows($cateResult);


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>WEB ADMIN SECTION</title>
	<link type="text/css" rel="stylesheet" media="all" href="css/base.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/jquery-ui.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/grid.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/visualize.css" />
	<script src="jscript/jquery-1.5.1.js" type="text/javascript"></script>
	<script src="jscript/jquery.min.js" type="text/javascript"></script>
	<script src="jscript/jquery-ui.min.js" type="text/javascript"></script>
	<script type="text/javascript">
	$(function() {
		$( "#datepicker" ).datepicker(
		{
		dateFormat: 'dd/mm/yy'
		}
		);
		$( "#datepicker1" ).datepicker(
		{
		dateFormat: 'dd/mm/yy'
		}
		);
	});
	</script>
	</head>

	<body id="actcategory">
<div id="header">
      <div class="header-top tr">
    <p>logged in as <?php echo $_SESSION['session_admin_username'];?>, <?php echo date("D j M Y");?></p>
  </div>
      <div class="header-middle"> 
    
    <!-- Start Top Nav -->
    <?php include_once("includes/top_nav.php");?>
    <!-- End Top Nav -->
    
    <div class="clear"> </div>
  </div>
    </div>
<div id="page-wrapper">
      <div class="page"> 
    
    <!-- Start Sidebar -->
    <?php include_once("includes/dashboard-header.php");?>
    <!-- End Sidebar --> 
    
    <!-- Star Page Content  -->
    <div id="page-content"> 
          <!-- Start Page Header -->
          <div id="page-header">
        <h1>List of Users</h1>
      </div>
          <!-- End Page Header -->
          <?php if($_REQUEST['msg']!=""){?>
          <div class="notification success"> <span class="strong">SUCCESS!</span> <?php echo $_REQUEST['msg'];?> </div>
          <?php }?>
          
          <!-- Start Grid -->
          <div class="container_12"> 
        
        <!-- Start Quick Index -->
        <div class="grid_12">
              <div class="box-header">Refine list by</div>
              <div class="box table">
            <form name="searchFrm" action="" method="get">
                  <input type="hidden" name="user_id" value="<?php echo $_REQUEST['user_id']; ?>" />
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="td-middle">
                <thead>
                      <tr>
                    <td><label>From:</label></td>
                    <td><input id="datepicker" name="from_date" type="text" value="<?php if($_REQUEST['from_date']!=""){ echo $_REQUEST['from_date'];} else {?>dd/mm/yyyy<?php }?>" /></td>
                    <td ><label>To:</label></td>
                    <td><input id="datepicker1" name="till_date" type="text" value="<?php if($_REQUEST['till_date']!=""){ echo $_REQUEST['till_date'];} else {?>dd/mm/yyyy<?php }?>"  /></td>
                    <?php if($_SESSION['session_admin_usertype']=="Super Admin") { ?>
                    <td>User type:</td>
                    <td><select name="user_type">
                        <option value="">Please select user type</option>
                        <option value="1" <?php if($_REQUEST['user_type']==1) { ?> selected="selected" <?php } ?>>Active User</option>
                        <option value="2" <?php if($_REQUEST['user_type']==2) { ?> selected="selected" <?php } ?>>Expired subscription user</option>
                        <option value="3" <?php if($_REQUEST['user_type']==3) { ?> selected="selected" <?php } ?>>Only registered user</option>
                      </select></td>
                    <?php  } else echo "<td colspan=\"2\">&nbsp;</td>";?>
                  </tr>
                      <tr>
                    <td><label>Customer First name:</label></td>
                    <td><input name="spagetitle" type="text" value="<?php echo $spagetitle; ?>"></td>
                    <?php if($_SESSION['session_admin_usertype']=="Super Admin") { ?>
                    <td>Website:</td>
                    <td><select name="website_id">
                        <option value="">Please select website</option>
                        <option value="1" <?php if($_REQUEST['website_id']==1) { ?> selected="selected" <?php } ?>>General</option>
                        <option value="2" <?php if($_REQUEST['website_id']==2) { ?> selected="selected" <?php } ?>>Groomlist</option>
                        <option value="3" <?php if($_REQUEST['website_id']==3) { ?> selected="selected" <?php } ?>>Confetti</option>
                      </select></td>
                    <?php  } else echo "<td colspan=\"2\">&nbsp;</td>";?>
                    <td ><input type="submit" value="Update" class="button small" name="searchBttn" alt="Preview" title="Preview"/></td>
                    <td>&nbsp;</td>
                  </tr>
                    </thead>
              </table>
                </form>
          </div>
              
              <!-- End Quick Index --> 
              
              <!-- Start Open Enquiries -->
              
              <div class="box-header">
            <table>
                  <tr>
                <td><b>List of Users</b></td>
              </tr>
                </table>
          </div>
              <div class="box table">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <thead>
                <tr class="bgcolor">
                      <td width="210">Customer Name</td>
                      <td width="187">Email Address</td>
                      <td>Wedding Date</td>
                      <td width="87">Total Orders</td>
                      <td width="86">Last Login</td>
                      <td width="134">View Orders</td>
                    </tr>
              </thead>
                  <tbody>
                <?php
									if($totRecords>0)
		{
			$cnt = 0;
	if(($_GET['page'])>1)
	{
	 $page=$_GET['page'];
	  $i=(($page-1)*10)+1;
	}
	else
	{
	$i=1;										
	}										
		
	while($rowsCate = $dbObj->fun_db_fetch_rs_object($cateResult)){
		$cnt++;
		if($cnt % 2 == 0){
			$alternateStyle="tablesRowBG_1";
		}else{
			$alternateStyle="tablesRowBG_2";
		}
	?>
                <tr >
                      <td><?php echo fun_db_output($rowsCate->user_fname)." ".$rowsCate->user_lname;?></td>
                      <td><?php echo fun_db_output($rowsCate->user_email);?></td>
                      <td><?php if($rowsCate->user_wed_date!="0000-00-00") {echo fun_site_date_format(fun_db_output($rowsCate->user_wed_date)); } else echo "N/A";?></td>
                      <td align="center"><?php echo fun_db_output($rowsCate->total_orders);?></td>
                      <td><?php echo fun_site_date_format(fun_db_output($rowsCate->last_login));?></td>
                      <td><a href="orders.php?user_id=<?php echo fun_db_output($rowsCate->user_id);?>">View orders</a></td>
                    </tr>
                <?php $i=$i+1;} }
    else
  {
	echo "<tr><td><td colspan=\"6\"><font color=\"#FF0000\">No Results Found.</font></td></tr>";  
  }
  ?>
              </tbody>
                  <thead>
                <tr class="bgcolor">
                      <td colspan="2" class="tl">Total Records: <?php echo $totRecords;?></td><td colspan="4"><?php echo $pagelinks;?></td>
                    </tr>
              </thead>
                </table>
            <div class="clear"> </div>
          </div>
            </div>
        <div class="clear"> </div>
      </div>
          <!-- End Open Enquiries -->
          <div class="clear"> </div>
        </div>
    <!-- End Grid -->
    <div class="clear"> </div>
  </div>
      <!-- End Page Content  -->
      <div class="clear"> </div>
    </div>
<div class="clear"> </div>
<?php if($_SESSION['session_admin_usertype']!="Super Admin") { 
				$cssFootr = 'style="position:fixed; left:0; bottom:0; width:100%;"';
				}?>
<div class="footer" <?php //echo $cssFootr;?>></div>
</body>
</html>
