<?php include_once("includes/header.php");
$pageId=20;
$sql="SELECT * FROM ".TABLE_PAGES." WHERE status='1' AND pages_id='".$pageId."'";
$result=mysql_query($sql);
$rowsResult=mysql_fetch_array($result);

$sqlFeedback="SELECT * FROM ".TABLE_FEEDBACKS." WHERE status='1' AND home_page_status='1' ORDER BY rand()";
$resultFeedback=mysql_query($sqlFeedback);
?>
  <section id="breadcrumbs" class="alpha-ver15">
    <div class="col_1280">
      <ul>
        <li><a href="<?php echo SITE_URL;?>">Home</a></li>
        <li>&gt;</li>
        <li><?php echo $rowsResult['pages_title'];?></li>
      </ul>
    </div>
  </section>
  <section class="content-outer alpha-ver15">
   <div class="col_1280">
    <aside id="msg-content" class="alignleft">
   <?php include_once("includes/subscribe-msg.php");?>
      </aside>
   </div>
    <div class="col_1280">
      <aside id="main-content" class="alignleft">

        <div class="sub-heading">
          <h1><?php echo $rowsResult['pages_title'];?></h1>
        </div>
        <div class="dashed-border"></div>
        <?php echo $rowsResult['pages_content'];?>
        <div class="contact-form omega10">
          <form method="post" name="ContactFrm" onsubmit="return validateContactFrm();" action="">
            <input class="ZipCodeStyle" type="text" value="" name="ZipCode">
            <input type="hidden" name="security_key" value="<?php echo md5("CONTACTUS");?>"  />
            <fieldset>
              
              <!--<aside class="omega10">
            <label>Intials</label>
            <div class="contact-input">
             <select class="small-select">
             <option>Mr.</option>
             <option>Mrs.</option>
             </select>
            </div>
          </aside>-->
              <aside class="omega10">
                <label>Name:<span class="red">*</span></label>
                <div class="contact-input">
                  <input type="text" name="name">
                </div>
              </aside>
              <aside class="omega10">
                <label>Email:<span class="red">*</span></label>
                <div class="contact-input">
                  <input type="text" name="email">
                </div>
              </aside>
              <aside class="omega10">
                <label>Phone:</label>
                <div class="contact-input">
                  <input type="text" name="phone_number">
                </div>
              </aside>
              <aside class="omega10">
                <label>Speech for:</label>
                <div class="contact-select">
                  <select name="speech_for">
                    <option value="1">Best Man</option>
                    <option value="2">Father of the Bride</option>
                    <option value="3">Groom</option>
                    
                  </select>
                </div>
              </aside>
               <aside class="omega10">
          <label>Wedding Date:</label>
            <div class="contact-input">
          <!-- <input type="text" name="user_wed_date" id="date" />-->
          <select class="date" name="wedding_day">
            <?php fun_create_number_options(1,31,date('d'));?>
          </select>
          <select class="month" name="wedding_month">
            <?php fun_created_month_option(date('m'));?>
          </select>
          <select class="year" name="wedding_year">
            <?php fun_created_year_option();?>
          </select></div>
        </aside>
              <aside class="omega10">
                <label>Your Message:<span class="red">*</span></label>
                <div class="contact-textarea">
                  <textarea cols="" rows="5" name="message"></textarea>
                </div>
              </aside>
              <aside class="omega10">
                <div class="contact-submit">
                  <input type="submit" value="Send Enquiry">
                </div>
              </aside>
              <p class="omega10">* Data protection is important to us, please read our <a class="blue" href="<?php echo SITE_URL;?>privacy.php">privacy policy</a>.</p>
            </fieldset>
          </form>
        </div>
      </aside>
      <aside id="contact-sidebar" class="alignright">
        <div class="sub-heading">Locate us</div>
        <div class="add-bg omega20">
         <p><img src="assets/images/contact-wing-img.png" class="alignright" /><strong> Charge Your Glasses</strong><br />
Aztec House<br />
397-405 Archway Road<br />
Highgate<br />
London N6 4ER
</p>
        </div>
        
      </aside>
    </div>
  </section>
  <?php include_once("includes/footer.php");?>
