<?php include_once("includes/header.php");
$pageId=8;
$sql="SELECT * FROM ".TABLE_PAGES." WHERE status='1' AND pages_id='".$pageId."'";
$result=mysql_query($sql);
$rowsResult=mysql_fetch_array($result);

$sqlFeedback="SELECT * FROM ".TABLE_FEEDBACKS." WHERE status='1' AND home_page_status='1' ORDER BY rand()";
$resultFeedback=mysql_query($sqlFeedback);
?>
<!--header end here-->

<section id="breadcrumbs" class="alpha-ver15">
  <div class="col_1280">
    <ul>
      <li><a href="#">Home</a></li>
      <li>&gt;</li>
      <li>Error</li>
    </ul>
  </div>
</section>
<section class="content-outer alpha-ver15">
  <div class="col_1280">
    <aside id="main-content" class="alignleft">
      <div class="sub-heading">
        <h1>Oops!</h1>
      </div>
      <div class="dashed-border"></div>
      <article class="aligncenter"> <img src="assets/images/404-img.jpg" alt="Error 404 " />
        <p class="omega-ver10"> We are sorry but the page you are looking for does not exist.
          You could return to the homepage or search using the search box below</p>
        <!--<form>
          <div class="query-404form">
            <input type="text" />
            <input type="submit" value="Search" />
          </div>
        </form>-->
      </article>
    </aside>
    <?php include_once("includes/sidebar.php");?>
  </div>
</section>
<?php include_once("includes/footer.php");?>
