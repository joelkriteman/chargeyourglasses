<?php
include_once("../includes/application-top-inner.php"); 

  // Remember to copy files from the SDK's src/ directory to a
  // directory in your application on the server, such as php-sdk/
  require_once('facebook.php');

  $config = array(
    'appId' => '1419289998350360',
    'secret' => 'c2226f09092c94f005a3a5bfe101b375',
    'allowSignedRequest' => false // optional but should be set to false for non-canvas apps
  );

  $facebook = new Facebook($config);
  $user_id = $facebook->getUser();
    if($user_id) {

      // We have a user ID, so probably a logged in user.
      // If not, we'll get an exception, which we handle below.
      try {

        $user_profile = $facebook->api('/me','GET');
        //echo "Name: " . $user_profile['name'];
		//echo "Email: " . $user_profile['email'];
		$_SESSION['session_username']=$user_profile['email'];
		$_SESSION['user_email']=$user_profile['email'];
		$_SESSION['user_pwd']=rand(1000,1000000);
		$_SESSION['user_fname']=$user_profile['first_name'];
		$_SESSION['website_id']=1;
		$_SESSION['user_lname']=$user_profile['last_name'];
		
		$emailExist=$customerobj->funIsCustomerEmailExists($_SESSION['session_username']);
		if($emailExist=='1')
		{
			$userDetails = $customerobj->funGetCustomerLoginInfoByEmail($_SESSION['session_username']);
		$_SESSION['user_id'] = $userDetails['user_id'];
		redirectURL(SITE_URL);
		}
		else
		{
		$userId=$customerobj->processFbUser();
		if($_SESSION['website_id']==1)
		{
			$amount = 	$generalInfoDetail['amount'];
		}
	    else
		{
			$amount = $generalInfoDetail['groomlist_amount'];
		}

		$order_id=$OrderObj-> processRenewOrder($userId,$_SESSION['website_id'],$amount);

		 $emailRegisterFile = SITE_EMAIL_TAMPLATE_WS . "customer-registration.html";
   	 	 $pwdContent = fun_getFileContent($emailRegisterFile);
 	     $pwdContent = str_replace("[%SITE_NAME%]", SITE_NAME, $pwdContent);
		 $pwdContent = str_replace("[%NAME%]", $_SESSION['user_fname']." ".$_SESSION['user_lname'], $pwdContent);
		 $pwdContent = str_replace("[%EMAIL%]",  $_SESSION['user_email'], $pwdContent);
		 $pwdContent = str_replace("[%DATE%]",  date("Y-m-d"), $pwdContent);
		 $pwdContent = str_replace("[%PASSWORD%]",$_SESSION['user_pwd'], $pwdContent);
		 $pwdContent = str_replace("[%WEDDATE%]", "N/A", $pwdContent);
         $pwdContent = str_replace("[%ROLE%]", "N/A", $pwdContent);
  		 $pwdContent = str_replace("[%SITE_LOGO%]", EMAIL_LOGO, $pwdContent);
		 
		 $to=$_SESSION['user_email'];
		 $subject = 'Your login details for '.SITE_NAME.'';
	 	 $from=SITE_SUPPORT_EMAIL_ID;
		 $fromContent="Charge Your Glasses";	
		 $mailSentStatus = fun_get_email($to , $subject, $pwdContent, $from,$fromContent);
		 
		 $_SESSION['user_id'] = $userId;

redirectURL(SITE_URL."checkout.php?order_id=".base64_encode($order_id))."&back_url=".urlencode($_SERVER['HTTP_REFERER']);
		}
      } catch(FacebookApiException $e) {
        // If the user is logged out, you can have a 
        // user ID even though the access token is invalid.
        // In this case, we'll get an exception, so we'll
        // just ask the user to login again here.
        $login_url = $facebook->getLoginUrl(); 
		header("Location: ".$login_url);
        error_log($e->getType());
        error_log($e->getMessage());
      }   
    } else {
      // No user, print a link for the user to login
     $login_url = $facebook->getLoginUrl();
	 header("Location: ".$login_url);
    }

  ?>
