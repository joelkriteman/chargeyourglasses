<?php 
require_once("includes/application-top.php");
if($_REQUEST['security_key']==md5("FORGOTPWD"))
{
		 $EmailExist= $customerobj->funIsCustomerEmailExists($_REQUEST['user_email']);
	 	 if($EmailExist==1)
	 		{
				 $pwd=rand(1000,100000);
				 $customerobj->editChangePwd($_REQUEST['user_email'],$pwd);
				 $emailRegisterFile = SITE_EMAIL_TAMPLATE_WS . "forgot_pwd.html";
				 $pwdContent = fun_getFileContent($emailRegisterFile);
				 $pwdContent = str_replace("[%SITE_NAME%]", SITE_NAME, $pwdContent);
				 $pwdContent = str_replace("[%EMAIL%]",  $_REQUEST['user_email'], $pwdContent);
				 $pwdContent = str_replace("[%PASSWORD%]",$pwd, $pwdContent);
				 $pwdContent = str_replace("[%LINK_LOGIN%]","<a href=\"".SITE_URL."signup.php\" target=\"_blank\  style=\"text-decoration:none; color: #4BBBED\";>Click here to Login</a>", $pwdContent);
				 $pwdContent = str_replace("[%DATE%]",  date("d/m/Y"), $pwdContent);

				 $pwdContent = str_replace("[%SITE_LOGO%]", EMAIL_LOGO, $pwdContent);
				 
				 $to=$_REQUEST['user_email'];
				 $subject = 'Your New Account Password for '.SITE_NAME.'';
				 $from=SITE_SUPPORT_EMAIL_ID;
				 $fromContent="Confetti";	
				 $mailSentStatus = fun_get_email($to , $subject, $pwdContent, $from,$fromContent);
				 redirectURL(SITE_URL."forgot-pwd-thanks.php");
				 ?>
<?php
	 		}
	 	else
	 		{
		  		?>
<script type="text/javascript">
					window.parent.location = '<?php echo SITE_URL;?>error.php';
					</script>
<?php
	 		}
}

?>
<!DOCTYPE HTML>
<html>
<head>
<meta name="robots" content="noindex, nofollow" />
<script type="text/javascript">
function isEmail(str){
	var at="@";
	var dot=".";
	var lat=str.indexOf(at);
	var ldot=str.indexOf(dot);
	var lstr=str.length;

	if(str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
		return false;
	}
	if(str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
		return false;
	}
	if(str.indexOf(" ")!=-1){
		return false;
	}
	if(str.indexOf(at,(lat+1))!=-1){
		return false;
	}
	if(str.indexOf(dot,(lat+2))==-1){
		return false;
	}
	if(str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
		return false;
	}
return true;
}


function validateForgotPwdFrm(){
var frm = document.ForgorPwdfrm;
var user_email = frm.user_email.value;
if(frm.user_email.value==""){
	frm.user_email.style.borderColor='#FF0000';
			frm.user_email.focus();
			return false;
		}
		if(frm.user_email.value!=""){
		if(isEmail(frm.user_email.value)==false){
			frm.user_email.style.borderColor='#FF0000';
			frm.user_email.focus();
			return false;
		}
		}
		if(frm.user_email.value!=""){
		var email=frm.user_email.value;
			var ajaxaction='EmailExistCheck';
	 		$.post('<?php echo SITE_URL;?>ajaxcon', {ajaxaction:ajaxaction,email:email}, function(data) {
				if(data=="")
				{
					window.location.href='forgot-password?security_key=<?php echo md5("FORGOTPWD");?>&user_email='+user_email;
					return true;
				}
	 		$('#validation1').html(data);
			return false;
			});
			return false;
}
}
</script>
<link href="<?php echo SITE_URL;?>assets/css/popup.css" rel="stylesheet" type="text/css">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-571089-4', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body lang="en">

<!--Content start Here-->
<article class="register-outer-568">
  <div class="register-bg aligncenter">
    <h1 class="forgot-heading">Please enter your Email Address:</h1>
  </div>
  <section class="register-form" style="width:99%;">
    <div class="alpha-all10">
      <div id="validation1"></div>
      <form method="post" name="ForgorPwdfrm" onSubmit="return validateForgotPwdFrm();" action="<?php echo SITE_URL;?>forgot-password">
        <input type="hidden" name="security_key" value="<?php echo md5("FORGOTPWD");?>"  /><br><br>
        <div class="omega5">
          <label class="width-20">Email Address:<span class="red">*</span></label>
          <input type="text" name="user_email" class="width-77"/>
        </div>
        <div class="omega5">
          <label class="width-20">&nbsp;</label>
        <input type="submit" value="Submit" />
         
        </div>
      </form>
    </div>
  </section>
</article>
<!--Content end Here-->

</body>
</html>