<?php include_once("includes/header.php");
$pageId=17;
$sql="SELECT * FROM ".TABLE_PAGES." WHERE status='1' AND pages_id='".$pageId."'";
$result=mysql_query($sql);
$rowsResult=mysql_fetch_array($result);
?>
<body>
<div id="content">
<div class="wedd-speech-head">
      <div class="head-font"></div><?php include "includes/user-login.php";?>
    </div>
<section class="content-outer alpha-ver15">
    <aside id="main-content" class="alignleft">
      <?php if($_SESSION['user_id']!="")
		{?>
      <div class="sub-heading"> 
        <!--        <h1>Thank you for registering with us.</h1>
-->
        <?php include_once("includes/subscribe-msg.php");?>
      </div>
      <div class="dashed-border"></div>
      <p class="omega-ver10">To view all Speech material - containing 2000 sample sentences and over 50 model speeches - you need to pay (&pound;<?php echo $generalInfoDetail['amount'];?>).</p>
      <div class="bullet-link-list">
        <ul>
          <li>You will be able to access the speeches as many times as you wish for <?php echo $generalInfoDetail['time_days'];?> days.</li>
          <li>You will also receive a payment confirmation email.</li>
          <li>Transactions are processed on the PayPal SECURE PAYMENT SERVER. All payment and personal details are encapsulated using PayPal's encrypted and digitally-signed protocol. PayPal is regularly audited by the banking authorities to ensure a 100% secure transaction environment.</li>
          <li>This is a one time only charge. There is NO repeat billing. We do NOT retain or have access to your payment details.</li>
        </ul>
      </div>
      <div class="thanks-outer-row">
        <ul class="width-50">
          <li class="thank-color-col">ITEM & DESCRIPTION </li>
          <li>Full access to all
            Speech material<br />
            (Access is valid for <?php echo $generalInfoDetail['time_days'];?> days)</li>
        </ul>
        <ul>
          <li class="thank-color-col">COST</li>
          <li>&pound;<?php echo $generalInfoDetail['amount'];?></li>
        </ul>
        <ul>
          <li class="thank-color-col"><img src="<?php echo SITE_URL;?>assets/images/pay-ico.jpg" /> </li>
          <li>
            <div class="omega10"><a class="payment-alert small-btn" href="#paymentpopup"><strong>Pay Now</strong><span class="sprite-img"></span></a></div>
          </li>
        </ul>
      </div>
      <div class="clear"></div>
      <p class="red subcribe-msg omega10">After paying with a credit/debit card, click on <img src="<?php echo SITE_URL;?>assets/images/subscribe-btn.png" />.</p>
      <p><img src="<?php echo SITE_URL;?>assets/images/visa-card-img.jpg" /></p>
      <p class="omega10">We accept all major debit and credit cards and payment in GBP, US DOLLARS, EURO and AUSTRALIAN DOLLARS. All payments are processed on the PayPal secure server and debited in pounds sterling. </p>
      <?php } else { ?>
      <div class="sub-heading">
        <h1>Sorry! page is not accessible. You should login or resgiter.</h1>
      </div>
      <?php } ?>
    </aside>
    <?php //include_once("includes/sidebar.php");?>
</section>
</div>

<!-- Payment Instruction Popup -->
<div style="display:none;">
  <div id="paymentpopup" class="popupwindow">
    <div class="register-bg aligncenter">
      <h3>Alert</h3>
    </div>
    <div class="alpha-all10 aligncenter">
      <p>After payment, click on <img src="<?php echo SITE_URL;?>assets/images/subscribe-btn.png" /></p>
      <p class="omega10">For reference find the screenshot below:</p>
      <p><img src="<?php echo SITE_URL;?>assets/images/thank-you-message.png" class="img-align" /></p>
      <p class="omega20"> <a class="proceed-btn" href="make-payment.php?back_url=<?php echo urlencode($_SERVER['HTTP_REFERER']);?>" target="_blank"><strong>OK! Proceed</strong></a> </p>
    </div>
  </div>
</div>
<!-- End Payment Instruction Popup -->
</body>
</html>
