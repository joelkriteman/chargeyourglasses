<?php include_once("includes/header.php");
?>

<body>
 <div id="content">
<div id="logo">
    <h1 class="center-align"><a href="http://www.confetti.co.uk" title="Confetti"><img src="<?php echo SITE_URL;?>/assets/images/confetti-logo.gif" alt="Confetti | Create the Perfect Wedding Speeches"></a></h1>
  </div>  
<div class="wedd-speech-head">
 <div class="head-font">PAYMENT FAILED</div> <?php //include "includes/user-login.php";?>
  </div>
<div class="dashed-border"></div>
<p><strong>OOPS - YOUR TRANSACTION DIDN'T GO THROUGH!</strong><br/>
    <br/>
    In case you continue to face problems, please call our customer support at +44 (0)20 8245 8999 .</p>
  <div style="clear:both;"></div>
 <div class="alignmodule">
 
    <div class="alignleft btn-register"><a href="<?php echo SITE_URL_MAIN;?>">Back to Home</a></div>
    <!--<div class="alignright"><a href="#" class="bluebtn">Try Again</a></div>
-->
    <div style="clear:both;"></div>
  </div>
</div>
</body>
</html>
