<?php 
include_once("includes/header.php");
$pageId=26;
$sql="SELECT * FROM ".TABLE_PAGES." WHERE status='1' AND pages_id='".$pageId."'";
$result=mysql_query($sql);
$rowsResult=mysql_fetch_array($result);


$sqlFeedback="SELECT * FROM ".TABLE_FEEDBACKS." WHERE status='1' AND home_page_status='1' ORDER BY rand()";
$resultFeedback=mysql_query($sqlFeedback);

?>
<body lang="en">
<div class="content mCustomScrollbar">
<div id="content">
  <div class="con-wedd-speech">
    <div class="wedd-speech-head">
      <div class="head-font">Everything you need to write your own <span class="head-txt-uper">WEDDING SPEECH</span></div>
      <?php include "includes/user-login.php";?>
    </div>
    <div class="wedd-speech-coll">
      <div class="wedd-speech-coll-1"><a href="<?php echo SITE_URL;?>father-of-the-bride-wedding-speeches">Speech for<br>
        FATHER of the BRIDE</a></div>
      <div class="wedd-speech-coll-2"><a href="<?php echo SITE_URL;?>groom-wedding-speeches">Speech for<BR>
        a Groom</a></div>
      <div class="wedd-speech-coll-3"><a href="<?php echo SITE_URL;?>best-man-wedding-speeches">Speech for<br>
        the Best Man</a></div>
    </div>
    <?php include_once("includes/subscribe-msg.php");?>
    <div class="clear"></div>
    <div class="wedd-speech-testi">
      <div class="wedd-speech-left">
        <div class="wedd-speech-left-head">Are YOU preparing a speech for YOURSELF?</div>
        <p>If so, just choose your speech, add the names of your wedding party and start selecting sections - you can swap them around before printing your personalised wedding speech!</p>
        <div class="wedd-speech-left-head">Do you want to forward this link to SOMEONE ELSE?</div>
        <p>If so, please send them an email with a link and message from you!  All their wedding speech worries should disappear within minutes.</p>
        <div class="btn-refer-friend"><a href="<?php echo SITE_URL;?>refer-friend" class="iframe-4">Refer a friend</a></div>
      </div>
      <div class="wedd-speech-right">
        <div class="head-main">Testimonials</div>
        <div class="testi-man-img"><img src="assets/images/testi-man-img.png" alt="testi-img-man"></div>
        <div class="testi-content">
          <div class="testi-img-left"><img src="assets/images/testi-img-left.jpg" alt="left-ico"></div>
          <div class="testi-img-right"><img src="assets/images/testi-img-right.jpg" alt="right-ico"></div>
          <?php 
			$rowsResultFeedback=mysql_fetch_array($resultFeedback);
			?>
          <div class="testi-txt"><strong><?php echo $rowsResultFeedback['client_name'];?></strong><br>
            <p><?php echo $rowsResultFeedback['feedback_text'];?></p>
            <br>
            <span class="co-green"><?php echo $rowsResultFeedback['location'];?> | <?php echo fun_site_date_format($rowsResultFeedback['date']);?></span></div>
        </div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
  <div class="con-wedd-speech-mid">
    <div class="speech-mid-left">
      <div class="speech-mid-head"> It will cost <span class="co-green">&pound;<?php echo $generalInfoDetail['amount'];?></span> to continue using it for the next <span class="co-green"><?php echo $generalInfoDetail['time_days'];?> days</span>.</div>
      <p><?php echo $rowsResult['home_midText'];?></p>
    </div>
    <?php
   if($_SESSION['user_id']!=""){
	   
    if(($userDetails['status']==0) || ($userDetails['expired_on']<date('Y-m-d'))) { ?>
    <div class="btn-register">
    	<a class="payment-alert" href="#paymentpopup" >Make payment</a>
	</div>
    <?php }}?>
    <?php
    if($_SESSION['user_id']=="")
	{
	?>
    <div class="btn-register"><a href="<?php echo SITE_URL;?>register" class="iframe-1">Register Now</a></div>
    <?php  }?>
  </div>
  <?php include_once("includes/footer-top.php");?>
</div>
</div>

<!-- Payment Instruction Popup -->
<div style="display:none;">
  <div id="paymentpopup" class="popupwindow">
    <div class="register-bg aligncenter">
      <h3>Alert</h3>
    </div>
    <div class="alpha-all10 aligncenter">
      <p>After payment, click on <img src="<?php echo SITE_URL;?>assets/images/subscribe-btn.png" /></p>
      <p class="omega10">For reference find the screenshot below:</p>
      <p><img src="<?php echo SITE_URL;?>assets/images/thank-you-message.png" class="img-align" /></p>
      <p class="omega20"> <a class="proceed-btn" href="make-payment.php?back_url=<?php echo urlencode($_SERVER['HTTP_REFERER']);?>" target="_blank"><strong>OK! Proceed</strong></a> </p>
    </div>
  </div>
</div>
<!-- End Payment Instruction Popup -->

</body>
</html>
