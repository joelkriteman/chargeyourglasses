<?php
class Setting{
	var $dbObj;
	
	function Setting(){ // class constructor
		$this->dbObj = new DB();
		$this->dbObj->fun_db_connect();
	}
	function funGetSettingInfo($cateID){
		$locArray = array();
		$sql = "SELECT * FROM " . TABLE_GENERAL_SETTING . " WHERE setting_id='".(int)$cateID."'";
		
		$result = $this->dbObj->fun_db_query($sql) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
		if(!$result || $this->dbObj->fun_db_get_num_rows($result) < 1){
			return; // user does not exists
		}
		$rowsCategory =  $this->dbObj->fun_db_fetch_rs_object($result);
		$locArray = array(
							"setting_id" => fun_db_output($rowsCategory->setting_id),
							"time_days" => fun_db_output($rowsCategory->time_days),
							"amount" => fun_db_output($rowsCategory->amount),
							"home_line_of_month_text" => fun_db_output($rowsCategory->home_line_of_month_text),
							"sample_speech_file" => fun_db_output($rowsCategory->sample_speech_file),
							"sample_groom_speech_file" => fun_db_output($rowsCategory->sample_groom_speech_file),
							"sample_bestman_speech_file" => fun_db_output($rowsCategory->sample_bestman_speech_file),
							"sample_fob_speech_file" => fun_db_output($rowsCategory->sample_fob_speech_file),
						    "groomlist_amount" => fun_db_output($rowsCategory->groomlist_amount),
							"status" => fun_db_output($rowsCategory->status),
							"last_modified" => fun_db_output($rowsCategory->last_modified),
							"added_date" => fun_db_output($rowsCategory->added_date)
						 );
		return $locArray;
	}
}
?>