<?php
class SitePages{
	var $dbObj;
	
	function SitePages(){ // class constructor
		$this->dbObj = new DB();
		$this->dbObj->fun_db_connect();
	}
	
	function funGetSitePagesInfo($pageID){
		$pagesArray = array();
		 $sql = "SELECT * FROM " . TABLE_SITE_PAGES . " WHERE pages_id='".(int)$pageID."'";
		
		$result = $this->dbObj->fun_db_query($sql) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On SitePages table.</font>");
		if(!$result || $this->dbObj->fun_db_get_num_rows($result) < 1){
			return; // user does not exists
		}
		$rowsSitePages =  $this->dbObj->fun_db_fetch_rs_object($result);
		$pagesArray = array(
							"pages_id" => fun_db_output($rowsSitePages->pages_id),
							"pages_title" => fun_db_output($rowsSitePages->pages_title),
							"pages_title_fr" => fun_db_output($rowsSitePages->pages_title_fr),
							"pages_content" => fun_db_output($rowsSitePages->pages_content),
							"parent_id" => fun_db_output($rowsSitePages->parent_id),
							"page_seo_meta_title" => fun_db_output($rowsSitePages->page_seo_meta_title),
							"page_seo_meta_keyword" => fun_db_output($rowsSitePages->page_seo_meta_keyword),
							"page_seo_meta_description" => fun_db_output($rowsSitePages->page_seo_meta_description),
							"status" => fun_db_output($rowsSitePages->status),
							"pages_last_modified" => fun_db_output($rowsSitePages->pages_last_modified),
							"pages_added_date" => fun_db_output($rowsSitePages->pages_added_date)
						 );
		return $pagesArray;
	}
	
	
	function funBreadcrumb($locID){
		if($locID){
			$last="false";
			$newLocID;
			$locName;
			$i=0;
			$k=0;
			$cmt=0;
			while($cmt<10){
				$sqlCate = "SELECT page_id, page_title, parent_id FROM ".TABLE_SITE_PAGES."  WHERE page_id= '" . (int)$locID . "'";
				
				$resultTmp = $this->dbObj->fun_db_query($sqlCate) or die("Error: Unable to execute request!<br>Invalid Query On SitePages table.");
				while($rowsCate = $this->dbObj->fun_db_fetch_rs_object($resultTmp)){
					$newLocID[$i] = "view-page.php?cPath=" . $rowsCate->page_id;
					$locName[$i]= fun_db_input($rowsCate->page_title);
					$locID = (int)fun_db_input($rowsCate->parent_id);
					$i=$i+1;
					
					if($locID==0){
						break;
					}
				}
				$cmt++;
			}
			echo "<a href=\"view-page.php\" class=\"bText_link\">Home</a><span class=\"bText_link\">&nbsp;&nbsp;&raquo;&nbsp;&nbsp;</span>";
			for($k=sizeof($locName) -1;$k>=0;$k--){
				if ($k!=0){
					echo("<a href=\"$newLocID[$k]\" class=\"bText_link\">$locName[$k]</a>");
					echo("<span class=\"bText_link\">&nbsp;&nbsp;&raquo;&nbsp;&nbsp;</span>");
				}else{
					if($last=="false"){	
						echo("<span class=\"bText_link\">$locName[$k]</span>");
					}else{
						echo("<a href=$newLocID[$k] class=\"bText_link\">$locName[$k]</a>");
					}
				}
			}
		}
	}
	
		
		
}
?>