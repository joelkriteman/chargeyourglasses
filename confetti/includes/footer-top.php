<div class="month-quiz-div">
    <div class="line-month">
      <div class="head-main">Line of the Month</div>
      <div class="testi-content">
        <div class="testi-img-left"><img src="assets/images/testi-img-left.jpg" alt="left-ico"></div>
        <div class="testi-img-right"><img src="assets/images/testi-img-right.jpg" alt="right-ico"></div>
        <div class="testi-txt">
          <?php echo $generalInfoDetail['home_line_of_month_text'];?>
          <span class="co-green t-right right">...a sample of our Best Man speech material</span></div>
      </div>
    </div>
    <div class="take-quiz">
      <div class="head-main">Take a Quiz</div>
      <ul>
        <li><a href="<?php echo SITE_URL;?>how-well-do-you-know-the-groom-quiz">How well do you know the Groom?</a></li>
        <li><a href="<?php echo SITE_URL;?>how-well-do-you-know-your-bride-quiz">How well do you know your fiancée?</a></li>
        <li><a href="<?php echo SITE_URL;?>is-he-good-enough-for-your-daughter-quiz">Is he good enough for your daughter?</a></li>
      </ul>
    </div>
  </div>
  
 