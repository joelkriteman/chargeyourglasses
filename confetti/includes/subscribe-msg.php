<?php 
   if($_SESSION['user_id']!="") {
   if(($userDetails['status']==0) || ($userDetails['expired_on']<date('Y-m-d'))) {
	   
	   if($_SERVER['HTTP_REFERER']==SITE_URL_MAIN){
		   $Target = "_blank";
		   }
	    ?>


<div class="msg-div omega-ver20">
  <p> <strong>Your subscription has expired. Please <a class="payment-alert" href="#confettiSubscribepaymentpopup">Click Here</a> to renew it.</strong></p>
</div>

<!-- Payment Instruction Popup -->
<div style="display:none;">
  <div id="confettiSubscribepaymentpopup" class="popupwindow">
    <div class="register-bg aligncenter">
      <h3>Alert</h3>
    </div>
    <div class="alpha-all10 aligncenter">
      <p>After payment, click on <img src="<?php echo SITE_URL;?>assets/images/subscribe-btn.png" /></p>
      <p class="omega10">For reference find the screenshot below:</p>
      <p><img src="<?php echo SITE_URL;?>assets/images/thank-you-message.png" class="img-align" /></p>
      <p class="omega20"> <a class="proceed-btn" href="make-payment.php?back_url=<?php echo urlencode($_SERVER['HTTP_REFERER']);?>" target="_blank"><strong>OK! Proceed</strong></a> </p>
    </div>
  </div>
</div>
<!-- End Payment Instruction Popup -->
<?php }}?>
