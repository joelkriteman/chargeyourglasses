<?php include_once("includes/header.php");
$pageId=20;
$sql="SELECT * FROM ".TABLE_PAGES." WHERE status='1' AND pages_id='".$pageId."'";
$result=mysql_query($sql);
$rowsResult=mysql_fetch_array($result);

?>
<body lang="en">
<div class="content mCustomScrollbar">
<!--Content start Here-->
<div id="content">
  <div class="con-wedd-speech">
    <div class="wedd-speech-head">
      <div class="head-font"><?php echo $rowsResult['pages_title'];?></div>
      <?php include "includes/user-login.php";?>
    </div>
    <div class="dashed-border"></div>
    <p><strong> <?php echo $rowsResult['pages_content'];?></strong></p>
    <div class="contact-form omega10">
      <form method="post" name="ContactFrm" onSubmit="return validateContactFrm();" action="">
            <input class="ZipCodeStyle" type="text" value="" name="ZipCode">
            <input type="hidden" name="security_key" value="<?php echo md5("CONTACTUS");?>"  />
            <fieldset>
          
          <!--<aside class="omega10">
            <label>Intials</label>
            <div class="contact-input">
             <select class="small-select">
             <option>Mr.</option>
             <option>Mrs.</option>
             </select>
            </div>
          </aside>-->
          <aside class="omega10">
            <label>Name:<span class="red">*</span></label>
            <div class="contact-input">
              <input type="text" name="name">
            </div>
          </aside>
          <div class="clear"></div>
          <aside class="omega10">
            <label>Email:<span class="red">*</span></label>
            <div class="contact-input">
              <input type="text" name="email">
            </div>
          </aside>
          <div class="clear"></div>
          <aside class="omega10">
            <label>Phone:</label>
            <div class="contact-input">
              <input type="text" name="phone_number">
            </div>
          </aside>
          <div class="clear"></div>
          <aside class="omega10">
            <label>Speech for:</label>
            <div class="contact-input">
              <select name="speech_for">
                <option value="1">Best Man</option>
                <option value="2">Father of the Bride</option>
                <option value="3">Groom</option>
              </select>
            </div>
          </aside>
          <div class="clear"></div>
          <aside class="omega10">
            <label>Wedding Date:</label>
            <div class="contact-input"> 
              <!-- <input type="text" name="user_wed_date" id="date" />-->
              <select name="wedding_day" class="date">
                <?php fun_create_number_options(1,31,date('d'));?>
              </select>
              <select name="wedding_month" class="month">
                 <?php fun_created_month_option(date('m'));?>
                <option selected="" value="12">December</option>
              </select>
              <select name="wedding_year" class="year">
                 <?php fun_created_year_option();?>
              </select>
            </div>
          </aside>
          <div class="clear"></div>
          <aside class="omega10">
            <label>Your Message:<span class="red">*</span></label>
            <div class="contact-textarea">
              <textarea name="message" rows="5" cols=""></textarea>
            </div>
          </aside>
          <div class="clear"></div>
          <aside class="omega10">
            <div class="contact-submit">
              <input type="submit" value="Send Enquiry">
            </div>
          </aside>
          <div class="clear"></div>
          <p class="omega10">* Data protection is important to us, please read our <a href="<?php echo SITE_URL;?>privacy" class="blue">privacy policy</a>.</p>
          <div class="clear"></div>
        </fieldset>
      </form>
    </div>
  </div>
  <div class="clear"></div>
  <?php include_once("includes/footer-top.php");?>
</div>
<!--Content end Here-->

</div>
</body>
</html>