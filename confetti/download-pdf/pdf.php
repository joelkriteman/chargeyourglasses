<?php
require_once("../includes/application-top-inner.php");
session_start();
//check if the session is set

require_once('config/lang/eng.php');
require_once('tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Wedding Speeches');
$pdf->SetTitle('Wedding Speeches');
$pdf->SetSubject('Wedding Speeches');
$pdf->SetKeywords('Wedding Speeches');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH,PDF_HEADER_STRING,PDF_HEADER_PHONE);


//$pdf->SetHeaderData(PDF_HEADER_PHONE);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage();

//$pdf->setJPEGQuality(75);


/* NOTE:
 * *********************************************************
 * You can load external XHTML using :
 *
 * $html = file_get_contents('/path/to/your/file.html');
 *
 * External CSS files will be automatically loaded.
 * Sometimes you need to fix the path of the external CSS.
 * *********************************************************
 */
// define some HTML content with style
$personId = $_REQUEST['person_id'];
$userId = $_SESSION['user_id'];

	  $sqlSpeech="SELECT * FROM speech JOIN user_speech WHERE user_speech.user_id='".$userId."' AND speech.person_id='".$personId."' AND user_speech.speech_id= speech.speech_id ORDER BY user_speech.position ASC,user_speech.user_speech_id DESC";
		$resultSpeech=mysql_query($sqlSpeech);
		$resultSpeechNumRows = mysql_num_rows($resultSpeech);
		
		if($resultSpeechNumRows>0)
		{
		?>
<?php 
		while($rowsResultSpeech=mysql_fetch_array($resultSpeech))
		{
			$speechText = updateSpeechPerson($rowsResultSpeech['speech_text']);
			$speechText = str_replace("<b>",'',$speechText);
			$speechText = str_replace("</b>",'',$speechText);
			//$strData .= $speechText;
			//$strData .= "\r\n\n";
// create some HTML content
$htmlcontent1 = '<table width="100%" border="0">
  <tr>
     <td align=right>'.$speechText.'</td>
  </tr>
  </table>';
 $pdf->writeHTML($htmlcontent1, true, 0, true, 0);
 }
		}
// output the HTML content



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document


//============================================================+
// END OF FILE                                                 
//============================================================+



// ---------------------------------------------------------




// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('your-speech.pdf', 'I');

//============================================================+
// END OF FILE                                                 
//============================================================+
?>
