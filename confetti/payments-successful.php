<?php
include_once("includes/header.php");
$orderId = base64_decode($_REQUEST['order_id']);
$orderDetails = $OrderObj->funGetOrderInfo($orderId);
$userDetails = $customerobj->funGetUserInfo($orderDetails['user_id']);
$_SESSION['session_username'] = $userDetails['user_email'];
$_SESSION['user_id'] = $orderDetails['user_id'];
$orderNumber = $orderDetails['order_number'];

if(($orderDetails['mail_sent']!='yes') && ($orderNumber!=''))
{

		 $emailRegisterFile = SITE_EMAIL_TAMPLATE_WS . "payment-confirmation.html";
   	 	 $pwdContent = fun_getFileContent($emailRegisterFile);
 	     $pwdContent = str_replace("[%SITE_NAME%]", SITE_NAME, $pwdContent);
		 $pwdContent = str_replace("[%NAME%]", $userDetails['user_fname']." ".$userDetails['user_lname'], $pwdContent);
		 $pwdContent = str_replace("[%EMAIL%]",  $userDetails['user_email'], $pwdContent);
		 $pwdContent = str_replace("[%DATE%]",  date("d/m/Y"), $pwdContent);
         $pwdContent = str_replace("[%AMOUNT%]", $orderDetails['total_amount']." GBP", $pwdContent);
 		 $pwdContent = str_replace("[%ORDERID%]", $orderNumber, $pwdContent);
         $pwdContent = str_replace("[%PAYMENTSTATUS%]", "Completed", $pwdContent);
		 $pwdContent = str_replace("[%SITE_LOGO%]", EMAIL_LOGO, $pwdContent);
		 
		 $emailPaymentAdminFile = SITE_EMAIL_TAMPLATE_WS . "payment-confirmation-admin.html";
   	 	 $payAdminContent = fun_getFileContent($emailPaymentAdminFile);
 	     $payAdminContent = str_replace("[%SITE_NAME%]", SITE_NAME, $payAdminContent);
		 $payAdminContent = str_replace("[%NAME%]", $userDetails['user_fname']." ".$userDetails['user_lname'], $payAdminContent);
		 $payAdminContent = str_replace("[%EMAIL%]",  $userDetails['user_email'], $payAdminContent);
		 $payAdminContent = str_replace("[%AMOUNT%]",  $orderDetails['total_amount']." GBP", $payAdminContent);
		 $payAdminContent = str_replace("[%ORDERID%]",$orderNumber, $payAdminContent);
         $payAdminContent = str_replace("[%PAYMENTSTATUS%]", "Completed", $payAdminContent);
 		 $payAdminContent = str_replace("[%DATE%]",  date("d/m/Y"), $payAdminContent);
		 $payAdminContent = str_replace("[%SITE_LOGO%]", EMAIL_LOGO, $payAdminContent);
		 
		 $to=$userDetails['user_email'];
		 $to1=SITE_PAYMENT_EMAIL_ID;
		 $subject = 'Your payment details for '.SITE_NAME.'';
		 $subject1 = 'Confetti Payment details - '.$userDetails['user_email'];
	 	 $from=SITE_SUPPORT_EMAIL_ID;
		 $fromContent="Confetti";
		 $fromContent="Confetti";	
		 $mailSentStatus = fun_get_email($to , $subject, $pwdContent, $from,$fromContent);
		 $mailSentStatus1 = fun_get_email($to1 , $subject1, $payAdminContent, $from,$fromContent1);
		 $orderUpdate = $OrderObj->OrderMailStatusModify($orderId);
}
?>

<?php 
include_once "globalpath.php";
?>
<!DOCTYPE HTML>
<!--[if lt IE 9]><html class="ie"><![endif]-->
<!--[if gte IE 9]><!--><html><!--<![endif]-->


<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="canonical" href="<?php echo SITE_URL;?>/"/>
<title>Confetti</title>
<!-- main mobile Js -->

<style>

.header{background:url(../confetti-main/header.jpg) no-repeat center; width:100%; height:206px;}
.header img{width:100%;}
.footer{background:url(../confetti-main/footer.jpg) no-repeat center; width:100%; height:317px;}
.footer img{width:100%;}

</style>


</head>

<body lang="en">
<div id="wrapper"><!---->
<!--Header Start Here-->
<div class="header">
&nbsp;
</div>
<div class="clear"></div>
<!--Header End Here-->

<!--Content start Here-->
  <div id="content">
    <?php if($orderNumber!="") {?>
    <div class="wedd-speech-head">
      <div class="head-font">Thank you for your payment</div><?php include "includes/user-login.php";?>
    </div>
    <div class="order-outer alignleft col_70 omega-ver20">
      
      <p class="small-heading yellow-bg"><strong>Your payment details:</strong></p>
      <div class="order-inner">
        <ul>
          <li class="order-left">Transaction ID:</li>
          <li class="order-right"><?php echo $orderDetails['transactionid'];?></li>
        </ul>
        <ul>
          <li class="order-left">Order Number:</li>
          <li class="order-right"><?php echo $orderNumber;?></li>
        </ul>
        <ul>
          <li class="order-left">Name:</li>
          <li class="order-right"><?php echo $userDetails['user_fname']." ".$userDetails['user_lname'];?></li>
        </ul>
        <ul>
          <li class="order-left">Amount:</li>
          <li class="order-right">&pound; <?php echo $orderDetails['total_amount'];?></li>
        </ul>
        <ul>
          <li class="order-left">Date:</li>
          <li class="order-right"><?php echo date("d/m/Y");?></li>
        </ul>
        <ul>
          <li class="order-left">Payment status:</li>
          <li class="order-right">Completed</li>
        </ul>
      </div>
    </div>
    <aside class="alignright">
      <h2 class="omega-ver20" align="center">Now Choose Your Role</h2>
      <!--banner widget outer start here-->
      <section class="banner-widget-outer w-auto"> <a href="<?php echo SITE_URL;?>groom-wedding-speeches.php">
        <div class="banner-widget">
          <div class="ban-img">
            <div class="ban-bg"><img src="<?php echo SITE_URL;?>/assets/images/groom-img.jpg" alt="Are You a Groom?"></div>
          </div>
          <div class="ban-ico"><img src="<?php echo SITE_URL;?>/assets/images/groom_ico.jpg" alt="groom"></div>
          <p>Ready to start writing your <br>
            groom speech</p>
        </div>
        <p><span class="blue-btn">Click here</span></p>
        </a> </section>
      <!--banner widget outer end here--> 
      <!--banner widget outer start here--> 
      
      <!--banner widget outer end here--> 
      <!--banner widget outer start here--> 
      
      <!--banner widget outer end here--> 
    </aside>
    <!-- Google Code for ChargeYourGlasses Conversion Page --> 
    <script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1067416664;
var google_conversion_language = "en";
var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "B1clCOjJ3AoQ2Pj9_AM";
var google_remarketing_only = false;
/* ]]> */
</script> 
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
    <noscript>
    <div style="display:inline;"> <img height="1" width="1" style="border-style:none;" alt="" src="// www.googleadservices.com/pagead/conversion/1067416664/?label=B1clCOjJ3AoQ2Pj9_AM&amp;guid=ON&amp;script=0"/> </div>
    </noscript>
    <?php } else { ?>
    <div class="order-outer alignleft col_40 omega-ver20">
      <h1 class="omega-ver20">Sorry! page is not accessible.</h1>
    </div>
    <?php } ?>
  </div>
 <script>
		setTimeout(function () {    
    window.location.href = 'http://www.chargeyourglasses.com/confetti-main/'; 
},5000); // 5 seconds
		</script>
        <!--Content end Here-->
<!--Footer Start Here-->
<div class="footer">
&nbsp;
</div>
<!--Footer End Here-->
    
</div>

</body>
</html>