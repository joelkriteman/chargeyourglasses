<!DOCTYPE HTML>
<!--[if lt IE 9]><html class="ie"><![endif]-->
<!--[if gte IE 9]><!--><html><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="canonical" href="<?php echo SITE_URL;?>"/>
<title>Refer a friend</title>
<meta name="robots" content="noindex, nofollow" />

<script type="text/javascript" src="http://www.chargeyourglasses.com/confetti/assets/js/jquery-1.8.3.min.js"></script>


<script type="text/javascript" src="http://www.chargeyourglasses.com/confetti/assets/fancy-box/old/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="http://www.chargeyourglasses.com/confetti/assets/fancy-box/old/jquery.fancybox-new.css" media="screen" />
<script type="text/javascript" src="http://www.chargeyourglasses.com/confetti/assets/fancy-box/old/fancybox.js"></script>


<!-- fancybox code ends here -->
<!-- main jquery 1.8.3.min js -->


</head>
<body lang="en">

<!--Content start Here-->
<article class="register-outer-568">
  <div class="register-bg aligncenter">
    <h1><a href="http://www.chargeyourglasses.com/confetti/" class="iframe-Login">Refer a friend</a></h1>
   
  </div>
  
   <div id="validation1"></div>
      <form method="post" name="ReferFriendfrm" onSubmit="return validateReferFriendFrm();" action="">
          <input class="ZipCodeStyle" type="text" value="" name="ZipCode">
         <input type="hidden" name="security_key" value="<?php echo md5("REFERFRIEND");?>"  />
  
  <div style="width:98%; margin:auto;">
      <div class="alignleft register-form col_39 omega-ver10" style="border:none;">
        <div class="alpha-all5">
           <div class="omega5 aligncenter ref-fnd-right"> 
          <p> Send your friend a link to this site</p><br>
     
             <img src="<?php echo SITE_URL;?>assets/images/confetti-logo.gif" alt="confetti-logo">

            </div>
          
        
        </div>
      </div>
      <div class="alignright register-form col_60 omega-ver10">
        <div class="alpha-all5">
        <div class="omega5 aligncenter"> <strong>Your Message:</strong> </div>
          <div class="omega5">
          <label>Your Name:<span class="red">*</span></label>
          <input type="text" name="user_name" />
            
          </div>
          <div class="omega5">
            <label>Your E-mail:<span class="red">*</span></label>
          <input type="text" name="user_email" />
          </div>
          
          <div class="omega5 aligncenter"> <strong>Your Friends Details:</strong> </div>
          <div class="omega5">
            <label>Friend's Name:<span class="red">*</span></label>
          <input type="text" name="friend_name" />
          </div>
      <div class="omega5">
          <label>Friend's E-mail:<span class="red">*</span></label>
          <input type="text" name="friend_Email" />
        </div>
       
        <div class="omega5">
          <label>&nbsp;</label>
          <input type="submit" value="Submit" />
        </div>
        </div>
      </div>
    </div>
  </form>
  
  
</article>
<!--Content end Here-->

</div>
</body>
</html>