<?php
include_once("includes/header.php");
$pageId=23;
$personId = 3;
$userId = $_SESSION['user_id'];
$sql="SELECT * FROM ".TABLE_PAGES." WHERE status='1' AND pages_id='".$pageId."'";
$result=mysql_query($sql);
$rowsResult1=mysql_fetch_array($result);
?>
<script type="text/javascript">
$(document).ready(function(){
    $("#show3").click(function(){
	$("#show2").show();

	    });
    $("#content-show").click(function(){
	$("#content-show1").show();
	    });
});
</script>
<script type="text/javascript">
function chngeParaOption(val)
{
	<?php

	if($_SESSION['user_id']=="") { ?>
	checkCookie();
	<?php } ?>
	var ajaxaction='getSpeechParaOptionByPara';
	$.post('<?php echo SITE_URL;?>ajaxcon', {ajaxaction:ajaxaction,speech_para_id:val}, function(data) {
    $("#show").show();
	$("#show-tab").show();
	$('#show').html(data);
	$("#show2").hide();
	$('#show-tab').addClass('active');
	$('#show-tab2').removeClass('active2');

});
}
function chngeSpeech(val)
{
	<?php if($_SESSION['user_id']=="") { ?>
	checkCookie();
	<?php } ?>
	var ajaxaction='getSpeechByParaOption';
	$.post('<?php echo SITE_URL;?>ajaxcon', {ajaxaction:ajaxaction,speech_section_id:val}, function(data) {
    $("#show2").show();
	$('#show2').html(data);
	$('#show-tab2').addClass('active2');
	$('#paraOption'+val).addClass('active');
	});
	changeSpeechColor();

}
function addSpeech(val,val1,val2)
{
	var ajaxaction='addSpeech';
	$.post('<?php echo SITE_URL;?>ajaxcon', {ajaxaction:ajaxaction,speech_id:val,user_id:val1,person_id:val2}, function(data) {
    //alert(data);
	$('#content-show1').html(data);
	sortable();
});

}
function deleteSpeech(val,val1,val2,val3)
{
	var ajaxaction='deleteSpeech';
	$.post('<?php echo SITE_URL;?>ajaxcon', {ajaxaction:ajaxaction,user_speech_id:val,user_id:val1,person_id:val2,main_title:val3}, function(data) {
    //alert(data);
	$('#content-show1').html(data);
	sortable();
});
}
$(function() {
$('.step1 > ul > li').click(function(){
   $(this).addClass('active').siblings().removeClass('active');
});
});
function changeSpeechColor()
{
$('.step2 > ul > li').click(function(){
   $(this).addClass('active').siblings().removeClass('active');
});

}

</script>



<body lang="en">

<!--Content start Here-->
<div class="content mCustomScrollbar">
<div id="content">
  <div class="con-wedd-speech">
    <div class="wedd-speech-head">
      <div class="head-font">Make a brilliant 'Father of the Bride' speech</div>
      <?php include "includes/user-login.php";?>
    </div>
    <div class="how-work">
      <div class="how-work-head"> How it<br>
        works </div>
      <?php echo $rowsResult1['page_howit_works'];?> </div>
    <div class="btn-refer-friend right"><a href="<?php echo SITE_URL;?>refer-friend" class="iframe-4">Refer a friend</a></div>
    <div class="btn-Wedd-party"><a class="iframe-3" href="<?php echo SITE_URL;?>edit-party-members.php?person_id=<?php echo $personId;?>">Edit Wedding Party</a></div>
  </div>
  <div class="clear"></div>
   <?php include_once("includes/subscribe-msg.php");?>
  <article id="js-steps" class="step-outer">
    <aside class="step1 alignleft">
      <div class="step1bg">
        <div class="stepaligncenter"> <span class="numberic">1</span>
          <p class="alignright">speech <br>
            paragraph...</p>
        </div>
      </div>
      <ul class="step-links">
        <?php
		  	$sqlPara="SELECT * FROM ".TABLE_SPEECH_PARA." WHERE status='1' AND person_id='".$personId."' ORDER BY list_order ASC";
			$result=mysql_query($sqlPara);
			while($rowsResult=mysql_fetch_array($result))
			{
				if($_SESSION['user_id']!="") {
	  				if(($userDetails['status']==0) || ($userDetails['expired_on']<date('Y-m-d'))) {

					echo "<li><a href=\"".SITE_URL."confirm-payment.php \">".$rowsResult['speech_para_text']."</a></li>";

					}
					else
					{
				echo "<li><a href=\"javascript:chngeParaOption('".$rowsResult['speech_para_id']."');\">".$rowsResult['speech_para_text']."</a></li>";
					}
			}
			else
					{
				echo "<li><a href=\"javascript:chngeParaOption('".$rowsResult['speech_para_id']."');\">".$rowsResult['speech_para_text']."</a></li>";
					}
			}?>
      </ul>
    </aside>
    <aside class="step2 alignleft">
      <div class="step2bg-default" id="show-tab">
        <div class="stepaligncenter"> <span class="numberic">2</span>
          <p class="alignright">paragraph <br>
            options...</p>
          <span></span></div>
      </div>
      <ul class="step-links" id="show" style="display:none;">
      </ul>
    </aside>
    <aside class="step3 alignleft">
      <p id="show-tab2" class="step3bg-default"><img src="assets/images/step3-ico.png"><span></span></p>
      <div id="show2"> </div>
    </aside>
  </article>
  <div class="clear"></div>
  <div id="content-show1">
    <?php
	  $sqlSpeech="SELECT * FROM ".TABLE_SPEECH." JOIN ".TABLE_USER_SPEECH." WHERE ".TABLE_USER_SPEECH.".user_id='".$userId."' AND ".TABLE_SPEECH.".person_id='".$personId."' AND ".TABLE_USER_SPEECH.".speech_id= ".TABLE_SPEECH.".speech_id ORDER BY ".TABLE_USER_SPEECH.".position ASC,".TABLE_USER_SPEECH.".user_speech_id DESC";
		$resultSpeech=mysql_query($sqlSpeech);
		$resultSpeechNumRows = mysql_num_rows($resultSpeech);
		if($resultSpeechNumRows>0)
		{
		?>
    <p class="head-main">Your <?php echo $rowsResult1['pages_title'];?> Speech</p>

  <p class="omega10">  You can reorder the paragraphs by clicking on the relevant paragraph and dragging it to the required position.</p>
    <ul class="caption-blue">
      <?php
		while($rowsResultSpeech=mysql_fetch_array($resultSpeech))
		{
			$speechText = updateSpeechPerson($rowsResultSpeech['speech_text']);
			echo "<li id=\"recordsArray_".$rowsResultSpeech['user_speech_id']."\"><div class=\"speech-left-col\">".$speechText."</div><div class=\"speech-right-col\"><a class=\"delete_speech\" href=\"javascript:deleteSpeech('".$rowsResultSpeech['user_speech_id']."','".$rowsResultSpeech['user_id']."','".$rowsResultSpeech['person_id']."','".$rowsResult1['pages_title']."')\";>Remove</a></div></li>";
		}
		?>
      <div class="clear"></div>
    </ul>
    <!-- <p class="aligncenter omega-ver20"> <a href="<?php echo SITE_URL;?>download-speech.php?person_id=<?php echo $personId;?>" class="medium-btn">DOWNLOAD SPEECH<span class="sprite-img"><img src="<?php echo SITE_URL;?>/assets/images/doc-ico.png" alt="download speech"></span></a>
        <a href="<?php echo SITE_URL;?>download-speech-rtf.php?person_id=<?php echo $personId;?>" class="medium-btn">DOWNLOAD SPEECH<span class="sprite-img"><img src="<?php echo SITE_URL;?>/assets/images/txt-ico.png" alt="download speech"></span></a>
         </p>-->

    <div class="omega-ver20">
      <div class="aligncenter omega20"><a style="cursor:pointer" onClick="document.getElementById('lightbox').style.display='inline';"  class="small-btn">Download Speech</a><!--<a id="various3" href="#inline" class="small-btn">Download Speech</a>--> </div>
    </div>
    <div id="lightbox" class="lightbox" style="display:none"onclick="document.getElementById('lightbox').style.display='none';">
  <table class="lightbox_table">
    <tr>
      <td class="lightbox_table_cell" align="center"><div class="lightbox_content" style="width:400px; background-color:white; border:10px solid #6e9f9e;">
          <div class="close-icon"><img src="assets/fancy-box/images/closew.png" /></div>
     <div id="inline" class="col_400">     <div class="pop-link"><a href="<?php echo SITE_URL;?>download-pdf/pdf.php?person_id=<?php echo $personId;?>" target="_blank"><img src="assets/images/pdf-ico-pop.png" /><span>Save as PDF</span></a></div><div class="pop-link"><a href="<?php echo SITE_URL;?>download-speech.php?person_id=<?php echo $personId;?>"><img src="assets/images/doc-ico.png" /><span>Save as DOC</span></a></div><div class="pop-link"><a href="<?php echo SITE_URL;?>download-speech-rtf.php?person_id=<?php echo $personId;?>"><img src="assets/images/txt-ico.png" /><span>Save as TXT</span></a></div></div>
        </div></td>
    </tr>
  </table>
</div>
    <!--<div style="display: none;">
      <div class="col_400" id="inline">
        <div class="pop-link"><a href="<?php echo SITE_URL;?>download-pdf/pdf.php?person_id=<?php echo $personId;?>" target="_blank"><img src="assets/images/pdf-ico-pop.png" /><span>Save as PDF</span></a></div>
        <div class="pop-link"><a href="<?php echo SITE_URL;?>download-speech.php?person_id=<?php echo $personId;?>"><img src="assets/images/doc-ico.png" /><span>Save as DOC</span></a></div>
        <div class="pop-link"><a href="<?php echo SITE_URL;?>download-speech-rtf.php?person_id=<?php echo $personId;?>"><img src="assets/images/txt-ico.png" /><span>Save as TXT</span></a></div>
      </div>
    </div>-->
    <?php }?>
    <div class="clear"></div>
  </div>
  <div class="btn-box">
    <div class="aligncenter omega20">
      <?php if($_SESSION['user_id']!=""){?>
      <a class="small-btn iframe-speech" href="<?php echo SITE_URL;?>father-of-the-bride-professional-speeches.php">A professionally written father of the bride speech</a>
      <?php } ?>
      <a class="small-btn" href="<?php echo SITE_URL;?>is-he-good-enough-for-your-daughter-quiz.php">Is he good enough for your daughter? Take the quiz!</a> </div>
  </div>
  <div class="clear"></div>
  <?php include_once("includes/footer-top.php");?>
</div>
</div>
<!--Content end Here-->

</body>
</html>