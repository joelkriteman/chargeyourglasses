<?php 
require_once("includes/application-top.php");

?>
<!DOCTYPE HTML>
<!--[if lt IE 9]><html class="ie"><![endif]-->
<!--[if gte IE 9]><!--><html><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="canonical" href="<?php echo SITE_URL;?>"/>
<title>father-of-the-bride-wedding-speeches</title>
<meta name="robots" content="noindex, nofollow" />

<script type="text/javascript">
function validateLoginFrm(){
var frm = document.LoginForm;
var referrer = "<?php echo SITE_URL;?>";
if(frm.user_email.value==""){
	frm.user_email.style.borderColor='#FF0000';
			frm.user_email.focus();
			return false;
		}
		if(frm.user_email.value!=""){
		if(isEmail(frm.user_email.value)==false){
			frm.user_email.style.borderColor='#FF0000';
			frm.user_email.focus();
			return false;
		}
		}
		if(frm.user_pwd.value==""){
	frm.user_pwd.style.borderColor='#FF0000';
			frm.user_pwd.focus();
			return false;
		}
		if((frm.user_pwd.value!='')&&(frm.user_email.value!='')){
			
			var email=frm.user_email.value;
			var pwd=frm.user_pwd.value;
			var ajaxaction='SignUpCheck';
	 		$.post('<?php echo SITE_URL;?>ajaxcon', {ajaxaction:ajaxaction,email:email,password:pwd}, function(data) {
			if(data=="")
				{
					if(referrer== "http://www.chargeyourglasses.com/confetti/signup.php")
					{
						window.parent.location.href ="http://www.chargeyourglasses.com/confetti/welcome.php";
					}
					else
					{
					window.parent.location.href ="http://www.chargeyourglasses.com/confetti/welcome.php";
					}

				}
	 		$('#validation1').html(data);
			return false;
			});
			//alert("dasd");
			return false;
		}

}

function isEmail(str){
	var at="@";
	var dot=".";
	var lat=str.indexOf(at);
	var ldot=str.indexOf(dot);
	var lstr=str.length;

	if(str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
		return false;
	}
	if(str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
		return false;
	}
	if(str.indexOf(" ")!=-1){
		return false;
	}
	if(str.indexOf(at,(lat+1))!=-1){
		return false;
	}
	if(str.indexOf(dot,(lat+2))==-1){
		return false;
	}
	if(str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
		return false;
	}
return true;
}

</script>

<script>
$("a.example-forgot").fancybox({
    'width'    : 755,
    'height'   : 200,
    'scrolling'   : 'no',
	'overlayOpacity': 0.7,
	'overlayColor'  : '#eee',
	'autoScale'			: true,
	'transitionIn'		: 'none',
	'transitionOut'		: 'none',
    'href':               "<?php echo SITE_URL;?>forgot-password.php"
   
   });
</script>

<script type="text/javascript">
function signUpwithFb()
{
//var fbWindow = parent.open("fb/request_permissions.php",'FacebookLogin','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no');
fbWindow.focus();
}
</script>
<link href="<?php echo SITE_URL;?>assets/css/popup.css" rel="stylesheet" type="text/css">
</head>
<body lang="en">

<!--Content start Here-->
<div class="register-outer">
  <div class="register-bg aligncenter">
    <h3>Please enter your login details below</h3>
  </div>
  <div class="alignleft col_40 omega-ver20">
    <div class="aligncenter alpha-all10">
      <p> You can connect with your Facebook
        account to prepopulate some of the fields. </p>
      <p class="omega20"> <a href="fb/request_permissions.php" target="_blank"><img src="assets/images/login-facebook-btn.jpg" /></a></p>
    </div>
  </div>
  <div class="alignright register-form col_58 omega-ver20">
    <div class="alpha-all10">
      <div id="validation1"></div>
      <form method="post" name="LoginForm" onSubmit="return validateLoginFrm();" action="<?php echo SITE_URL;?>login">
      <!-- <input type="hidden" name="security_key" value="<?php echo md5("SIGNIN");?>"  />-->
        <div class="omega5">
          <label>Email Address:<span class="red">*</span></label>
          <input type="text" name="user_email"/>
        </div>
        <div class="omega5">
          <label>Password:<span class="red">*</span></label>
          <input type="password" name="user_pwd"/>
        </div>
        <div class="omega5">
          <label>&nbsp;</label>
          <div class="alignleft">
            <input type="submit" value="Login" />
          </div>
          <div class="alignright omega15"><a href="<?php echo SITE_URL;?>forgot-password" class="example-forgot blue" style="cursor:pointer;">Forgot Password?</a></div>
        </div>
      </form>
    </div>
  </div>
</div>
<!--Content end Here-->

</body>
</html>