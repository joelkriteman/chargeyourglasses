<?php 
require_once("includes/application-top.php");
if($_SESSION['user_id']!="")
		{

$return_url = SITE_URL.'payment-successful.php';
$userDetails = $customerobj->funGetUserInfo($_SESSION['user_id']);
if($userDetails['website_id']==1)
{
$amount = 	$generalInfoDetail['amount'];
}
else
{
	$amount = $generalInfoDetail['groomlist_amount'];
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<link href="assets/css/payment-checkout.css" rel="stylesheet" type="text/css" />
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-571089-4', 'auto');
  ga('send', 'pageview');

</script>

</head>
<body>
<div class="col-700">
  <div id="logo">
    <h1 class="center-align">Please wait while we transfer you to our payment gateway...</h1>
    <!--<img src="http://clientprojects.com.dedi1544.your-server.de/cyg/demo/assets/images/gif-load.jpg" alt="CHARGEYOURGLASSES | Create the Perfect Wedding Speeches">--> </div>
  <div class="center-align">
    <form method="post" action="https://www.paypal.com/cgi-bin/webscr">
      <input type="hidden" value="_xclick" name="cmd" />
      <input type="hidden" value="anna.quayle@greatspeechwriting.co.uk" name="business" />
      <input type="hidden" value="<?php echo $generalInfoDetail['time_days'];?> days Subscription to CYG" name="item_name" />
      <input type="hidden" value="<?php echo $amount;?>" name="amount" />
      <input type="hidden" value="No comments" name="cn" />
      <input type="hidden" value="GBP" name="currency_code" />
      <input type="hidden" value="<?php echo urlencode(stripslashes($return_url));?>" name="return" />
      <input type="hidden" name="cancel_return" value="<?php echo SITE_URL;?>cancel-payment.php" />
      <input type="hidden" name="back_url" value="<?php echo urlencode(stripslashes($return_url));?>" />
      <input type="hidden" name="notify_url" value="<?php echo urlencode(stripslashes($return_url));?>" />
      <input type="hidden" value="2" name="rm" />
      <input type="hidden" name="custom" value="<?php echo $_REQUEST['order_id'];?>" />
      <!--      <input type="image" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal – The safer, easier way to pay online." />
      <img alt="" border="0" src="https://www.paypalobjects.com/en_GB/i/scr/pixel.gif" width="1" height="1">
-->
    </form>
  </div>
  <script language="javascript">
    document.forms[0].submit();
</script> 
</div>
</body>
</html>
<?php } 
else
{?>
<html><head><meta name="robots" content="noindex nofollow" /></head><body><h1>Sorry! page is not accessible.</h1></body></html>	
<?php }?>