<?php
ini_set("session.gc_maxlifetime", 60*60*60*60*60);
@session_start();

require_once("../includes/database-table.php");
require_once("../includes/classes/class.DB.php");
require_once("../includes/functions/general.php");
require_once("../includes/classes/class.SitePages.php");
require_once("../includes/classes/class.Setting.php");
require_once("../includes/functions/anti-xss.php");
require_once("../includes/classes/class.Users.php");
require_once("../includes/classes/class.Contact.php");
require_once("../includes/classes/class.Quiz.php");
require_once("../includes/classes/class.Orders.php");

$dbObj = new DB();
$page = new SitePages(); 
$setting = new Setting(); 
$customerobj = new Users(); 
$contactObj = new Contact(); 
$QuizObj = new Quiz(); 
$OrderObj = new Orders(); 

ob_start();
require_once("../includes/common.php");

?>