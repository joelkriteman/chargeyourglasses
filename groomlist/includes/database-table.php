<?php
define("TABLE_ADMINS_USERS", "admin_users");
define("TABLE_ADMINS_TYPE", "admin_type");
define("TABLE_GENERAL_SETTING", "general_settings");
define("TABLE_NEWSLETTER", "newsletter");
define("TABLE_PERSON", "person");
define("TABLE_PAGES", "site_pages");
define("TABLE_USERS", "site_users");
define("TABLE_SPEECH", "speech");
define("TABLE_SPEECH_PARA", "speech_para");
define("TABLE_SPEECH_SECTION", "speech_section");
define("TABLE_QUIZ", "quiz_question");
define("TABLE_ORDERS", "site_orders");
define("TABLE_FEEDBACKS", "site_feedbacks");
define("TABLE_USER_SPEECH", "user_speech");
define("TABLE_USER_WED_PARTY", "site_user_wed_party");
define("TABLE_CONTACT", "site_contact");
define("TABLE_SPAM_CONTACT", "site_spam_contact");
define("TABLE_SUCCESSFUL_ORDERS", "site_successful_orders");

?>