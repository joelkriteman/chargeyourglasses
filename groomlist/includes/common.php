<?php
define("SITE_NAME", "Groomlist");
$generalInfoDetail=$setting->funGetSettingInfo(2);
if($_SERVER["SERVER_NAME"]=="localhost"){
	define("MAIN_URL", "http://localhost");
	define("SITE_URL", "http://localhost/akhil_projects/cyg/groom-list/");
	define("SITE_ADMIN_URL", "http://localhost/akhil_projects/cyg/groom-list/admin/");
	define("SITE_DOC_ROOT", "/XAMPP/htdocs/akhil_projects/cyg/groom-list/");
}
else{
	define("MAIN_URL", "http://www.chargeyourglasses.com/groomlist/");
	define("SITE_URL", "http://www.chargeyourglasses.com/groomlist/");
	define("SITE_ADMIN_URL", "http://www.chargeyourglasses.com/admin/");
	define("SITE_DOC_ROOT", $_SERVER['DOCUMENT_ROOT']."/groomlist/");
}

define("SITE_IMAGES", SITE_URL . "images/");
define("SITE_IMAGES_BANNER", SITE_IMAGES . "banner/");
define("SITE_EMAIL_TAMPLATE", SITE_URL . "email-template/");

define("SITE_IMAGES_WS", SITE_DOC_ROOT . "images/");
define("SITE_IMAGES_BANNER_WS", SITE_IMAGES_WS . "banner/");
define("SITE_EMAIL_TAMPLATE_WS", SITE_DOC_ROOT . "email-template/");
define("EMAIL_LOGO", SITE_EMAIL_TAMPLATE."CYG-GL-Logo.png");

define("EMAIL_ID_REG_EXP_PATTERN", "/^[A-Za-z0-9-_]+(\.[A-Za-z0-9-_]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$/");

define("SITE_INFO_EMAIL_ID", 'katie@greatspeechwriting.co.uk, enquiry@chargeyourglasses.com, nikhil@isglobalweb.com');
define("SITE_PAYMENT_EMAIL_ID", 'katie@greatspeechwriting.co.uk, enquiry@chargeyourglasses.com, nikhil@isglobalweb.com');
define("SITE_CONTACTUS_EMAIL_ID", 'katie@greatspeechwriting.co.uk, jack@greatspeechwriting.co.uk, nikhil@isglobalweb.com, akash@isglobalweb.com');


define("SITE_SUPPORT_EMAIL_ID", 'tom@chargeyourglasses.com');

define("SITE_COOKIE_EXPIRATION_DAY", 7);
define("NO_IMAGE_FILE", "nofound.gif");
?>
