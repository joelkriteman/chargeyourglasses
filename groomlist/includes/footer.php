<!--footer outer start here-->
<footer id="footer-outer" class="alpha-ver15">
  <div class="col_1280">
    <!--foot widget1 start here-->
    <section class="foot-widget-1 alignleft">
      <div class="footer-heading"> Tweets by @CHARGEYOURGLASSES</div>
      <div class="omega20"> <a class="twitter-timeline" height="180" href="https://twitter.com/ChargeUrGlasses" data-widget-id="466520818982191104"></a>
        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
      </div>
    </section>
    <!--foot widget1 end here-->
    <!--foot widget2 start here-->
    <section class="foot-widget-2 alignleft">
      <div class="footer-heading">Line of the Month</div>
      <div class="omega20">
        <div class="footer-para"> <span class="test-ico-left"><img src="<?php echo SITE_URL;?>assets/images/test-ico.png" alt="test ico"></span> <?php echo $generalInfoDetail['home_line_of_month_text'];?>
          <p class="small-heading blue" align="center">...a sample of our Groom speech material...</p>
          <span class="test-ico-right"><img src="<?php echo SITE_URL;?>assets/images/test-ico2.png" alt="test ico2"></span> </div>
      </div>
    </section>
    <!--foot widget2 end here-->
    <!--foot widget3 start here-->
    <section class="foot-widget-3 alignleft">
      <div class="footer-heading">Take a Quiz</div>
      <!--bullet points start here-->
      <div class="omega20">
        <ul class="bullet-links">
<!--          <li><a href="< ?php echo SITE_URL;?>how-well-do-you-know-the-groom-quiz.php" class="blue">How well do you know the Groom? </a></li>
-->          <li><a href="<?php echo SITE_URL;?>how-well-do-you-know-your-bride-quiz.php" class="blue">How well do you know your fianc&eacute;e?</a><br/><span>You'll find out just how good an idea this marriage is...</span></li>

<!--          <li><a href="< ?php echo SITE_URL;?>is-he-good-enough-for-your-daughter-quiz.php" class="blue">Is he good enough for your daughter?</a> </li>
-->        </ul>
      </div>
      <!--bullet point end here-->
      <!--subscribe form start here-->
      <div class="omega30">
        <p class="blue"><strong>Subscribe for more updates</strong></p>
        <aside class="subscribe-form">
          <form action="" method="post" name="subscribeForm" onsubmit="return validateNewsContactFrm();">
            <input type="hidden" name="security_key" value="<?php echo md5("NEWSCONTACTUS");?>"  />
            <div class="sub-input">
              <input type="text" name="email_address" placeholder="Enter Your Email">
            </div>
            <div class="sub-button">
              <input type="submit" value="Subscribe">
            </div>
          </form>
        </aside>
      </div>
      <!--subscribe form end here-->
      <!--social links start here-->
      <aside class="omega20">
        <div class="social-ico">
          <ul>
            <li><a href="https://www.facebook.com/chargeyourglasses" target="_blank"><img src="<?php echo SITE_URL;?>assets/images/fb-ico.png" alt="like this on facebook"></a></li>
            <li><a href="https://twitter.com/ChargeUrGlasses" target="_blank"><img src="<?php echo SITE_URL;?>assets/images/tweet-ico.png" alt="share this on twitter"></a></li>
            <li><a href="#"><img src="<?php echo SITE_URL;?>assets/images/google+-ico.png" alt="share this on google+"></a></li>
            <li><a href="#"><img src="<?php echo SITE_URL;?>assets/images/linkedin-ico.png" alt="share this on linkedin"></a></li>
          </ul>
        </div>
      </aside>
      <!--social links end here-->
    </section>
    <!--foot widget3 end here-->
  </div>
</footer>
<!--footer outer end here-->
<!--bottom outer start here-->
<section id="bottom-outer" class="alpha-ver15">
  <div class="col_1280">
    <div class="alignleft"><img src="<?php echo SITE_URL;?>assets/images/groom-footer-logo.png" alt="groom list"></div>
    <div class="alignright footer-links"> <a href="<?php echo SITE_URL;?>terms-condition.php">T&amp;Cs</a> - <a href="<?php echo SITE_URL;?>privacy.php">Privacy</a> -  © Copyright 2007 - 2014</div>
  </div>
</section>
<!--bottom outer end here-->

</div>
<!--Page end here-->
<div id='fixed-bar' style="right: 0px;">
  <div id='bar-inner'> <a class='go-top' href='#page-wrapper' title='back to top'><img src="<?php echo SITE_URL;?>assets/images/backtop.png" width="42" height="43"></a> </div>
</div>
<div id="show-background" class="hide"> <a href="#"></a> </div>

<div id="content-area" style="display:none;">
<div class="introjs-overlay" style="top: 0;bottom: 0; left: 0;right: 0;position: fixed;background-color: rgb(0,0,0);opacity: 0.8;"></div>
<!--  <div class="margincenter"><img src="<?php echo SITE_URL;?>assets/images/sprite.png"></div>-->
<div class="introjs-helperLayer layer-0 subscribe" style="width: 119px; height:31px; top:47px;left: 879px;"><div class="introjs-tooltip" style="background-image: url(http://assets-1.housingcdn.com/website/feature_intro/sprite-df96b74c18dc441f994fcee8f247c87f.png); bottom: -120px; background-position: -390px -235px; background-repeat: initial initial;"></div></div>
<div class="introjs-helperLayer layer-1 icon-resize-horizontal" style="width: 16px; height:16px; top:364.25px;right:250px;"><div class="introjs-tooltip" style="background-image: url(http://assets-1.housingcdn.com/website/feature_intro/sprite-df96b74c18dc441f994fcee8f247c87f.png); left: 0px; top: -120px; background-position: -600px -240px; background-repeat: initial initial;"></div></div>
<div class="introjs-helperLayer layer-2" style="width: 145px; height:31px; top:47px;left: 722px;"><div class="introjs-tooltip" style="background-image: url(http://assets-1.housingcdn.com/website/feature_intro/sprite-df96b74c18dc441f994fcee8f247c87f.png); top: 0px; right: 130px; background-position: -600px 0px; background-repeat: initial initial;"></div></div>
<div class="introjs-helperLayer layer-3 icon-move" style="width: 17px; height:11px; top:367.203125px;left: 821.25px;"><div class="introjs-tooltip" style="background-image: url(http://assets-1.housingcdn.com/website/feature_intro/sprite-df96b74c18dc441f994fcee8f247c87f.png); left: 0px; top: -120px; background-position: -610px -113px; background-repeat: initial initial;"></div></div>
</div>

<!-- Payment Instruction Popup -->
<div style="display:none;">
  <div id="paymentpopup" class="popupwindow">
    <div class="register-bg aligncenter">
      <h3>Alert</h3>
    </div>
    <div class="alpha-all10 aligncenter">
      <p>After payment, click on <img src="<?php echo SITE_URL;?>assets/images/subscribe-btn.png" /></p>
      <p class="omega10">For reference find the screenshot below:</p>
      <p><img src="<?php echo SITE_URL;?>assets/images/thank-you-message.png" class="img-align" /></p>
      <p class="omega20"> <a class="proceed-btn" href="make-payment.php?back_url=<?php echo urlencode($_SERVER['HTTP_REFERER']);?>"><strong>OK! Proceed</strong></a> </p>
    </div>
  </div>
</div>
<!-- End Payment Instruction Popup -->

<!-- Payment Instruction Popup -->
<div style="display:none;">
  <div id="backurlpaymentpopup" class="popupwindow">
    <div class="register-bg aligncenter">
      <h3>Alert</h3>
    </div>
    <div class="alpha-all10 aligncenter">
      <p>After payment, click on <img src="<?php echo SITE_URL;?>assets/images/subscribe-btn.png" /></p>
      <p class="omega10">For reference find the screenshot below:</p>
      <p><img src="<?php echo SITE_URL;?>assets/images/thank-you-message.png" class="img-align" /></p>
      <p class="omega20"> <a class="proceed-btn" href="make-payment.php?back_url=<?php echo $_REQUEST['back_url'];?>"><strong>OK! Proceed</strong></a> </p>
    </div>
  </div>
</div>
<!-- End Payment Instruction Popup -->

<script type="text/javascript">
$(function () {
  $("#fixed-bar")
    .css({position:'fixed',bottom:'0px'})
    .hide();
  $(window).scroll(function () {
    if ($(this).scrollTop() > 200) {
      $('#fixed-bar').fadeIn(200);
    } else {
      $('#fixed-bar').fadeOut(200);
    }
  });
  $('.go-top').click(function () {
    $('html,body').animate({
      scrollTop: 0
    }, 1000);
    return false;
  });
});
</script>
<script type="text/javascript">
$("#show-background").click(function () {
  if ($("#content-area").hasClass("bg_hidden")){	
     $("#content-area")
      .removeClass("bg_hidden")
      .stop()
      .fadeOut("fast");
	 $('html').removeClass("overlay");
     $("#show-background").addClass("hide");
     $("#show-background").removeClass("show");
  }
  else{
     $("#content-area")
      .addClass("bg_hidden")
      .stop()
      .fadeIn("fast");
     $('html').addClass("overlay");
     $("#show-background").removeClass("hide");
	 $("#show-background").addClass("show");
  }
});  
</script>
</body>
</html>