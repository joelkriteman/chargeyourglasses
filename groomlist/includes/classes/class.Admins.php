<?php
class Admins{
	var $dbObj;
	
	function Admins(){ // class constructor
		$this->dbObj = new DB();
		$this->dbObj->fun_db_connect();
	}
	
	function fun_check_username_admin_existance($username, $auID=''){ // this function checked checks wheather username exists or not
		$unameFound = false;
		$sqlCheck = "SELECT au_username FROM " . TABLE_ADMINS_USERS . " WHERE au_username='".fun_db_input($username)."' ";
		if($auID!=""){
			$sqlCheck .= " AND au_id<>'".(int)$auID."'";
		}
		if($this->fun_get_num_rows($sqlCheck) > 0){
			$unameFound = true;
		}
		return $unameFound;
	}
	
	function fun_check_pwd_admin_existance($pwd, $auID=''){ // this function checked checks wheather username exists or not
		$pwdFound = false;
		$sqlCheck = "SELECT au_password FROM " . TABLE_ADMINS_USERS . " WHERE au_password='".fun_db_input($pwd)."' ";
		if($auID!=""){
			$sqlCheck .= " AND au_id<>'".(int)$auID."'";
		}
		if($this->fun_get_num_rows($sqlCheck) > 0){
			$pwdFound = true;
		}
		return $pwdFound;
	}
	
		function resetPwd($email,$pwd)
	{
	 $sqlUpdate = "UPDATE " . TABLE_ADMINS_USERS . " SET au_password='".$pwd."' WHERE au_email='".$email."'";
				$this->dbObj->fun_db_query($sqlUpdate) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
				return $this->dbObj->fun_db_get_affected_rows();
				
}

	
	function fun_check_OPI_admin_existance($CustomerNo){ // this function checked checks wheather username exists or not
		$custNoFound = false;
		$sqlCheck = "SELECT au_operator_identifier FROM " . TABLE_ADMINS_TYPE . " WHERE au_operator_identifier='".fun_db_input($CustomerNo)."' ";
		if($this->fun_get_num_rows($sqlCheck) > 0){
			$custNoFound = true;
		}
		return $custNoFound;
	}
	function fun_check_Email_admin_existance($email){ // this function checked checks wheather username exists or not
		$custNoFound = false;
		 $sqlCheck = "SELECT au_email FROM " . TABLE_ADMINS_USERS . " WHERE au_email='".fun_db_input($email)."' ";
		
		if($this->fun_get_num_rows($sqlCheck) > 0){
			$custNoFound = true;
		}
		return $custNoFound;
	}
	function fun_check_emailid_admin_existance($emailid, $auID=''){ // this function checked checks wheather email id exists or not
		$emailIDFound = false;
		$sqlCheck = "SELECT au_email FROM " . TABLE_ADMINS_USERS . " WHERE au_email='".fun_db_input($emailid)."' ";
		if($auID!=""){
			$sqlCheck .= " AND au_id<>'".(int)$auID."'";
		}
		$result = $this->dbObj->fun_db_query($sqlCheck) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!</font>");
		if($this->fun_get_num_rows($result) > 0){
			$emailIDFound = true;
		}
		return $emailIDFound;
	}
	
	function fun_check_login_admins(){
		if(isset($_SESSION['session_admin_userid']) && isset($_SESSION['session_admin_username']) && isset($_SESSION['session_admin_password'])){
			if($this->fun_verify_admins($_SESSION['session_admin_username'], $_SESSION['session_admin_password'])){
				return true;
			}else{
				unset($_SESSION['session_admin_userid']);
				unset($_SESSION['session_admin_username']);
				unset($_SESSION['session_admin_password']);
				return false;
			}
		}else{
			return false;
		}
	}
	
	function fun_verify_admins($username, $password){
		 $sqlCheck = "SELECT au_username, au_password FROM " . TABLE_ADMINS_USERS . " WHERE au_username='".fun_db_input($username)."'";
		$result = $this->dbObj->fun_db_query($sqlCheck) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!</font>");
		if(!$result || $this->dbObj->fun_db_get_num_rows($result) < 1){
			return false; // ADMIN does not exists
		}
		
		$rowsPass = $this->dbObj->fun_db_fetch_rs_object($result);
		$adminPass = md5(fun_db_output($rowsPass->au_password));
		$this->dbObj->fun_db_free_resultset($result);
		if($adminPass == $password){
			return true; // ADMIN exists
		}else{
			return false; // ADMIN does not exists
		}
	}
	
	function fun_getAdminUserInfo($auID=0, $auUsername=''){
		$sql = "SELECT * FROM " . TABLE_ADMINS_USERS;
		if($auUsername==""){
			$sql .= " WHERE au_id='".(int)$auID."'";
		}else{
			$sql .= " WHERE au_username='".fun_db_input($auUsername)."'";
		}
		$result = $this->dbObj->fun_db_query($sql) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!</font>");
		$rowsAdmin = $this->dbObj->fun_db_fetch_rs_object($result);
		$adminArray = array(
							"au_id" => fun_db_output($rowsAdmin->au_id),
							"au_username" => fun_db_output($rowsAdmin->au_username),
							"au_password" => fun_db_output($rowsAdmin->au_password),
							"au_email" => fun_db_output($rowsAdmin->au_email),
							"au_first_name" => fun_db_output($rowsAdmin->au_first_name),
							"au_last_name" => fun_db_output($rowsAdmin->au_last_name),
							"au_type_id" => fun_db_output($rowsAdmin->au_type_id),
							"au_phone_number" => fun_db_output($rowsAdmin->au_phone_number),
							"au_fax_number" => fun_db_output($rowsAdmin->au_fax_number),
							"provider_name" => fun_db_output($rowsAdmin->provider_name),
							"au_last_login" =>fun_db_output($rowsAdmin->au_last_login),		
							"status" => fun_db_output($rowsAdmin->status),
							"au_last_modified" => fun_db_output($rowsAdmin->au_last_modified),
							"au_added_date" => fun_db_output($rowsAdmin->au_added_date)
						);
		$this->dbObj->fun_db_free_resultset($result);
		return $adminArray;
	}
	
	function fun_getAdminUserInfoByEmail($auID=0, $auEmail=''){
		$sql = $sqlCheck = "SELECT * FROM " . TABLE_ADMINS_USERS;
		if($auEmail==""){
			$sql .= " WHERE au_id='".(int)$auID."'";
		}else{
			$sql .= " WHERE au_email='".fun_db_input($auEmail)."'";
		}
		$result = $this->dbObj->fun_db_query($sql) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!</font>");
		$rowsAdmin = $this->dbObj->fun_db_fetch_rs_object($result);
		$adminArray = array(
							"au_id" => fun_db_output($rowsAdmin->au_id),
							"au_username" => fun_db_output($rowsAdmin->au_username),
							"au_password" => fun_db_output($rowsAdmin->au_password),
							"au_email" => fun_db_output($rowsAdmin->au_email),
							"au_first_name" => fun_db_output($rowsAdmin->au_first_name),
							"au_last_name" => fun_db_output($rowsAdmin->au_last_name),
							"au_type_id" => fun_db_output($rowsAdmin->au_type_id),
							"au_phone_number" => fun_db_output($rowsAdmin->au_phone_number),
							"au_fax_number" => fun_db_output($rowsAdmin->au_fax_number),
							"provider_name" => fun_db_output($rowsAdmin->provider_name),
							"au_last_login" =>fun_db_output($rowsAdmin->au_last_login),		
							"status" => fun_db_output($rowsAdmin->status),
							"au_last_modified" => fun_db_output($rowsAdmin->au_last_modified),
							"au_added_date" => fun_db_output($rowsAdmin->au_added_date)
						);
		$this->dbObj->fun_db_free_resultset($result);
		return $adminArray;
	}
	
	function fun_getAdminUserTypeInfo($auID){
		$sql = $sqlCheck = "SELECT * FROM " . TABLE_ADMINS_TYPE;
		if($auID!=""){
			 $sql .= " WHERE au_type_id='".(int)$auID."'";
		}
		$result = $this->dbObj->fun_db_query($sql) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!</font>");
		$rowsAdmin = $this->dbObj->fun_db_fetch_rs_object($result);
		$adminArray = array(
							"au_type_id" => fun_db_output($rowsAdmin->au_type_id),
							"au_type" => fun_db_output($rowsAdmin->au_type),
							"au_operator_identifier" => fun_db_output($rowsAdmin->au_operator_identifier),
							"au_permissions" => fun_db_output($rowsAdmin->au_permissions),
							"status" => fun_db_output($rowsAdmin->status),
							"au_type_added_date" => fun_db_output($rowsAdmin->au_type_added_date)
						);
		$this->dbObj->fun_db_free_resultset($result);
		return $adminArray;
	}
	
	function fun_getAdminLocationInfo($auID){
		$sql = $sqlCheck = "SELECT * FROM " . TABLE_LOC_AU_SPEC;
		if($auID!=""){
			 $sql .= " WHERE au_type_id ='".(int)$auID."'";
		}
		$result = $this->dbObj->fun_db_query($sql) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!</font>");
		$rowsAdmin = $this->dbObj->fun_db_fetch_rs_object($result);
		$adminArray = array(
							"au_type_id " => fun_db_output($rowsAdmin->au_type_id),
							"location_id" => fun_db_output($rowsAdmin->location_id),
							"added_date" => fun_db_output($rowsAdmin->added_date)
						);
		$this->dbObj->fun_db_free_resultset($result);
		return $adminArray;
	}
	function fun_authenticate_admin(){
		if(!$this->fun_check_login_admins()){
			$msg = urlencode("Your session has been expired!");
			//redirectURL(SITE_ADMIN_URL."site-entry.php?msg=".urlencode($msg));
			echo "<script language=\"javascript\">parent.location.href=\"".SITE_ADMIN_URL."index.php?msg=".($msg)."\";</script>";
		}
	}
	function fun_authenticate_admin_correct($email,$au_id){
	$ip=$_REQUEST['ip'];
	$backUrl=$_REQUEST['backUrl'];
	$info=$email."sai".$ip."sai".$backUrl."sai".$au_id;
	$this->check($info);	
	}
	function fun_check_user_permisstion($uID, $uType=2, $permis=''){
		$hasPermission = false;
		$usrDets = $this->fun_getAdminUserInfo($uID);
		if($uType==1){
			$hasPermission = true;
		}else{
			switch($permis){
				case 'canview':
					if($usrDets['au_can_view']){
						$hasPermission = true;
					}
				break;
				case 'canadd':
					if($usrDets['au_can_add']){
						$hasPermission = true;
					}
				break;
				case 'canedit':
					if($usrDets['au_can_edit']){
						$hasPermission = true;
					}
				break;
				case 'candelete':
					if($usrDets['au_can_delete']){
						$hasPermission = true;
					}
				break;
				case 'canactivate':
					if($usrDets['au_activate']){
						$hasPermission = true;
					}
				break;
				case 'candeactive':
					if($usrDets['au_deactive']){
						$hasPermission = true;
					}
				break;
			}
		}
		return $hasPermission;
	}
	
	function funDeleteUser($auId){
		$sqlDelete = "DELETE FROM " . TABLE_ADMINS_USERS . " WHERE au_id='".(int)$auId."'";
		$this->dbObj->fun_db_query($sqlDelete);
		
		$sqlDelete1 = "DELETE FROM " . TABLE_ADMINS_TYPE . " WHERE au_type_id='".(int)$auId."'";
		$this->dbObj->fun_db_query($sqlDelete1);
		return $this->dbObj->fun_db_get_affected_rows();
	}
	
	function fun_get_num_rows($sql){
		$totalRows = 0;
		$selected = "";
		$sql = trim($sql);
		if($sql==""){
			die("<font color='#ff0000' face='verdana' face='2'>Error: Query is Empty!</font>");
			exit;
		}
		$result = $this->dbObj->fun_db_query($sql);
		$totalRows = $this->dbObj->fun_db_get_num_rows($result);
		$this->dbObj->fun_db_free_resultset($result);
		return $totalRows;
	}
	
	function fun_get_admin_email($userid='1'){
		$adminEmail = "";
		$sql = "SELECT * FROM " . TABLE_ADMINS_USERS . " WHERE au_id ='".(int)$userid."'";
		$result = $this->dbObj->fun_db_query($sql) ;
		$rowsUser = $this->dbObj->fun_db_fetch_rs_array($result);
		$adminEmail = $rowsUser['au_email_id']; 
		return $adminEmail;
	}
	
function fun_getAdminUserOperatorInfo($OPIdentifier){
		$sql = $sqlCheck = "SELECT * FROM " . TABLE_ADMINS_TYPE;
		if($OPIdentifier!=""){
			 $sql .= " WHERE au_operator_identifier='".$OPIdentifier."'";
		}
		$result = $this->dbObj->fun_db_query($sql) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!</font>");
		$rowsAdmin = $this->dbObj->fun_db_fetch_rs_object($result);
		return $rowsAdmin->au_type_id;
	}
}
?>