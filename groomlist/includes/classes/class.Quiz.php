<?php
class Quiz{
	var $dbObj;
	
	function Quiz(){ // class constructor
		$this->dbObj = new DB();
		$this->dbObj->fun_db_connect();
		   		//$this->image = new SimpleImage();

	}
	function funGetQuizInfo($cateID){
		$locArray = array();
		$sql = "SELECT * FROM " . TABLE_QUIZ . " WHERE quiz_id='".(int)$cateID."'";
		
		$result = $this->dbObj->fun_db_query($sql) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
		if(!$result || $this->dbObj->fun_db_get_num_rows($result) < 1){
			return; // user does not exists
		}
		$rowsCategory =  $this->dbObj->fun_db_fetch_rs_object($result);
		$locArray = array(
							"quiz_id" => fun_db_output($rowsCategory->quiz_id),
							"quiz_ques_text" => fun_db_output($rowsCategory->quiz_ques_text),
							"person_id" => fun_db_output($rowsCategory->person_id),
						    "list_order" => fun_db_output($rowsCategory->list_order),
							"quiz_score_1" => fun_db_output($rowsCategory->quiz_score_1),
							"quiz_score_2" => fun_db_output($rowsCategory->quiz_score_2),
							"quiz_score_3" => fun_db_output($rowsCategory->quiz_score_3),
							"quiz_score_4" => fun_db_output($rowsCategory->quiz_score_4),
							"status" => fun_db_output($rowsCategory->status),
							"last_modified" => fun_db_output($rowsCategory->last_modified),
							"added_date" => fun_db_output($rowsCategory->added_date)
						 );
		return $locArray;
	}
	
	function fun_get_num_rows($sql){
		$totalRows = 0;
		$selected = "";
		$sql = trim($sql);
		if($sql==""){
			die("<font color='#ff0000' face='verdana' face='2'>Error: Query is Empty!</font>");
			exit;
		}
		$result = $this->dbObj->fun_db_query($sql);
		$totalRows = $this->dbObj->fun_db_get_num_rows($result);
		$this->dbObj->fun_db_free_resultset($result);
		return $totalRows;
	}
	
	
	
}
?>