<?php
class Contact{
	var $dbObj;
	
	function Contact(){ // class constructor
		$this->dbObj = new DB();
		$this->dbObj->fun_db_connect();
	} 
	
	function processContact(){

		$locArray = array(
						"name" => $_POST['name'],
						"email" => $_POST['email'],
						"user_wed_date" => $_POST['wedding_year']."-".$_POST['wedding_month']."-".$_POST['wedding_day'],
						"phone_number" => $_POST['phone_number'],
						"speech_for" => $_POST['speech_for'],
						"message" => $_POST['message'],
						"contactform_id" => $_POST['contactForm'],
						"last_modified" => date("Y-m-d H:i:s")
					);
			$fields = "";
			$fieldsVal = "";
			foreach($locArray as $keys => $vals){
				$fields .= $keys . ", ";
				$fieldsVal .= "'" . fun_db_input($vals). "', ";
			}
			
			$sqlInsertFeature = "INSERT INTO " . TABLE_CONTACT . "(contact_id, ".$fields." added_date) " ;
			 $sqlInsertFeature .= " VALUES(null, ".$fieldsVal." '".date("Y-m-d H:i:s")."')";
			$result=$this->dbObj->fun_db_query($sqlInsertFeature) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
			
			return $this->dbObj->fun_db_get_affected_rows();
	}
		function processSpamContact(){

		$locArray = array(
						"name" => $_POST['name'],
						"email" => $_POST['email'],
						"phone_number" => $_POST['phone_number'],
						"speech_for" => $_POST['speech_for'],
						"message" => $_POST['message'],
						"last_modified" => date("Y-m-d H:i:s")
					);
			$fields = "";
			$fieldsVal = "";
			foreach($locArray as $keys => $vals){
				$fields .= $keys . ", ";
				$fieldsVal .= "'" . fun_db_input($vals). "', ";
			}
			
			$sqlInsertFeature = "INSERT INTO " . TABLE_SPAM_CONTACT . "(contact_id, ".$fields." added_date) " ;
			 $sqlInsertFeature .= " VALUES(null, ".$fieldsVal." '".date("Y-m-d H:i:s")."')";
			$result=$this->dbObj->fun_db_query($sqlInsertFeature) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
			
			return $this->dbObj->fun_db_get_affected_rows();
	}

	function processNewsContact(){

		$locArray = array(
						"email" => $_POST['email_address']
					);
			$fields = "";
			$fieldsVal = "";
			foreach($locArray as $keys => $vals){
				$fields .= $keys . ", ";
				$fieldsVal .= "'" . fun_db_input($vals). "', ";
			}
			
			$sqlInsertFeature = "INSERT INTO " . TABLE_NEWSLETTER . "(newsletter_id, ".$fields." added_date) " ;
			 $sqlInsertFeature .= " VALUES(null, ".$fieldsVal." '".date("Y-m-d H:i:s")."')";
			$result=$this->dbObj->fun_db_query($sqlInsertFeature) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
			
			return $this->dbObj->fun_db_get_affected_rows();
	}

}
?>