<?php
class SpeechPara{
	var $dbObj;
	
	function SpeechPara(){ // class constructor
		$this->dbObj = new DB();
		$this->dbObj->fun_db_connect();
		   		//$this->image = new SimpleImage();

	}
	function funGetParaInfo($cateID){
		$locArray = array();
		$sql = "SELECT * FROM " . TABLE_SPEECH_PARA . " WHERE speech_para_id='".(int)$cateID."'";
		
		$result = $this->dbObj->fun_db_query($sql) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
		if(!$result || $this->dbObj->fun_db_get_num_rows($result) < 1){
			return; // user does not exists
		}
		$rowsCategory =  $this->dbObj->fun_db_fetch_rs_object($result);
		$locArray = array(
							"speech_para_id" => fun_db_output($rowsCategory->speech_para_id),
							"speech_para_text" => fun_db_output($rowsCategory->speech_para_text),
							"person_id" => fun_db_output($rowsCategory->person_id),
						    "list_order" => fun_db_output($rowsCategory->list_order),
							"status" => fun_db_output($rowsCategory->status),
							"press_image"=> fun_db_output($rowsCategory->press_image),
							"last_modified" => fun_db_output($rowsCategory->last_modified),
							"added_date" => fun_db_output($rowsCategory->added_date)
						 );
		return $locArray;
	}
	
	function fun_get_num_rows($sql){
		$totalRows = 0;
		$selected = "";
		$sql = trim($sql);
		if($sql==""){
			die("<font color='#ff0000' face='verdana' face='2'>Error: Query is Empty!</font>");
			exit;
		}
		$result = $this->dbObj->fun_db_query($sql);
		$totalRows = $this->dbObj->fun_db_get_num_rows($result);
		$this->dbObj->fun_db_free_resultset($result);
		return $totalRows;
	}
	
	
	
}
?>