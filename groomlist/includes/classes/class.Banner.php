<?php
class Banner{
	var $dbObj;
	
	function Banner(){ // class constructor
		$this->dbObj = new DB();
		$this->dbObj->fun_db_connect();
	} 
	
	function funGetbannerInfo($featureId)
	{
		$cateArray = array();
		  $sql = "SELECT * FROM " . TABLE_BANNER . " WHERE banner_id ='".(int)$featureId."'";
		
		$result = $this->dbObj->fun_db_query($sql) or die("<font color='#ff0000' face='verdana' size='2'>Error: Unable to execute request!<br>Invalid Query On Category table.</font>");
		if(!$result || $this->dbObj->fun_db_get_num_rows($result) < 1){
			return; // user does not exists
		}
		$rowsCategory =  $this->dbObj->fun_db_fetch_rs_object($result);
		$cateArray = array(
							"banner_id" => fun_db_output($rowsCategory->banner_id),
							"pages_id" => fun_db_output($rowsCategory->pages_id),	
							"banner_image_path" => fun_db_output($rowsCategory->banner_image_path),
							"banner_image_alt"  => fun_db_output($rowsCategory->banner_image_alt),
							"status"  => fun_db_output($rowsCategory->status),
							"banner_added_date" => fun_db_output($rowsCategory->banner_added_date),
							"banner_last_modified" => fun_db_output($rowsCategory->banner_last_modified)
						 );
		return $cateArray;
	}
	
}
?>