<?php 
require_once("includes/application-top.php");
	unset($_SESSION['user_id']);
	session_unset();
	
	session_destroy();
	redirectURL(SITE_URL."index.php");

?>
