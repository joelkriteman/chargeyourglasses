<?php include_once("includes/header.php");
$pageId=21;
$sql="SELECT * FROM ".TABLE_PAGES." WHERE status='1' AND pages_id='".$pageId."'";
$result=mysql_query($sql);
$rowsResult=mysql_fetch_array($result);

$sqlFeedback="SELECT * FROM ".TABLE_FEEDBACKS." WHERE status='1' AND home_page_status='1' AND groom_page_status='1' ORDER BY rand()";
$resultFeedback=mysql_query($sqlFeedback);
?>
<!--banner start here-->
<article id="banner" class="alpha-ver15">
  <div class="col_1280">
    <article class="aligncenter">
      <h1 class="main-heading">
        <?php $mainTitle = str_replace('<p>','',$rowsResult['home_main_title']);
	  echo $mainTitle = str_replace('</p>','',$mainTitle);
	  ?>
      </h1>
    </article>
    <?php include_once("includes/subscribe-msg.php");?>
    <article class="omega20">
      <!--col78 start here-->
      <aside class="alignleft col_78"> <?php echo $rowsResult['home_LeftIntro'];?> <br>
      </aside>
      <!--col78 end here-->
      <!--col20 start here-->
      <aside class="alignright col_20">
        <!--banner widget outer start here-->
        <section class="banner-widget-outer w-auto"> <a href="<?php echo SITE_URL;?>groom-wedding-speeches.php">
          <div class="banner-widget">
            <div class="ban-img">
              <div class="ban-bg"><img src="<?php echo SITE_URL;?>/assets/images/groom-img.jpg" alt="Are You a Groom?"></div>
            </div>
            <div class="ban-ico"><img src="<?php echo SITE_URL;?>/assets/images/groom_ico.jpg" alt="groom"></div>
            <p>Ready to start writing your  <br>
              groom speech</p>
          </div>
          <p><span class="blue-btn">Click here</span></p>
          </a> </section>
        <!--banner widget outer end here-->
      </aside>
      <!--col20 end here-->
    </article>
  </div>
</article>
<!--banner end here-->
<!--caption outer start here-->
<article id="caption-outer" class="alpha-ver15">
  <div class="col_1280">
    <aside class="alignleft col_78">
      <h2>The Groom List Speech Builder costs only <strong class="blue">&pound;<?php echo $generalInfoDetail['amount'];?></strong> for <strong class="blue"><?php echo $generalInfoDetail['time_days'];?> days</strong> access.</h2>
      <p><?php echo $rowsResult['home_midText'];?></p>
    </aside>
    <div class="alignright">
      <p><a href="<?php echo SITE_URL;?>register.php" class="example big-btn">Register Now!</a></p>
      <p class="blue">To Create Unlimited Speeches</p>
    </div>
  </div>
</article>
<!--caption outer end here-->
<?php include_once("includes/footer.php");?>
