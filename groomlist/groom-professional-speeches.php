<?php include_once("includes/header.php");
$pageId=17;
$sql="SELECT * FROM ".TABLE_PAGES." WHERE status='1' AND pages_id='".$pageId."'";
$result=mysql_query($sql);
$rowsResult=mysql_fetch_array($result);
?>
<!--header end here-->

<section id="breadcrumbs" class="alpha-ver15">
  <div class="col_1280">
    <ul>
           <li><a href="<?php echo SITE_URL;?>">Home</a></li>
      <li>&gt;</li>
      <li><?php echo $rowsResult['pages_title'];?></li>
    </ul>
  </div>
</section>
<section class="content-outer alpha-ver15">
  <div class="col_1280">
    <aside id="main-content" class="alignleft">
      <div class="sub-heading">
        <h1><?php echo $rowsResult['pages_title'];?></h1>
      </div>
      <div class="dashed-border"></div>
      <p class="omega20"><?php echo $rowsResult['pages_content'];?></p>
      <div class="omega30"><a href="http://www.chargeyourglasses.com/pdf/<?php echo $generalInfoDetail['sample_groom_speech_file'];?>" target="_blank" class="medium-btn">DOWNLOAD SPEECH<span class="sprite-img"><img src="<?php echo SITE_URL;?>assets/images/download-ico.png" alt="download speech"></span></a></div>
    </aside>
    <?php include_once("includes/sidebar.php");?>
  </div>
</section>
<?php include_once("includes/footer.php");?>
