<?php 
require_once("includes/application-top.php");
if($_REQUEST['security_key']==md5("REGISTERME"))
{
		 $userId=$customerobj->processUser();
		 //$order_id=$OrderObj-> processOrder($userId);

		 $emailRegisterFile = SITE_EMAIL_TAMPLATE_WS . "customer-registration.html";
   	 	 $pwdContent = fun_getFileContent($emailRegisterFile);
 	     $pwdContent = str_replace("[%SITE_NAME%]", SITE_NAME, $pwdContent);
		 $pwdContent = str_replace("[%NAME%]", $_POST['user_fname']." ".$_POST['user_lname'], $pwdContent);
		 $pwdContent = str_replace("[%EMAIL%]",  $_REQUEST['user_email'], $pwdContent);
		 $pwdContent = str_replace("[%DATE%]",  date("d/m/Y"), $pwdContent);
		 $pwdContent = str_replace("[%PASSWORD%]",$_POST['user_pwd'], $pwdContent);
		 $pwdContent = str_replace("[%WEDDATE%]", fun_site_date_format_for_email($_POST['wedding_year']."-".$_POST['wedding_month']."-".$_POST['wedding_day']), $pwdContent);
         $pwdContent = str_replace("[%ROLE%]", $_POST['user_role'], $pwdContent);
  		 $pwdContent = str_replace("[%SITE_LOGO%]", EMAIL_LOGO, $pwdContent);
		 
		 $to=$_REQUEST['user_email'];
		 $subject = 'Your login details for '.SITE_NAME.'';
	 	 $from=SITE_SUPPORT_EMAIL_ID;
		 $fromContent="Groomlist";	
		 $mailSentStatus = fun_get_email($to , $subject, $pwdContent, $from,$fromContent);
		 
		 $_SESSION['session_username'] = $_REQUEST['user_email'];
		 $_SESSION['user_id'] = $userId;
		 ?>
					<script type="text/javascript">
					window.parent.location = '<?php echo SITE_URL;?>confirm-signup-payment.php?back_url=<?php echo urlencode($_SERVER['HTTP_REFERER']);?>';
					</script>
<?php
}
?>
<script type="text/javascript" src="<?php echo SITE_URL;?>assets/js/jquery-1.8.3.min.js"></script>
<link href="<?php echo SITE_URL;?>assets/css/popup.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
function validateRegisterFrm(){
var frm = document.registerForm;
if(frm.user_fname.value==""){
	frm.user_fname.style.borderColor='#FF0000';
	frm.user_fname.focus();
	return false;
	}
if(frm.user_email.value==""){
	frm.user_email.style.borderColor='#FF0000';
	frm.user_email.focus();
	return false;
	}
if(frm.user_email.value!=""){
		if(isEmail(frm.user_email.value)==false){
			frm.user_email.style.borderColor='#FF0000';
			frm.user_email.focus();
			return false;
		}
	}
if(frm.user_email.value!=""){
			var email=frm.user_email.value;
			var ajaxaction='EmailDuplicacyCheck';
	 		$.post('<?php echo SITE_URL;?>ajaxcon.php', {ajaxaction:ajaxaction,email:email}, function(data) {
	 		$('#validation1').html(data);
			if(data!="")
			{
			frm.user_email.style.borderColor='#FF0000';
			frm.user_email.focus();
			}
			return false;
			});
}
if(frm.user_pwd.value==""){
	frm.user_pwd.style.borderColor='#FF0000';
	frm.user_pwd.focus();
	return false;
	}
if(frm.confirm_password.value==""){
	frm.confirm_password.style.borderColor='#FF0000';
	frm.confirm_password.focus();
	return false;
	}
if(frm.confirm_password.value!=frm.user_pwd.value){
	frm.confirm_password.style.borderColor='#FF0000';
	frm.confirm_password.focus();
	return false;
	}
if(!document.registerForm.chkconfirm.checked)
	{
	alert("Please accept our terms and conditions!");
	return false;	
	}
}
function ChkmailtoDb()
{
	var frm = document.registerForm;
	if(frm.user_email.value!=""){
			var email=frm.user_email.value;
			var ajaxaction='EmailDuplicacyCheck';
	 		$.post('<?php echo SITE_URL;?>ajaxcon', {ajaxaction:ajaxaction,email:email}, function(data) {
	 		$('#validation1').html(data);
			if(data!="")
			{
			frm.user_email.style.borderColor='#FF0000';
			frm.user_email.focus();
			}
			return false;
			});
}
}

function isEmail(str){
	var at="@";
	var dot=".";
	var lat=str.indexOf(at);
	var ldot=str.indexOf(dot);
	var lstr=str.length;

	if(str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
		return false;
	}
	if(str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
		return false;
	}
	if(str.indexOf(" ")!=-1){
		return false;
	}
	if(str.indexOf(at,(lat+1))!=-1){
		return false;
	}
	if(str.indexOf(dot,(lat+2))==-1){
		return false;
	}
	if(str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
		return false;
	}
return true;
}
$(document).ready(function() {

$('#go_fb').click(function() {

        var left = (screen.width / 2) - 200;

        var top = (screen.height / 2) - 150;

        var fbWindow = parent.open("fb/request_permissions.php",'FacebookLogin','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no');

        fbWindow.focus();

    });	

});

</script>
<div class="register-outer">
<div class="register-bg aligncenter">
  <h3>Join today and start making your speech</h3>
  <p class="omega10">To view all Speech material you need to pay (&pound;<?php echo $generalInfoDetail['groomlist_amount'];?>). The moment your payment is processed you will be automatically taken back to the Speech Builder and be able to get cracking!</p>
</div>
<div class="alignleft col_49">
  <div class="aligncenter alpha-all10">
    <p> You can connect with your Facebook
      account to prepopulate some of the fields. </p>
    <p class="omega40"> <a id="go_fb" href="javascript:void(0)"><img src="assets/images/sign-in-facebook.jpg" /></a></p>
    <!--    <p><a href="#"><img src="assets/images/sign-in-twitter.jpg" /></a></p>
-->
    <p class="omega60">Already have an account? <a class="examplepage yellow-btn" href="<?php echo SITE_URL;?>login.php" style="cursor:pointer;">Login</a></p>
  </div>
</div>
<div class="alignright register-form col_49">
  <div id="validation1"></div>
  <div class="alpha-all10">
    <form method="post" name="registerForm" onSubmit="return validateRegisterFrm();" action="<?php echo SITE_URL;?>register">
      <input type="hidden" name="security_key" value="<?php echo md5("REGISTERME");?>"  />
      <input type="hidden" name="website_id" value="2"  />
      <input type="hidden" name="total_amount" value="<?php echo $generalInfoDetail['groomlist_amount'];?>"  />
      <div class="omega5">
        <label>First Name:<span class="red">*</span></label>
        <input type="text" name="user_fname"/>
      </div>
      <div class="omega5">
        <label>Surname:</label>
        <input type="text" name="user_lname"/>
      </div>
      <div class="omega5">
        <label>Wedding Date:</label>
        <!-- <input type="text" name="user_wed_date" id="date" />-->
        <select class="date" name="wedding_day">
          <?php fun_create_number_options(1,31,date('d'));?>
        </select>
        <select class="month" name="wedding_month">
          <?php fun_created_month_option(date('m'));?>
        </select>
        <select class="year" name="wedding_year">
          <?php fun_created_year_option();?>
        </select>
      </div>
      <input type="hidden" name="user_role" value="Groom"  />
      
      <!--<div class="omega5">
          <label>Speech for:</label>
          <select name="user_role">
            <option value="Best Man">Best Man</option>
            <option value="Father Of the Bride">Father of the Bride</option>
            <option value="Groom">Groom</option>
           
          </select>
        </div>-->
      <div class="omega5">
        <label>Email Address:<span class="red">*</span></label>
        <input type="text" name="user_email" onblur="javascript:ChkmailtoDb();"/>
      </div>
      <div class="omega5">
        <label>Password:<span class="red">*</span></label>
        <input type="password" name="user_pwd"/>
      </div>
      <div class="omega5">
        <label>Confirm Password:<span class="red">*</span></label>
        <input type="password" name="confirm_password"/>
      </div>
      <div class="omega5 textalignright">
        <input type="checkbox" value="1" name="chkconfirm"/>
        <span>I agree to the <a href="<?php echo SITE_URL;?>terms-condition.php" target="_blank" class="blue">Terms & Conditions</a></span> </div>
      <div class="omega5">
        <label>&nbsp;</label>
        <input type="submit" value="Register" />
      </div>
    </form>
  </div>
</div>
</div>
